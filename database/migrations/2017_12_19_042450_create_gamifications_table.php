<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gamifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('watering')->default(0); //ubntuk gamification bosoteer (0 & 1)
            $table->float('fertilizer', 15, 2)->default(0); //ubntuk gamification substrate (history amount total donation)
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gamifications');
    }
}
