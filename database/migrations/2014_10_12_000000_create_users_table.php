<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 25);
            $table->string('last_name', 25);
            $table->string('gender', 1);
            $table->string('username', 75)->unique()->nullable();
            $table->string('facebook_email', 25)->nullable();
            $table->integer('facebook_id')->unsigned()->nullable();
            $table->string('gmail_email', 25)->nullable();
            $table->integer('gmail_id')->unsigned()->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('mobile', 14);
            $table->string('status', 25);
            $table->string('image', 255)->nullable();
            $table->string('address', 100)->nullable();
            $table->integer('role_id')->unsigned();
            $table->string('email', 50);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
