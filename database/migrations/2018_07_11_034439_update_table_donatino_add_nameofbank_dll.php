<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableDonatinoAddNameofbankDll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('confirmation_donations', function (Blueprint $table) {
            //
            $table->string('email')->nullable();
            $table->string('name_of_bank')->nullable();
            $table->date('trasnfer_date')->nullable();
            $table->text('picture')->nullable();
            $table->dateTime('donation_datetime')->default(null);               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donations', function (Blueprint $table) {
            //
        });
    }
}
