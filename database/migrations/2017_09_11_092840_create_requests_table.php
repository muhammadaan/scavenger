<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->string('longitude', 100);
            $table->string('latitude', 100);
            $table->date('order_date');
            $table->date('date_request_pickup');
            $table->string('time_request_pickup',25);
            $table->string('note', 255);
            $table->string('image', 255);
            $table->date('pickup_date')->nullable();
            $table->string('pickup_time', 25)->nullable();
            $table->string('vehicle', 30)->nullable();
            $table->tinyInteger('status');
            $table->double('weight_total_estimate', 5, 2);
            $table->integer('scavanger_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
