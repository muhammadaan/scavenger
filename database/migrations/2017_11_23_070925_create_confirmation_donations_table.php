<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfirmationDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmation_donations', function (Blueprint $table) {

            
            $table->increments('id');
            $table->integer('donation_id')->unsigned();
            $table->string('bank_account_name', 20)->nullable();
            $table->string('register_name', 50)->nullable();
            $table->string('bank_account_number', 50);
            $table->string('transfer_to', 50)->nullable();
            $table->double('amount_of_donation');
            $table->text('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirmation_donations');
    }
}
