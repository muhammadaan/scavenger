@extends('layouts.back-end.sidebar')

@section('content')
        <!-- Breadcrumb -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>
            <li><a href="{{url('admin/vehicle/detail',$vehicles->id)}}">Vehicle Detail</a></li>
            <li class="active">List</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="block">
            <div class="header">
                <h2 style="color: rgb(0,146,69)">Vehicle Detail</h2>
            </div>
            <form id="validate" method="POST" action="{{url('admin/vehicle', $vehicles->id)}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="content controls">
                    <div class="form-row">
                        <div class="col-md-3">Vehicle Name</div>
                        <div class="col-md-9">
                            <input type="text"  class="validate[required]" name="vehicleName" value="{{$vehicles->vehicle_name}}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3">Description</div>
                        <div class="col-md-9">
                            <textarea rows="3" class="validate[required]" name="desc">{{$vehicles->desc}}</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3">Status</div>
                        <div class="col-md-9">
                            <select class="validate[required]" name="status">
                                @if($vehicles->status == 1)
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                @elseif($vehicles->status == 2)
                                    <option value="2">Inactive</option>
                                    <option value="1">Active</option>
                                @else
                                    <option value="2">-</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    {{--<div class="form-row">--}}
                        {{--<div class="col-md-3">Created By</div>--}}
                        {{--<div class="col-md-9">--}}
                            {{--<input type="text" class="validate[required]" name="createdBy" value="{{$vehicles->created_by}}">--}}
                        {{--</div>--}}
                    {{--</div>--}}

                </div>
                <div class="footer">
                    <div class="side pull-right">
                        <div class="btn-group">
                                <span style="float: right;">
                                    <button type="submit" class="btn btn-info btn-rounded" style="background-color: rgb(0,146,69)">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-rounded" style="background-color: rgb(0,146,69)">
                                        <a style="color: white">
                                            Cancel
                                        </a>
                                    </button>
                                 </span>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script type="text/javascript">

    </script>
@endsection
