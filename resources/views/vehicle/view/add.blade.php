@extends('layouts.back-end.sidebar')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{url('admin/home')}}">Home</a></li>
                <li><a href="{{url('/admin/vehicle-list')}}">Vehicle</a></li>
                <li class="active">Add</li>
            </ol>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

            <div class="block">
                <div class="header">
                    <h2>Add New Vehicle</h2>
                </div>
                <form id="validate" method="POST" action="{{url('admin/vehicle-add')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="content controls">
                        <div class="form-row">
                            <div class="col-md-3">Vehicle Name</div>
                            <div class="col-md-9">
                                <input type="text"  class="validate[required]" name="vehicleName">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3">Description</div>
                            <div class="col-md-9">
                                <textarea rows="3" class="validate[required]" name="desc"></textarea>
                            </div>
                        </div>
                        <div class="form-row" style="visibility: hidden">
                            <div class="col-md-3">Status</div>
                            <div class="col-md-9">
                                <select class="validate[required]" name="status">
                                    <option value="1">Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <div class="side pull-right">
                            <div class="btn-group">
                                <span style="float: right;">
                                    <button type="submit" class="btn btn-info btn-rounded">
                                        Save
                                    </button>
                                    <button type="reset" class="btn btn-rounded" style="background-color: gray">
                                        <a style="color: white">
                                            Cancel
                                        </a>
                                    </button>
                                 </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
