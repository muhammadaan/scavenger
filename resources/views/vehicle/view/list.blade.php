@extends('layouts.back-end.sidebar')

@section('content')
        <!-- Breadcrumb -->
<style>
    select[name="table-vehicle_length"] {
        width: 75px;
    }

    div[class="dataTables_length"] {
        width: 300px;
    }
</style>


<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>
            <li><a href="{{url('admin/vehicle-list')}}">Vehicle</a></li>
            <li class="active">List</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @if (session('status'))
            <div class="alert alert-success">
                <strong>{{ session('status') }}.</strong>
                <button type="button" class="close" data-dismiss="alert">×</button>

            </div>
        @endif
        <div class="block">
            <div class="header">
                <h2 style="color: rgb(0,146,69)">Vehicle List</h2>

                <div class="side pull-right">
                    <ul class="buttons">
                        <li class="btn-group">

                        </li>
                        <li><a href="#" class="block-toggle tip" title="Toggle content"><span
                                        class="icon-chevron-down"></span></a></li>

                    </ul>
                </div>
            </div>
            <hr>
            <div class="content">
                <div class="col-md-2">
                    <a href="{{ url('admin/vehicle-add') }}" class="btn btn-primary btn-block" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69);">Add Vehicle</a>
                </div>
                <table class="table table-bordered table-striped table-hover" id="table-vehicle">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Vehicle Name</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <!-- <th>Created By</th> -->
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vehicles as $index => $vehicle)
                        <tr class="clicklable-row" data-href="{{url('/admin/vehicle/detail', $vehicle->id)}}">
                            <td>{{$index+1}}</td>
                            <td>{{$vehicle->vehicle_name}}</td>
                            @if($vehicle->status == 1)
                                <td>Active</td>
                            @elseif($vehicle->status == 2)
                                <td>Inactive</td>
                            @else
                                <td>-</td>
                            @endif
                            <td>{{\Carbon\Carbon::parse($vehicle->created_at)->format('d M Y - H:i:s')}}</td>
                            <!-- <td>{{\Illuminate\Support\Facades\Auth::user()->name}}</td> -->
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script type="text/javascript">

        $('#table-vehicle').dataTable({
            "aLengthMenu": [10, 50, 100]
        });

        jQuery(document).ready(function ($) {
            $("#table-vehicle").on('click', '.clicklable-row', function () {
                window.location = $(this).data("href");
            });
            $("#table-vehicle").css('cursor', 'pointer');
            $("#table-vehicle").hover(function () {
                $(this).css('cursor', 'pointer');
            });
        });

    </script>
@endsection
