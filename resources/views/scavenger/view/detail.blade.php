@extends('layouts.back-end.sidebar')

@section('content')
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
<!-- Breadcrumb -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<style>

    #exTab h3 {
        color: white;
        padding: 5px 15px;
    }

    div[class="content information"] {
        height: 250px;
    }

    span {
        margin-top: 60px;
    }

    select[name="tableHistoryRecycle_length"] {
        width: 75px;
    }

    div[class="dataTables_length"] {
        width: 300px;
    }

    div[class="col-md-1 weightTotalActual"]{
        width: 9.5%;
    }

    #new-selected-image {
        width: 100%;
    }

</style>

<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>
            <li><a href="{{url('admin/scavenger-list')}}">Scavenger List</a></li>
            <li class="active"><a href="{{url('admin/scavenger/detail',$scavengers->id)}}">Scavenger Detail</a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="block">
            <div id="exTab" class="container">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#scavenger-tab" data-toggle="tab" style="color:rgb(128,128,128)">Information
                            Detail</a></li>
                    <li role="presentation"><a href="#rating-tab" data-toggle="tab" style="color:rgb(128,128,128)">History Rating</a></li>
                    <li role="presentation"><a href="#recycle-tab" data-toggle="tab" style="color:rgb(128,128,128)">History Recycle</a></li>
                </ul>
            </div>
            <form id="validate" method="POST" action="{{url('admin/scavenger', $scavengers->id)}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="idScavenger" id="idScavenger" value="{{$scavengers->id}}">
                <input type="hidden" name="countDoc" id="countDoc" value="0">

                <div class="content controls">
                    <div class="tab-content">
                        <div class="tab-pane active" id="scavenger-tab">
                            <div class="form-row">
                                <div class="col-md-2">
                                    <p class="text-center">

                                        @if($scavengers->image == null)
                                            <img src="{{url('/storage/app/images/user-no-image.jpg')}}" alt="No Photo" height="100" width="100">
                                        @else
                                            <img src="{{url('/storage/app/images'.'/'.$scavengers->image)}}" alt="Photo not load" height="100" width="100">
                                        @endif
                                    <div id="rateYo" style="margin: 0 auto;"></div>
                                    <div class="counter"></div>

                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <div class="block">
                                        <div class="header">
                                            <h2 style="color: rgb(0,146,69)">Scavenger Information</h2>
                                        </div>
                                        <div class="content information">

                                            <div class="form-row">
                                                <div class="col-md-5">Scavenger Name</div>
                                                <div class="col-md-1">:</div>
                                                <div class="col-md-6">
                                                    @if($scavengers->name == null)
                                                        -
                                                    @else
                                                        {{$scavengers->name}}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-5">Gender</div>
                                                <div class="col-md-1">:</div>
                                                <div class="col-md-6">
                                                    @if($scavengers->gender == 'L')
                                                        Male
                                                    @elseif($scavengers->gender == 'P' || $scavengers->gender == 'W')
                                                        Female
                                                    @elseif($scavengers->gender == null)
                                                        -
                                                    @else
                                                        -
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-5">Date Of Birth</div>
                                                <div class="col-md-1">:</div>
                                                <div class="col-md-6">
                                                    @if($scavengers->date_of_birth == null)
                                                        -
                                                    @else
                                                        {{\Carbon\Carbon::parse($scavengers->date_of_birth)->format('d M Y')}}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-5">Email</div>
                                                <div class="col-md-1">:</div>
                                                <div class="col-md-6">
                                                    @if($scavengers->email == null)
                                                        -
                                                    @else
                                                        {{$scavengers->email}}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-5">Mobile Phone</div>
                                                <div class="col-md-1">:</div>
                                                <div class="col-md-6">
                                                    @if($scavengers->mobile == null)
                                                        -
                                                    @else
                                                        {{$scavengers->mobile}}
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-5">Registration Date</div>
                                                <div class="col-md-1">:</div>
                                                <div class="col-md-6">
                                                    @if($scavengers->created_at == null)
                                                        -
                                                    @else
                                                        {{\Carbon\Carbon::parse($scavengers->created_at)->format('d M Y - H:i:s')}}
                                                    @endif
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="block">
                                        <div class="header">
                                            <h2 style="color: rgb(0,146,69)">Verify Status</h2>
                                        </div>
                                        <div class="content information">

                                            <div class="form-row">
                                                <div class="col-md-5">Status</div>
                                                <div class="col-md-1">:</div>
                                                <div class="col-md-3">
                                                    @if($scavengers->status == 1)
                                                        <select class="form-control validate[required]" id="status"
                                                                name="status" disabled>
                                                            <option value="1">Verified</option>
                                                        </select>
                                                    @else
                                                        <select class="form-control validate[required]" id="status"
                                                                name="status">
                                                            <option value="0">Unverified</option>
                                                            <option value="1">Verified</option>
                                                        </select>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-5">Note</div>
                                                <div class="col-md-1">:</div>
                                                <div class="col-md-6">
                                                    @if($scavengers->note_verify == null)
                                                        <textarea class="form-control validate[required]" id="note_verify" name="note_verify"
                                                                  rows="3">-</textarea>
                                                    @else
                                                        <textarea class="form-control validate[required]" id="note_verify" name="note_verify"
                                                                  rows="3">{{$scavengers->note_verify}}</textarea>
                                                    @endif
                                                </div>
                                            </div>

                                            @if($scavengers->status == 1)
                                                <div class="form-row"></div>
                                            @elseif($scavengers->status == 0)
                                                <span style="float: right;">
                                                    <button type="submit" class="btn btn-info btn-rounded" style="background-color: rgb(0,146,69)">
                                                        Update
                                                    </button>
                                                    <button type="reset" class="btn btn-rounded"
                                                            style="background-color: gray">
                                                        <a style="color: white">
                                                            Cancel
                                                        </a>
                                                    </button>
                                                </span>
                                            @else
                                                <div class="form-row"></div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    @if($scavengers->status == 0)
                                        <span style="float: right; margin-bottom: 10px">
                                            <button name="add_more_img" id="add_more_img" class="btn btn-success" style="background-color: rgb(0,146,69)">
                                                <i class="icon-plus" ></i>
                                                &nbsp;&nbsp;Add Image
                                            </button>
                                        </span>
                                    @else
                                        &nbsp;
                                    @endif
                                    <table class="table table-bordered table-striped table-hover authors-list" id="example">
                                        <thead>
                                        <tr>
                                            <th style="width:35%;">Image</th>
                                            <th>Type</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($scavengers->status == 1)
                                            @foreach($scavengers->document as $key => $doc)
                                                <tr>
                                                    <td>
                                                        <img src="{{ url('storage/app/images') }}/{{$doc->image}}" style="width: 100%">
                                                    </td>
                                                    <td>
                                                        <input type="text" id="type" name="type" value="{{$doc->type}}"
                                                               readonly  style="background-color: white; color: rgb(0,146,69);">
                                                    </td>
                                                    <td id="form-modal">
                                                        <center>
                                                            <a href="#" data-id="{{$doc->id}}"
                                                               class="btn btn-default editButton"><i
                                                                        class="icon-pencil">
                                                                </i>
                                                                Edit
                                                            </a>
                                                            {{--<button class="btn btn-danger btn-rounded">--}}
                                                            <a href="{{route('document.delete', $doc->id)}}"
                                                               style="color: white" class="btn btn-danger">
                                                                <i class="icon-trash"></i>
                                                                Delete
                                                            </a>
                                                            {{--</button>--}}
                                                        </center>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @elseif($scavengers->status == 0)
                                            <tr id=row>
                                                <td id="img_preview_td">
                                                    <input type="file" onchange="myfn(this)" name="image" id="image"
                                                           data-panelid="images" class="form-control validate[required]"
                                                           accept="image/gif, image/png, image/jpeg, image/pjpeg" style="background-color: white;">
                                                    <center><img id="img_preview" style="width: 100%"></center>
                                                </td>
                                                <td>
                                                    <select class="validate[required]" id="type" name="type" style="background-color: white; color: rgb(0,146,69);">
                                                        <option></option>
                                                        <option value="Identity Card">Identity Card</option>
                                                        <option value="Family Card">Family Card</option>
                                                        <option value="Driving License">Driving License</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <button name="remove" id="" class="btn btn-danger btn-remove">
                                                        <i class="icon-trash"></i>
                                                        &nbsp;&nbsp;&nbsp;Delete
                                                    </button>
                                                </td>
                                            </tr>
                                        @else
                                            -
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="rating-tab">
                            <div class="form-row">
                                <h2>
                                    <center>History Ratings</center>
                                </h2>
                                <div class="col-md-12">
                                    <table class="table table-bordered table-striped table-hover authors-list" id="tableHistoryRating">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Request No</th>
                                            <th>Customer Name</th>
                                            <th>Request Date</th>
                                            <th>Rating</th>
                                            <th>Comment</th>                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1; ?>
                                            @foreach($ratings as $rating)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{$rating->request_id}}</td>
                                                <td>{{$rating->user->first_name.' '.$rating->user->last_name}} Name</td>
                                                <td>{{$rating->request->order_date}}</td>
                                                <td>{{$rating->rating}}</td>
                                                <td>{{$rating->note}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="recycle-tab">
                            <div class="form-row">
                                <div class="col-md-1 weightTotalActual">Weight Total Actual</div>
                                <div class="col-md-1" style="width: 10px">:</div>
                                <div class="col-md-3">
                                    {{$scavengers->request->sum('berat_realisasi')}}&nbsp;Kg
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-striped table-hover authors-list" id="tableHistoryRecycle">
                                        <thead>
                                        <tr>
                                            <th>Request No</th>
                                            <th>Request Date</th>
                                            <th>Member Name</th>
                                            <th>Weight Estimate (Kg)</th>
                                            <th>Weight Actual (Kg)</th>
                                            <th>Status</th>
                                            <th>Rate</th>
                                            <th>Note</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($scavengers->request as $key => $recycle)
                                                <tr>
                                                    <td>
                                                        @if(@$recycle->id == null)
                                                            -
                                                        @else
                                                            00{{$recycle->id}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$recycle->order_date == null)
                                                            -
                                                        @else
                                                            {{\Carbon\Carbon::parse($recycle->order_date)->format('d M Y')}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{$scavengers->name}}
                                                    </td>
                                                    <td>
                                                        @if(@$recycle->weight_total_estimate == null)
                                                            -
                                                        @else
                                                            {{$recycle->weight_total_estimate}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$recycle->berat_realisasi == null)
                                                            -
                                                        @else
                                                            {{$recycle->berat_realisasi}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$recycle->status == null)
                                                            -
                                                        @else
                                                            @if($recycle->status == 1)
                                                                Searching for scavenger
                                                            @elseif($recycle->status == 2)
                                                                Waiting for pick up
                                                            @elseif($recycle->status == 3)
                                                                Complete
                                                            @else
                                                                Failed
                                                            @endif
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$recycle->status == 3)
                                                            <div id="rateRecycle" style="margin: 0 auto;"></div>
                                                            <div class="counter"></div>
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(@$recycle->status == 3)
                                                            @if($scavengers->rating->note == null)
                                                                -
                                                            @else
                                                                {{$scavengers->rating->note}}
                                                            @endif
                                                        @else
                                                            -
                                                        @endif

                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @if($scavengers->status == 1)
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        @if(isset($doc))
                            <form id="editDoc" method="post" action="" enctype="multipart/form-data" >
                                <meta id="token" name="token" content="{{csrf_token()}}">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Document Detail</h4>
                                    <div class="modal-body" id="ModalEdit">
                                        <input type="hidden" id="idtest" name="idtest" class="col-md-1">
                                        <input type="hidden" id="idScv" name="idScv" value="{{$doc->scavenger_id}}" class="col-md-1">
                                        <div class="form-row">
                                            <div class="col-md-2">Image</div>
                                            <div class="col-md-5">
                                                <div class="input-group file">
                                                    <input type="text" class="form-control"/>
                                                    <input type="file" name="image" id="addImage"/>
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="button">Browse</button>
                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row" id="logo">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-5">
                                                <div class="image-wrapper">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row" id="partner">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-5" id="addImage">
                                                <div class="image-wrapper">
                                                    <img id="image" class="previewImage" style="width: 100%">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-2">Type</div>
                                            <div class="col-md-5">
                                                <select class="validate[required]" id="typemodal" name="type">

                                                    <option value="Identity Card">Identity Card</option>
                                                    <option value="Family Card">Family Card</option>
                                                    <option value="Driving License">Driving License</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        {{--<button type="submit" class="btn btn-success saveUpdate">Save</button>--}}
                                        <button type="button" class="btn btn-success saveUpdate" onclick="saveChange()">Save</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                </div>
                            </form>

                        @endif
                    </div>
                </div>
            @else
            @endif
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function () {
            var count = 0;
            var i = 0;
            $("#add_more_img" || "#add_more_img'+i+'").click(function (e) {
                i++;
                count++;
                $("#countDoc").val(count);
                $("#example").append(
                        '<tr id=row' + i + '>' +
                        '<td id="img_preview_td">' +
                        '<input type="file" onchange="myfn(this)" name="image' + i + '" id="image' + i + '" ' +
                        'data-panelid="image' + i + '" class="form-control validate[required]" accept="image/gif, image/png, image/jpeg, image/pjpeg">' +
                        '<center>' + '<img id="img_preview' + i + '" />' + '</center>' +
                        '</td>' +
                        '<td>' +
                        '<select class="validate[required]' + i + '" id="type' + i + '" name="type' + i + '">' +
                        '<option></option>' +
                        '<option value="Identity Card">Identity Card</option>' +
                        '<option value="Family Card">Family Card</option>' +
                        '<option value="Driving License">Driving License</option>' +
                        '</select>' +
                        '</td>' +
                        '<td>' +
                        '<button name="remove" id="' + i + '" class="btn btn-danger btn-remove"><i class="icon-trash"></i> &nbsp;&nbsp;&nbsp;Delete</button>' +
                        '</td>' +
                        '</tr>');
                e.preventDefault();
            });

            $(".editButton").click(function (e) {
                var id = $(this).attr("data-id");
                console.log("{{route('document.ajax.getmodal','')}}/" + id);

                $.ajax({
                    url: "{{route('document.ajax.getmodal','')}}/" + id,
                    type: "GET",
                    success: function (ajaxData) {
                        var data = JSON.parse(ajaxData);
                        $("#idtest").val(data.data[0].id);
                        $('#image').attr('src', '{{url('storage/app/images')}}/' + data.data[0].image);
                        $("#typemodal").val(data.data[0].type);
                        $("#myModal").modal('show', {backdrop: 'true'});
                    }
                });
            });

            $(document).on("click", ".btn-remove", function () {
                count--;
                $("#countDoc").val(count);
                var button_id = $(this).attr("id");
                $("#row" + button_id + "").remove();
            })

            $("#logo").hide();
            $('#addImage').on('change', function (evt) {
                $('#partner').remove();
                $("#logo").show();
                var selectedImage = evt.currentTarget.files[0];
                var imageWrapper = document.querySelector('.image-wrapper');
                var theImage = document.createElement('img');
                imageWrapper.innerHTML = '';

                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                if (regex.test(selectedImage.name.toLowerCase())) {
                    if (typeof(FileReader) != 'undefined') {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            theImage.id = 'new-selected-image';
                            theImage.src = e.target.result;
                            imageWrapper.appendChild(theImage);
                        }
                        //
                        reader.readAsDataURL(selectedImage);
                    } else {
                        //-- Let the user knwo they cannot peform this as browser out of date
                        console.log('browser support issue');
                    }
                } else {
                    //-- no image so let the user knwo we need one...
                    $(this).prop('value', null);
                    console.log('please select and image file');
                }

            });

            $('#tableHistoryRecycle').dataTable({
                "aLengthMenu": [10, 50, 100]
            });

            $('#tableHistoryRating').dataTable({
                "aLengthMenu": [10, 50, 100]
            });

            $('#myModal').on('hidden.bs.modal', function () {
                location.reload();
            })

        });

        function saveChange(){
            var id = $('#idtest').val();
            var idScv = $('#idScv').val();
            var image = $('#addImage')[0].files[0];
            var type = $('#typemodal').val()

            form = new FormData();
            form.append('id',id);
            form.append('image',image);
            form.append('type',type);

            $.ajax({
                url     : "{{route('document.update','')}}/" + id,
                type : "POST",
                headers: { 'X-CSRF-Token' : $('meta[name="token"]').attr('content') },
                data : form,
                cache: false,
                contentType: false,
                processData: false,
                success : function(response){
                    window.location.href = "{{route('scavenger.detail','')}}/" + idScv;
                }
            })
            return false;
        }


        function myfn(myinput) {
            var name = $(myinput).attr("name");
            var id = $(myinput).attr("id");
            var val = $(myinput).val();
            debugger;
            switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
                case 'gif':
                case 'jpg':
                case 'png':
                case 'jpeg':
                    readURL(myinput);
                    break;
                default:
                    $(this).val('');
                    break;
            }
        }

        function readURL(myinput) {
            debugger;
            if (myinput.files && myinput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img_preview' + $(myinput).attr("id").replace('image', '')).attr('src', e.target.result);
                }
                reader.readAsDataURL(myinput.files[0]);
            }
            reader.hide(myinput.files[0]);
        }

        $('#example .search th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        });

        $("#rateYo").rateYo({
            rating    : @if($scavengers->rating == null)
                            {!!$scavengers->rating / 20!!}
                        @else
                            {!!$scavengers->rating->rating_avg / 20!!}
                        @endif,
            spacing   : "5px",
//             starWidth: "20px",
            readOnly: true,
            onInit: function (rating, rateYoInstance) {

                $(this).next().text(rating);

            },

//             multiColor: {
//
//               "startColor": "#FF0000", //RED
//               "endColor"  : "#F39C12"  //GREEN
//             }
        });

        $("#rateRecycle").rateYo({
            rating    : @if($scavengers->rating == null)
                            {!!$scavengers->rating / 20!!}
                        @else
                            {!!$scavengers->rating->rating / 20!!}
                        @endif,
            spacing   : "5px",
             starWidth: "20px",
            readOnly: true,
            onInit: function (rating, rateYoInstance) {

                $(this).next().text(rating);

            }
        });

    </script>
@endsection