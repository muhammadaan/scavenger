@extends('layouts.back-end.sidebar')

@section('content')
        <!-- Breadcrumb -->
<style>
    select[name="table-scavenger_length"] {
        width: 75px;
    }

    div[class="dataTables_length"] {
        width: 300px;
    }

    div[class="dataTables_filter"] {
        /*margin-top: 5px;*/
        margin-left: 90%;
    }

    div.dt-buttons{
        color: black;
        float: right;
        
    }

    button[class="dt-button buttons-excel buttons-html5"]{
    /*background-color: green;*/
      color: #ffffff;
      font-weight: bold;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
      background-image: -webkit-linear-gradient(top, #00cc00 0%, #00cc00 100%);
      background-image: -o-linear-gradient(top, #00cc00 0%, #00cc00 100%);
      background-image: linear-gradient(to bottom, #00cc00 0%, #00cc00 100%);
      /*background-color: #00cc00;*/
      background-image: -moz-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(rgb(0,146,69)), to(rgb(0,146,69)));
      background-image: -webkit-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: -o-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: linear-gradient(to bottom, rgb(0,146,69), rgb(0,146,69));
      background-repeat: repeat-x;
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff00cc00', endColorstr='#ffe34619', GradientType=0);
      border-color: #00cc00 #00cc00 #00cc00;
      border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
      background-color: rgb(0,146,69);
      /* Darken IE7 buttons by default so they stand out more given they won't have borders */
      filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
      width:50px;
      height: 30px;

    }

    button[class="dt-button buttons-pdf buttons-html5"]{
      color: #ffffff;
      font-weight: bold;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
      background-image: -webkit-linear-gradient(top, rgb(0,146,69) 0%, rgb(0,146,69) 100%);
      background-image: -o-linear-gradient(top, rgb(0,146,69) 0%, rgb(0,146,69) 100%);
      background-image: linear-gradient(to bottom, rgb(0,146,69) 0%, rgb(0,146,69) 100%);
      background-color: rgb(0,146,69);
      background-image: -moz-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(rgb(0,146,69)), to(rgb(0,146,69)));
      background-image: -webkit-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: -o-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: linear-gradient(to bottom, rgb(0,146,69), rgb(0,146,69));
      background-repeat: repeat-x;
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffff0000', endColorstr='#ffe34619', GradientType=0);
      border-color: #ff0000 #ff0000 #ff0000;
      border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
      /**background-color: #ff0000;*/
      background-color: rgb(0,146,69);
      /* Darken IE7 buttons by default so they stand out more given they won't have borders */
      filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
      margin-left: 10px;
      width:50px;
      height: 30px;
    }
</style>


<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>
            <li><a href="{{url('admin/scavenger-list')}}">Scavenger</a></li>
            <li class="active">List</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @if (session('status'))
            <div class="alert alert-success">
                <strong>{{ session('status') }}.</strong>
                <button type="button" class="close" data-dismiss="alert">×</button>

            </div>
        @endif
        <div class="block">
            <div class="header">
                <h2 style="color: rgb(0,146,69)">Scavenger List</h2>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" onclick="reloadPage()" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69)">Refresh</button>

                <div class="side pull-right">
                    <ul class="buttons">
                        <li class="btn-group">

                        </li>
                        <li><a href="#" class="block-toggle tip" title="Toggle content"><span
                                        class="icon-chevron-down"></span></a></li>

                    </ul>
                </div>
            </div>
            <hr>
            <div class="content">
                <table class="table table-bordered table-striped table-hover" id="table-scavenger">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Created At</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($scavengers as $index => $scavenger)
                        @if($scavenger->role_id == 2)
                            <tr class="clicklable-row" data-href="{{url('/admin/scavenger/detail', $scavenger->id)}}">
                                <td>{{$i++}}</td>
                                <td>{{$scavenger->name}}</td>
                                @if($scavenger->gender == 'L')
                                    <td>Male</td>
                                @elseif($scavenger->gender == null)
                                    <td>-</td>
                                @else
                                    <td>Female</td>
                                @endif
                                <td>{{$scavenger->email}}</td>
                                @if($scavenger->status == 'Verified' || $scavenger->status == 1)
                                    <td>Verified</td>
                                @elseif($scavenger->status == 'Unverified' || $scavenger->status == 0)
                                    <td>Unverified</td>
                                @else
                                    <td>-</td>
                                @endif
                                <td>{{\Carbon\Carbon::parse($scavenger->created_at)->format('d M Y - H:i:s')}}</td>
                            </tr>
                        @else
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script type="text/javascript">

        $('#table-scavenger').dataTable({
            "aLengthMenu": [10, 50, 100],
            "dom": 'lBfrtip',
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: 'Scavenger List'
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Scavenger List'
                },
              ],
        });

        jQuery(document).ready(function ($) {
            $("#table-scavenger").on('click', '.clicklable-row', function () {
                window.location = $(this).data("href");
            });
            $("#table-scavenger").css('cursor', 'pointer');
            $("#table-scavenger").hover(function () {
                $(this).css('cursor', 'pointer');
            });
        });

        {{--$(document).ready(function(){--}}
        //        var table = $('#example').DataTable({
        //            "aLengthMenu": [10,50,100],
        //        });

        {{--table.ajax.url('{{route("scavenger.ajax.getdata")}}').load();--}}
        {{--});--}}

        function reloadPage()
        {
            location.reload();
        }

    </script>
@endsection
