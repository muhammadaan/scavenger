 @extends('layouts.back-end.sidebar')

@section('content') 

 

<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/report-list')}}">Scavenger</a></li>                    
                    <li class="active">Edit</li>
                </ol>
            </div>
        </div>   


 <div class="row">
            <div class="col-md-12">
                
                <div class="block">
                    <div class="header">
                        <h2>Customer List</h2>
                    </div>
                {{-- <form id="validate" method="POST" action="#"> --}}
                <form id="validate" method="POST" action="{{ url('/admin/scavenger') }}/{{$data->id}}">
                {{ csrf_field() }}
                    <div class="content controls">
                        <div class="form-row">
                            <div class="col-md-3">Photo:</div>
                            <div class="col-md-9"><input type="file" id="poto" class="validate[required]"  name="foto" value="{{$data->name}}"/></div>
                        </div>
                         <div class="form-row">
                            <div class="col-md-3">Name:</div>
                            <div class="col-md-9"><input type="text" id="nama" class="validate[required]" name="name" value="{{$data->username}}"/></div>
                        </div>
                      <div class="form-row">
                            <div class="col-md-3">Username:</div>
                            <div class="col-md-9"><input type="text" id="usern" class="validate[required,custom[email]]" value="{{$data->email}}" name="username" /></div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3">Alamat:</div>
                            <div class="col-md-9">
                            {{-- <div class="col-md-9"><input type="email"  value="{{$data->email}}" name="alamat" /></div> --}}
                            <textarea name="alamat" id="alamats" class="validate[required,maxSize[500]]" id="" cols="20" rows="10"></textarea>
                            </div>
                        </div>


                         {{-- <div class="form-row">
                            <div class="col-md-3">Edit Password:</div>
                            <div class="col-md-9">
                                <div class="checkbox-inline">
                                    <label><input type="checkbox" name="check_ex1" id="checkpass" checked="checked"/> </label>
                                </div>                                
                            </div>
                        </div> --}}

                       {{--  <div class="form-row">
                            <div class="col-md-3">Gender:</div>
                            <div class="col-md-9">
                                <select class="form-control" name="roles" class="validate[required] form-control">
                                    @foreach(\App\Models\Admin::all() as $row)
                                    @if($row->id == $data->roles)
                                    <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                    @else
                                    <option value="{{$row->id}}" >{{$row->name}}</option>
                                    @endif
                                    @endforeach
                                    
                                    <option value="" ></option>
                                    <option value="1" >Laki-Laki</option>
                                    <option value="2" >Wanita</option>
                                </select>
                            </div>
                        </div> --}}
                        <div class="form-row">
                            <div class="col-md-3">Gender:</div>
                            <div class="col-md-9">                            
                                <select name="gender" id="gends" class="validate[required] form-control">
                                    <option value=""></option>
                                    <option value="1">Laki laki</option>
                                    <option value="2">Wanita</option>                                
                                                                                                           
                                </select>                            
                                <span class="help-block">Required</span>
                            </div>
                        </div>

                         <div class="form-row">
                            <div class="col-md-3">Date Of Birth:</div>
                            <div class="col-md-9"><input type="text" id="birth" name="date_of_birth" class="validate[required] datepicker form-control"  /></div>
                        </div>

                    <div class="form-row">
                                    <div class="col-md-3">E-mail:</div>
                                    <div class="col-md-9">
                                        <input type="text" name="email" id="mail" class="validate[required,custom[email]]"/>
                                        <span class="help-inline">Required, email</span>
                                    </div>
                                </div>
                        <div class="form-row">
                            <div class="col-md-3">Mobile Number:</div>
                            <div class="col-md-9"> <input type="number" id="mobs" name="mobile" class="validate[required,custom[number]] form-control"/></div>
                        </div>
                       <div class="form-row">
                            <div class="col-md-3">Status:</div>
                            <div class="col-md-9">                            
                                <select name="status" id="stats" class="validate[required] form-control">
                                    <option value=""></option>
                                    <option value="1">Verified</option>
                                    <option value="2">Unverified</option>                                
                                                                                                           
                                </select>                            
                                <span class="help-block">Required</span>
                            </div>
                        </div>
                           
                        <input type="hidden" name="created_by" value="{{Auth::user()->name}}">
                           
                    </div>
                     <div class="footer">
                        <div class="side pull-right">
                            {{-- <div class="btn-group">
                                
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div> --}}

                            <div class="btn-group">
                            {{-- <label class="input-group-addon"></label>                           --}}
                           <input type="submit" id="btnSubmit" staff="1" value="Edit" class="btn btn-primary"></input>
                            <input type="button" value="Cancel" id="btnTest" class="btn btn-danger"></input>
                        </div>
                        </div>
                    </div>    
                    </form>
                </div>       
            </div>
        </div>                

@endsection
@section('scripts')
    {{-- <script type="text/javascript">
           $('#form-password').show()
                $('#form-confpassword').show()
        $('#checkpass').click(function(){
               if(this.checked){
                // alert("checked")
                $('#form-password').show()
                $('#form-confpassword').show()
               }else{
                // alert("unchecked")
                $('#form-password').hide()
                $('#form-confpassword').hide()

               }     
        });

    </script> --}}
    <script>
    $(document).ready(function(){
     $("#btnTest").hide();
            $('#btnSubmit').click(function(e) {


                if($(this).attr("staff") == 1){
                    $(this).attr("staff",0)
                    e.preventDefault()
                }else{
                    $(this).unbind('click')
                }

                $(this).val('Save Staff');

                    $("#btnTest").show();
                        if($('#poto').prop('disabled') && $('#nama').prop('disabled') && $('#usern').prop('disabled') && $('#alamats').prop('disabled') && $('#gends').prop('disabled') && $('#birth').prop('disabled') && $('#mail').prop('disabled') && $('#mobs').prop('disabled') && $('#stats').prop('disabled'))
                    {
                        $('#poto').prop('disabled', false)
                        $('#nama').prop('disabled', false)
                        $('#usern').prop('disabled', false)
                        $('#alamats').prop('disabled', false)
                        $('#gends').prop('disabled', false)
                        $('#birth').prop('disabled', false)
                        $('#mail').prop('disabled', false)
                        $('#mobs').prop('disabled', false)
                        $('#stats').prop('disabled', false)
                       
                       
                    }
                        else{

                        $('#poto').prop('disabled', false)
                        $('#nama').prop('disabled', false)
                        $('#usern').prop('disabled', false)
                        $('#alamats').prop('disabled', false)
                        $('#gends').prop('disabled', false)
                        $('#birth').prop('disabled', false)
                        $('#mail').prop('disabled', false)
                        $('#mobs').prop('disabled', false)
                        $('#stats').prop('disabled', false)
                        
                       
                    }
                });

         
         $('#btnTest').click(function() {
                    
                    if($("#btnSubmit").attr('staff') == 0){

                         // $()
                         $("#btnSubmit").attr('staff',1)
                         $("#btnTest").hide();
                         $("#btnSubmit").val('Edit');
                         $('#poto').prop('disabled', true)
                        $('#nama').prop('disabled', true)
                        $('#usern').prop('disabled', true)
                        $('#alamats').prop('disabled', true)
                        $('#gends').prop('disabled', true)
                        $('#birth').prop('disabled', true)
                        $('#mail').prop('disabled', true)
                        $('#mobs').prop('disabled', true)
                        $('#stats').prop('disabled', true)
                     

                    }

                     
                     
                 
                });
         });

</script>
@endsection