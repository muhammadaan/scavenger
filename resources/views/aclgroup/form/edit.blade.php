@extends('layouts.back-end.sidebar')

@section('content')  
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>                    
            <li><a href="{{url('admin/users-group')}}">Configuration</a></li>                    
            <li><a href="#">User Group</a></li>
            <li class="active">Edit</li>
        </ol>
    </div>
</div>       

            
<div class="row">
    <div class="col-md-12">
        <form action="{{url('admin/users-group')}}/{{$data->id}}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="block">
            <div class="header">
                <button type="submit" class="btn btn-info pull-right"> <i class="icon-save"></i> Update Group</button>
            </div>
            <div class="content controls">
                
                <div class="form-row">
                    <div class="col-md-3">Group Name </div>
                    <div class="col-md-9"><input type="text" class="form-control" name="name" placeholder="insert group name here ..." value="{{$data->name}}" /></div>
                </div>
                <div class="form-row">
                    <div class="col-md-3">Group Desc </div>
                    <div class="col-md-9">
                        <textarea class="form-control" name="description" placeholder="insert group desc here ..." >{{$data->description}}</textarea>                        
                        </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3">Access Information </div>
                    <div class="col-md-9" class="scroll-y">
                        <?php 
                            $routeCollection = Route::getRoutes();
                            $new             = [];
                            $groups          = [];
                            foreach ($routeCollection as $value) {

                                if(($value->getName() != '') or (!$value)){

                                    if(strpos($value->getName(), 'front-user') === false){
                                        
                                        if (in_array(explode(".",$value->getName())[0], $groups)) {
                                            
                                        } else {

                                            array_push($groups, explode(".",$value->getName())[0]);
                                        }                            
                                        array_push($new, $value->getName());
                                    }
                                }                            
                            } 
                            asort($groups);
                            foreach ($groups as $key => $group) {
                                # code...
                                echo '<label class="switch"><input type="checkbox" class="skip" onclick="checkclass(\''.$group.'\')" id="'.$group.'"/><span></span><em> '.$group.'</em> </label></br>';
                                foreach ($routeCollection as $row) {
                                    if(explode(".",$row->getName())[0] == $group){

                                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="switch"><input type="checkbox" id="as'.str_replace(".","bb",$row->getName()).'" name="routelist[]" class="'.$group.' skip" value="'.$row->getName().'"/><span></span> '.$row->getName().'</label></br>';
                                    }
                                }
                                echo '</br>';
                            }
                            
                        ?>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

<script>
    function checkclass(data)
    {   
        if($( "#"+data ).is(':checked'))
        {                    
            $( "."+data ).not(this).prop( "checked", true );        
        } 
        else 
        {                    
            $( "."+data ).not(this).prop( "checked", false );        
        }        

    }

    @foreach (json_decode($data->route_access_list)->routelist as $listcheck)
        $( '#as{{str_replace(".","bb",$listcheck)}}' ).not(this).prop( "checked", true );                
    @endforeach


</script>
         
@endsection