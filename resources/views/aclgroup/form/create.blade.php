@extends('layouts.back-end.sidebar')

@section('content')  
<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>                    
            <li><a href="{{url('admin/users-group')}}">User Group</a></li>                    
            <li class="active">Add</li>
        </ol>
    </div>
</div>       

<div class="row">
    <div class="col-md-12">
        <form id="validate" action="{{url('admin/users-group')}}" method="post">
        {{ csrf_field() }}
        <div class="block">
            <div class="header">
                <button type="submit" class="btn btn-info pull-right" style="background-color: rgb(0,146,69);"> <i class="icon-save"></i> Submit Group</button>
            </div>
            <div class="content controls">
                
                <div class="form-row">
                    <div class="col-md-3">Group Name* </div>
                    <div class="col-md-9"><input type="text" class="validate[required,maxSize[20]] form-control" name="name" placeholder="insert group name here ..." /></div>
                </div>
                <div class="form-row">
                    <div class="col-md-3">Group Desc </div>
                    <div class="col-md-9">
                        <textarea class="form-control" name="desc" class="validate[required,minSize[10]] form-control" placeholder="insert group desc here ..." ></textarea>    
                        </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3">Access Information* </div>
                    <div class="col-md-9">
                        <?php 
                            $routeCollection = Route::getRoutes();
                            $new             = [];
                            $groups          = [];
                            foreach ($routeCollection as $value) {

                                if(($value->getName() != '') or (!$value)){
                                    if(strpos($value->getName(), 'front-user') === false){
                                        
                                        if (in_array(explode(".",$value->getName())[0], $groups)) {
                                            
                                        } else {

                                            array_push($groups, explode(".",$value->getName())[0]);
                                        }                            
                                        array_push($new, $value->getName());
                                    }
                                }                            
                            } 
                            asort($groups);
                            foreach ($groups as $key => $group) {
                                # code...
                                echo '<label class="switch"><input type="checkbox" class="skip" onclick="checkclass(\''.$group.'\')" id="'.$group.'"/><span></span><em> '.$group.'</em> </label></br>';
                                foreach ($routeCollection as $row) {
                                    if(explode(".",$row->getName())[0] == $group){

                                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="switch"><input type="checkbox" name="routelist[]" class="'.$group.' skip" value="'.$row->getName().'"/><span></span> '.$row->getName().'</label></br>';
                                    }
                                }
                                echo '</br>';
                            }
                            
                        ?>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

<script>
    function checkclass(data)
            {   
                if($( "#"+data ).is(':checked'))
                {                    
                    $( "."+data ).not(this).prop( "checked", true );        
                } 
                else 
                {                    
                    $( "."+data ).not(this).prop( "checked", false );        
                }        

            }


</script>
         
@endsection