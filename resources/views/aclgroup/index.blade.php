            
@extends('layouts.back-end.sidebar')

@section('content')

    <style>
        select[name="example_length"] { width: 75px; }
        div[class="dataTables_length"] { width:300px; }
    </style>


    <div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('/admin/home')}}">Home</a></li>                    
            <li><a href="{{url('admin/users-group')}}">Group User</a></li>                    
            <li class="active">List</li>
        </ol>
    </div>
</div>       

<div class="row">
    <div class="col-md-12">
        
        <div class="block">
            <div class="header">
                <div class="row">
                    <div class="col-md-10">
                        <a href="{{url('admin/users-group/create')}}"><button type="button" class="btn btn-info" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69) ;"> <i class="icon-plus"></i> Add New Group User</button></a>
                    </div>
                </div>
            </div>
            
            <div class="content">
                   <table class="table table-bordered table-striped table-hover" id="example">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>         
                    </tbody>
                </table>                     
            </div> 
        </div>                
        
    </div>
</div>
         
@endsection

@section('scripts')

<script type="text/javascript">
    // $('select[name="example_length"]').css('width', '100px')

    getData()
    function getData(){
        var table = $('#example').DataTable();
        table.ajax.url( "{{route('users-group')}}" ).load();
    }
</script>
@endsection