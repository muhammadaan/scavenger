 @extends('layouts.back-end.sidebar')

@section('content') 

 

<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/customer-list')}}">Customer</a></li>
                    <li class="active">Edit</li>
                </ol>
            </div>
        </div>   


 <div class="row">
            <div class="col-md-12">
                
                <div class="block">
                    <div class="header">
                        <h2 style="color: rgb(0,146,69)">Customer List</h2>
                    </div>
                <form id="validate" method="POST" action="{{ url('/admin/donation') }}/{{$data->id}}">
                {{ csrf_field() }}
                    <div class="content controls">

                        <div class="form-row">
                            <div class="col-md-3">Photo:</div>
                            <div class="col-md-9"><input type="file" id="poto" class="validate[required]"  name="foto" value="{{$data->image}}"/></div>
                        </div>

                         <div class="form-row">
                            <div class="col-md-3">Name:</div>
                            <div class="col-md-9"><input type="text" id="nama" class="validate[required]" name="name" value="{{$data->name}}"/></div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-3">Username:</div>
                            <div class="col-md-9"><input type="text" id="usern" class="validate[required]" value="{{$data->username}}" name="username" /></div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-3">Gender:</div>
                            <div class="col-md-9">                            
                                <select name="gender" id="gends" class="validate[required] form-control">

                                    @if($data->gender == 'L')
                                        <option value="L">Male</option>
                                        <option value="P">Female</option>
                                    @elseif($data->gender == 'P')
                                        <option value="P">Femlae</option>
                                        <option value="L">Male</option>
                                    @else
                                        <option></option>
                                        <option value="L">Male</option>
                                        <option value="P">Female</option>
                                    @endif
                                                                                                           
                                </select>                            
                                {{--<span class="help-block">Required</span>--}}
                            </div>
                        </div>

                         <div class="form-row">
                            <div class="col-md-3">Date Of Birth:</div>
                            <div class="col-md-9">
                                <input type="text" id="birth" name="date_of_birth" class="validate[required] form-control" value="{{\Carbon\Carbon::parse($data->date_of_birth)->format('d M Y')}}">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-3">E-mail:</div>
                            <div class="col-md-9">
                                <input type="text" name="email" id="mail" class="validate[required,custom[email]]" value="{{$data->email}}">
                                {{--<span class="help-inline">Required, email</span>--}}
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-3">Mobile Number:</div>
                            <div class="col-md-9">
                                <input type="number" id="mobs" name="mobile" class="validate[required,custom[number]] form-control" value="{{$data->mobile}}">
                            </div>
                        </div>

                       <div class="form-row">
                            <div class="col-md-3">Status:</div>
                            <div class="col-md-9">                            
                                <select name="status" id="stats" class="validate[required] form-control">

                                    @if($data->status == 1)
                                        <option value="1">Verified</option>
                                    @else
                                        <option></option>
                                        <option value="1">Verified</option>
                                        <option value="0">Unverified</option>
                                    @endif
                                                                                                           
                                </select>                            
                                {{--<span class="help-block">Required</span>--}}
                            </div>
                        </div>
                           
                        <input type="hidden" name="created_by" value="{{Auth::user()->name}}">
                           
                    </div>
                     <div class="footer">
                        <div class="side pull-right">
                            {{-- <div class="btn-group">
                                
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div> --}}

                            <div class="btn-group">
                            {{-- <label class="input-group-addon"></label>                           --}}
                           <input type="submit" id="btnSubmit" staff="1" value="Edit" class="btn btn-primary" style="background-color: rgb(0,146,69)">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                            <input type="button" value="Cancel" id="btnTest" class="btn btn-danger">
                        </div>
                        </div>
                    </div>    
                    </form>
                </div>       
            </div>
        </div>                

@endsection
@section('scripts')
    <script>

        $(function() {
            $('#birth').datepicker({
                dateFormat : 'd M yy'
            });
        })

        $(document).ready(function(){
            $("#btnTest").hide();
                $('#btnSubmit').click(function(e) {


                    if($(this).attr("staff") == 1){
                        $(this).attr("staff",0)
                        e.preventDefault()
                    }else{
                        $(this).unbind('click')
                    }

                    $(this).val('Save Staff');

                        $("#btnTest").show();
                            if($('#poto').prop('disabled') && $('#nama').prop('disabled') && $('#usern').prop('disabled') && $('#gends').prop('disabled') && $('#birth').prop('disabled') && $('#mail').prop('disabled') && $('#mobs').prop('disabled') && $('#stats').prop('disabled'))
                        {
                            $('#poto').prop('disabled', false)
                            $('#nama').prop('disabled', false)
                            $('#usern').prop('disabled', false)

                            $('#gends').prop('disabled', false)
                            $('#birth').prop('disabled', false)
                            $('#mail').prop('disabled', false)
                            $('#mobs').prop('disabled', false)
                            $('#stats').prop('disabled', false)


                        }
                            else{

                            $('#poto').prop('disabled', false)
                            $('#nama').prop('disabled', false)
                            $('#usern').prop('disabled', false)

                            $('#gends').prop('disabled', false)
                            $('#birth').prop('disabled', false)
                            $('#mail').prop('disabled', false)
                            $('#mobs').prop('disabled', false)
                            $('#stats').prop('disabled', false)


                        }
                    });

            $('#btnTest').click(function() {
                if($("#btnSubmit").attr('staff') == 0){

                    $("#btnSubmit").attr('staff',1)
                    $("#btnTest").hide();
                    $("#btnSubmit").val('Edit');
                    $('#poto').prop('disabled', true)
                    $('#nama').prop('disabled', true)
                    $('#usern').prop('disabled', true)
                    $('#gends').prop('disabled', true)
                    $('#birth').prop('disabled', true)
                    $('#mail').prop('disabled', true)
                    $('#mobs').prop('disabled', true)
                    $('#stats').prop('disabled', true)
                }
            });
        });

</script>
@endsection