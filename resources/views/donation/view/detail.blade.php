@extends('layouts.back-end.sidebar')

@section('content')
        <!-- Breadcrumb -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>
            <li><a href="{{url('admin/donation/detail',$donationById->id)}}">Donation Detail</a></li>
            <li class="active">List</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="block" style="background-color: linear-gradient(to bottom right, rgb(0,146,69), rgb(140,198,63))">
            <div class="header">
                <h2 style="color: rgb(0,146,69)">Donation Detail</h2>
            </div>

            <div class="content">
                <div class="row">
                    <div class="col-md-12">

                        <div class="block">
                            <div class="header">

                                <div class="col-md-11">
                                    <h2 style="color: rgb(0,146,69)">Donation No {{$donationById->id}}</h2>
                                </div>
                                <div class="col-md-1">
                                    <h2 style="color: rgb(0,146,69)">
                                        @if($donationById->status == '1')
                                            Verified
                                        @else
                                            Unverified
                                        @endif
                                    </h2>
                                </div>

                            </div>
                            <div class="content">

                                <div class="form-group col-md-12">
                                    <div class="col-md-3">Name</div>
                                    <div class="col-md-6">{{$donationById->user->first_name}}</div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-md-3">Amount (Rp)</div>
                                    <div class="col-md-6">{{$donationById->amount}}</div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-md-3">Payment Method</div>
                                    <div class="col-md-6">{{$donationById->payment_method}}</div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-md-3">Donation Date</div>
                                    <div class="col-md-6">{{\Carbon\Carbon::parse($donationById->created_at)->format('d M Y')}}</div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-md-3">Recycle Number</div>
                                    <div class="col-md-6">{{$donationById->request_id > 0 ? $donationById->request_id : '-'}}</div>
                                </div>

                                <div class="form-group col-md-12"></div>
                                <div class="form-group col-md-12"></div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="block">
                                            <div class="header">
                                                <h2 style="color: rgb(0,146,69)">Donation Confirmation</h2>
                                            </div>
                                            <div class="content">

                                                <div class="form-row">
                                                    <div class="col-md-6">Bank Account Name</div>
                                                    <div class="col-md-6">
                                                        @if($donationById->confirmation_donations == null || $donationById->confirmation_donations->bank_account_name == null)
                                                            <div class="col-md-6">-</div>
                                                        @else
                                                            <div class="col-md-6">{{$confirmDonation->bank_account_name}}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6">Register Name</div>
                                                    <div class="col-md-6">
                                                        @if($donationById->confirmation_donations == null || $donationById->confirmation_donations->bank_account_name == null)
                                                            <div class="col-md-6">-</div>
                                                        @else
                                                            <div class="col-md-6">{{$confirmDonation->register_name}}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6">Bank Account Number</div>
                                                    <div class="col-md-6">
                                                        @if($donationById->confirmation_donations == null || $donationById->confirmation_donations->bank_account_name == null)
                                                            <div class="col-md-6">-</div>
                                                        @else
                                                            <div class="col-md-6">{{$confirmDonation->bank_account_number}}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6">Transfer To</div>
                                                    <div class="col-md-6">
                                                        @if($donationById->confirmation_donations == null || $donationById->confirmation_donations->bank_account_name == null)
                                                            <div class="col-md-6">-</div>
                                                        @else
                                                            <div class="col-md-6">{{$confirmDonation->transfer_to}}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6">Amount Of Donation</div>
                                                    <div class="col-md-6">
                                                        @if($donationById->confirmation_donations == null || $donationById->confirmation_donations->bank_account_name == null)
                                                            <div class="col-md-6">-</div>
                                                        @else
                                                            <div class="col-md-6">{{$confirmDonation->amount_of_donation}}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6">Note</div>
                                                    <div class="col-md-6">
                                                        @if($donationById->confirmation_donations == null || $donationById->confirmation_donations->bank_account_name == null)
                                                            <div class="col-md-6">-</div>
                                                        @else
                                                            <div class="col-md-6">{{$confirmDonation->note}}</div>
                                                        @endif
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="block">
                                            <div class="header">
                                                <h2 style="color: rgb(0,146,69)">Update Status</h2>
                                            </div>
                                            <div class="content">
                                                <br><form method="post" action="{{url('/admin/donation', $donationById->id)}}" id="editDonation" name="editDonation">
                                                {{csrf_field()}}
                                                {{method_field('PUT')}}

                                                    <div class="form-row">
                                                        <div class="col-md-4">Status</div>
                                                        <div class="col-md-3">
                                                            <select class="form-control" id="status" name="status">
                                                                @if($donationById->status == "1")
                                                                    <option readonly value="1">Verified</option>
                                                                @else
                                                                    <option value="0">Unverified</option>
                                                                    <option value="1">Verified</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-4">Note</div>
                                                        <div class="col-md-8">
                                                            <textarea class="form-control" name="note" id="note" rows="3">{{$donationById->note}}</textarea>
                                                        </div>
                                                    </div>
                                                     <span style="float: right; margin-top: 79px">
                                                         <a id="submitform" class="btn btn-info btn-rounded" onclick="showModal();" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69);">
                                                             Update Status
                                                         </a>
                                                         <button type="reset" class="btn btn-rounded" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69);">
                                                             <a style="color: white">
                                                                 Cancel
                                                             </a>
                                                         </button>
                                                     </span>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade" id="test">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4>Apakah anda yakin</h4>
                                            </div>
                                            <div class="modal-body">
                                                {{csrf_field()}}
                                                <button class="btn btn-primary btn-cons" onclick="updateStatus({{$donationById->id}})">
                                                    Yes
                                                </button>
                                                <button class="btn btn-danger btn-cons" data-dismiss="modal">
                                                    No
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script type="text/javascript">
        function showModal(){
            $('#submitform').click(function(){
                $('#test').modal('show');
            });
        }

        function updateStatus(id){
            $.ajax({
                url : "{{route('donation.donation.update', $donationById->id)}}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type : "POST",
                data : {
                    'status' : $('#status').val(),
                    'note' : $('#note').val(),
                },
                success: function(result) {
                    window.location.href = "{{route('donation.list')}}";
                }
            });
        };
    </script>
@endsection
