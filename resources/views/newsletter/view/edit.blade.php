 @extends('layouts.back-end.sidebar')

@section('content') 

<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/users-list')}}">Users</a></li>                    
                    <li class="active">List</li>
                </ol>
            </div>
        </div>   


 <div class="row">
            <div class="col-md-12">
                
                <div class="block">
                    <div class="header">
                        <h2 style="color: rgb(0,146,69)">Edit Newsletter</h2>
                    </div>
                <form id="validate" method="POST" action="{{ url('admin/newsletter') }}/{{$data->id}}">
                {{ csrf_field() }}
                    <div class="content controls">
                        <div class="form-row">
                            <div class="col-md-3">Newsletter Title:</div>
                            <div class="col-md-9"><input type="text"  class="validate[required]"  name="title" value="{{$data->title}}" /></div>
                        </div>
                         <div class="form-row">
                            <div class="col-md-3">Newsletter Role:</div>
                            <div class="col-md-9">
                               <select class="form-control" name="category" class="validate[required] form-control">
                                    <option value="1"  <?php if($data->category == 1){ echo"selected";} ?>>Scavenger</option>
                                    <option value="2"  <?php if($data->category == 2){ echo"selected";} ?>>Customer</option>
                                    <option value="3"  <?php if($data->category == 3){ echo"selected";} ?>>All</option>
                                </select>
                            </div>
                        </div>  
                        <div class="form-row">
                            <div class="col-md-3">Newsletter Status:</div>
                            <div class="col-md-9">
                               <select class="form-control" name="status" class="validate[required] form-control">
                                    <option value="1"  <?php if($data->status == 1){ echo"selected";} ?>>Active</option>
                                    <option value="2"  <?php if($data->status == 2){ echo"selected";} ?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3">Start Date</div>
                            <div class="col-md-3">
                                <input type="text" class="validate[required] form-control" name="mulai_dari" id="mulai_dari" value="{{$data->mulai_dari}}">
                            </div>

                            <div class="col-md-3">End Date</div>
                            <div class="col-md-3">
                                <input type="text" class="validate[required] form-control" name="sampai_dengan" id="sampai_dengan" value="{{$data->sampai_dengan}}">
                            </div>
                        </div>
                        <div class="form-row">
                              <div class="col-md-3">Newsletter Descriptions:</div>
                              <div class="col-md-9">
                                <textarea class="tmce" name="desc">
                                {{$data->desc}}
                                </textarea>
                             </div>
                        </div>
                    </div>
                     <div class="footer">
                        <div class="side pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default" type="button" onClick="$('#validate').validationEngine('hide');" style="color: rgb(0,146,69); border-color:rgb(0,146,69); ">Hide prompts</button>
                                <button class="btn btn-success" type="submit" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69);">Submit</button>
                            </div>
                        </div>
                    </div>    
                    </form>
                </div>       
            </div>
        </div>                
@push('scripts')
    <script type="text/javascript">
        $(function() {
            $('#mulai_dari').datepicker();
            $('#sampai_dengan').datepicker();
        });
    </script>
@endpush
@endsection
