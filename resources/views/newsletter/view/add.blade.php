 @extends('layouts.back-end.sidebar')

@section('content') 
@push('css')
<style type="text/css">
    
        .image-wrapper {
          padding: 5px;
          border: 1px #ddd solid;
          height auto;
          width: 200px;
        }

        .image-wrapper img {
          max-width: 200px;
        }
</style>
@endpush
<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/users-list')}}">Users</a></li>                    
                    <li class="active">List</li>
                </ol>
            </div>
        </div>   


 <div class="row">
            <div class="col-md-12">
                
                <div class="block">
                    <div class="header">
                        <h2>Add New Newsletter</h2>
                    </div>
                <form id="validate" method="POST" action="{{ url('admin/newsletter-add') }}">
                {{ csrf_field() }}
                    <div class="content controls">
                        <div class="form-row">
                            <div class="col-md-3">Newsletter Title:</div>
                            <div class="col-md-9"><input type="text"  class="validate[required]"  name="title" /></div>
                        </div>
                         <div class="form-row">
                            <div class="col-md-3">Newsletter Role:</div>
                            <div class="col-md-9">
                               <select class="form-control" name="category" class="validate[required] form-control">
                                    <option value="1">Scavenger</option>
                                    <option value="2">Customer</option>
                                    <option value="3">All</option>
                                </select>
                            </div>
                        </div>  
                        <div class="form-row">
                            <div class="col-md-3">Newsletter Status:</div>
                            <div class="col-md-9">
                               <select class="form-control" name="status" class="validate[required] form-control">
                                    <option value="1">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-3">Starting From</div>
                            <div class="col-md-3">
                                <input type="date" name="mulai_dari" id="mulai_dari" class="validate[required] form-control">
                            </div>
                            <div class="col-md-3">Up To</div>
                            <div class="col-md-3">
                                <input type="date" name="mulai_dari" id="sampai_dengan" class="validate[required] form-control">
                            </div>
                        </div>
                        <div class="form-row">
                              <div class="col-md-3">Newsletter Descriptions:</div>
                              <div class="col-md-9">
                                <textarea class="tmce" name="desc">
                                </textarea>
                             </div>
                        </div>
                    </div>
                     <div class="footer">
                        <div class="side pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default" type="button" onClick="$('#validate').validationEngine('hide');">Hide prompts</button>
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div>
                        </div>
                    </div>    
                    </form>
                </div>       
            </div>
        </div>                
@push('scripts')
    <script type="text/javascript">
      
    </script>
@endpush
@endsection
