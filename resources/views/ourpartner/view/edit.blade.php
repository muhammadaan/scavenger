 @extends('layouts.back-end.sidebar')

@section('content') 
@push('css')
<style type="text/css">
    
        .image-wrapper {
          padding: 5px;
          border: 1px #ddd solid;
          height auto;
          width: 200px;
        }

        .image-wrapper img {
          max-width: 200px;
        }
</style>
@endpush
<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/users-list')}}">Users</a></li>                    
                    <li class="active">List</li>
                </ol>
            </div>
        </div>   


 <div class="row">
            <div class="col-md-12">
                
                <div class="block">
                    <div class="header">
                        <h2 style="color: rgb(0,146,69);">Add New Partner</h2>
                    </div>
                <form id="validate" method="POST" action="{{ url('admin/partner') }}/{{$data->id}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="content controls">
                        <div class="form-row">
                            <div class="col-md-3">Partner Name:</div>
                            <div class="col-md-9"><input type="text"  class="validate[required]"  name="name" value="{{ $data->name}}" /></div>
                        </div>
                         <div class="form-row">
                            <div class="col-md-3">Partner Desc:</div>
                            <div class="col-md-9">
                            <textarea name="desc" class="validate[required,minSize[5],maxSize[500]]">{{ $data->desc}}</textarea>
                            </div>
                        </div>
                      <div class="form-row">
                            <div class="col-md-3">Logo Partner:</div>
                            <div class="col-md-9">
                            <div class="input-group file">                                    
                                    <input type="text" class="form-control"/>
                                    <input type="file" name="image" id="addImage"/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button" style="background-color: rgb(50,205,50)">Browse</button>
                                    </span>
                                </div>         
                            </div>
                        </div> 
                      <div class="form-row" id="logo">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                               <div class="image-wrapper">      
                                </div>     
                            </div>
                        </div> 

                        <div class="form-row" id="partner">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                               <div class="image-wrapper">
                                    <img src="{{ url('storage/app/images') }}/{{$data->logo}}" >
                                </div>     
                            </div>
                        </div>
                           
                    </div>
                     <div class="footer">
                        <div class="side pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default" type="button" onClick="$('#validate').validationEngine('hide');" style="border-color: rgb(0,146,69);">Hide prompts</button>
                                <button class="btn btn-success" type="submit" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69)">Submit</button>
                            </div>
                        </div>
                    </div>    
                    </form>
                </div>       
            </div>
        </div>                
@push('scripts')
    <script type="text/javascript">
        $("#logo").hide();
        $('#addImage').on('change', function(evt) {
            $('#partner').remove();
            $("#logo").show();
          var selectedImage = evt.currentTarget.files[0];
          var imageWrapper = document.querySelector('.image-wrapper');
          var theImage = document.createElement('img');
          imageWrapper.innerHTML = '';
         
          var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
          if (regex.test(selectedImage.name.toLowerCase())) {
            if (typeof(FileReader) != 'undefined') {
              var reader = new FileReader();
              reader.onload = function(e) {
                  theImage.id = 'new-selected-image';
                  theImage.src = e.target.result;
                  imageWrapper.appendChild(theImage);
                }
                //
              reader.readAsDataURL(selectedImage);
            } else {
              //-- Let the user knwo they cannot peform this as browser out of date
              console.log('browser support issue');
            }
          } else {
            //-- no image so let the user knwo we need one...
            $(this).prop('value', null);
            console.log('please select and image file');
          }

        });

    </script>
@endpush
@endsection
