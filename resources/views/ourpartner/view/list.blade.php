@extends('layouts.back-end.sidebar')

@section('content') 
 <!-- Breadcrumb -->
<style>
    select[name="example_length"] { width: 75px; }
    div[class="dataTables_length"] { width:300px; }
</style>



<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/partner-list')}}">Partner</a></li>
                    <li class="active">List</li>
                </ol>
            </div>
        </div>    

        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                    <strong>{{ session('status') }}.</strong>
                            <button type="button" class="close" data-dismiss="alert">×</button>
                        
                    </div>
                 @endif
                <div class="block">
                    <div class="header">
                        <h2 style="color: rgb(0,146,69)">Partner List</h2>
                        <div class="side pull-right">                            
                            <ul class="buttons">
                                <li class="btn-group">                                    
                                                                                                               
                                </li>
                                <li><a href="#" class="block-toggle tip" title="Toggle content"><span class="icon-chevron-down"></span></a></li>
                               
                            </ul>
                        </div>                        
                    </div>
                    <hr>
                    <div class="content">
                          <div class="col-md-2">
                                <a href="{{ url('admin/partner-add') }}"  class="btn btn-default btn-block btn-clean">New Partner</a>
                            </div>
                           <table class="table table-bordered table-striped table-hover" id="example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Partner Name</th>
                                    <th>Partner Desc</th>
                                    <th>Logo</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>         
                            </tbody>
                        </table>                     
                    </div>   
                </div> 
            </div>
        </div>
@endsection
@section('scripts')

<script type="text/javascript">
            $(document).ready(function(){
             var table = $('#example').DataTable();
            table.ajax.url('{{route("partner.list")}}').load();
        });
</script>
@endsection

