@extends('layouts.back-end.sidebar')

@section('content')  
            <div class="col-md-3">

                <div class="block block-drop-shadow" style="cursor: pointer;" onclick="window.location.href='{{route('customer.list')}}'">
                    <div class="head bg-dot20">
                                
                        <div class="head-panel nm">
                            <div class="hp-info pull-left">      
                                <h2>User</h2>                                                          
                            </div>
                            <div class="hp-info pull-left">                                
                            </div>                                                        
                            <div class="hp-info pull-right" style="margin-right: 10%;color: white; ">
                                <div class="hp-icon">                                    
                                </div>                                                 
                                <span class="hp-sm">Active User</span>                                
                                <span class="hp-main"><b>{{\App\Models\User::whereRoleId(1)->whereStatus(1)->count()}}</b></span>                                
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="block block-drop-shadow" style="cursor: pointer;" onclick="window.location.href='{{route('scavenger.list')}}'">
                    <div class="head bg-dot20">
                                
                        <div class="head-panel nm">
                            <div class="hp-info pull-left">      
                                <h2>Drive</h2>                                                          
                            </div>
                            <div class="hp-info pull-left">                                
                            </div>                                                        
                            <div class="hp-info pull-right" style="margin-right: 10%;color: white;">
                                <div class="hp-icon">                                    
                                </div>                                                 
                                <span class="hp-sm">Active Driver</span>                                
                                <span class="hp-main"><b>{{\App\Models\User::whereRoleId(2)->whereStatus(1)->count()}}</b></span>                                
                            </div>                            
                        </div>
                    </div>
                </div>
                <div class="block block-drop-shadow" style="cursor: pointer;" onclick="window.location.href='{{route('request.list')}}'">
                    <div class="head bg-dot20">
                                
                        <div class="head-panel nm">
                            <div class="hp-info pull-left">      
                                <h2>Recycle</h2>                                                          
                            </div>
                            <div class="hp-info pull-left">                                
                            </div>                                                        
                            <div class="hp-info pull-right" style="margin-right: 10%;color: white;">
                                <div class="hp-icon">                                    
                                </div>                                                 
                                <span class="hp-sm">Total Waste</span>                                
                                <span class="hp-main"><b>{{\App\Models\Request::whereStatus(3)->sum('berat_realisasi')}}</b></span>                                
                            </div>                            
                        </div>
                    </div>
                </div>  
                <div class="block block-drop-shadow" style="cursor: pointer;" onclick="window.location.href='{{route('donation.list')}}'">
                    <div class="head bg-dot20">
                                
                        <div class="head-panel nm">                            
                            <span class="hp-sm" style="color: white;">
                                <span class="icon-download"></span> Total Donation
                            </span>                                
                            <span class="hp-sm pull-right" style="margin-right: 10%;color: white;"><b>{{number_format(\App\Models\Donation::whereStatus(1)->sum('amount'),2,',','.')}}</b></span>
                        </div>
                    </div>
                </div>                            
            </div>
            
            <div class="col-md-5">

                <div class="block block-drop-shadow">
                    <div class="head bg-dot20">
                                
                        <div class="head-panel nm">                            
                            <span class="hp-sm" style="color: white;">
                                <span class="icon-ok"></span> Complete Request
                            </span>                                
                            <span class="hp-sm pull-right" style="margin-right: 10%;color: white;"><b>{{\App\Models\Request::whereStatus(2)->count()}}</b></span>
                        </div>
                    </div>
                </div>
                <div class="block block-drop-shadow">
                    <div class="head bg-dot20">
                                
                        <div class="head-panel nm">                            
                            <span class="hp-sm" style="color: white;">
                                <span class="icon-remove"></span> Cancelled Request
                            </span>                                
                            <span class="hp-sm pull-right" style="margin-right: 10%;color: white;"><b>{{\App\Models\Request::whereStatus(4)->count()}}</b></span>
                        </div>
                    </div>
                </div>
            
                <div class="block block-drop-shadow">
                    <div class="head bg-dot20">
                        <h2 style="padding-left: 35%">Recycle Graphics</h2>                        
                        <div class="head-panel nm">                            
                            <div class="chart" id="dash_chart_custome" style="height: 200px;"></div>
                        </div>                        
                    </div>
                </div> 

            </div>
            
            <div class="col-md-4">
                
                <div class="block block-drop-shadow">
                    <div class="head bg-dot20">

                        <h2 style="padding-left: 35%">5 Top User</h2>

                        <?php $batas = 0; ?>
                        @foreach(\App\Http\Controllers\Rank\RankUserController::queryranktop() as $row)
                                
                        <div class="head-panel nm">
                            <div class="block block-drop-shadow">
                                <div class="head bg-dot110">                                            
                                    <div class="head-panel nm" style="cursor: pointer;" onclick="window.location.href='{{route('customer.edit', $row->id)}}'">
                                        <div class="hp-info pull-left" style="margin-right: 10%">
                                            <div class="hp-icon">
                                                <span class="icon-user"></span>
                                            </div>                                                 
                                            <span class="hp-sm">Name : {{$row->name}} </span> 
                                            <span class="hp-sm">Total Recycle : {{$row->total}}</span>                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                        <?php $batas++; if($batas == 5){break;} ?>
                        @endforeach

                    </div>
                </div>

                <div class="block block-drop-shadow">
                    <div class="head bg-dot20">

                        <h2 style="padding-left: 35%">5 Top Driver</h2>

                        <?php $batas = 0; ?>
                        @foreach(\App\Http\Controllers\Rank\RankUserController::queryranktopscav() as $row)
                                
                        <div class="head-panel nm">
                            <div class="block block-drop-shadow">
                                <div class="head bg-dot110">                                            
                                    <div class="head-panel nm" style="cursor: pointer;" onclick="window.location.href='{{route('scavenger.edit', $row->id)}}'">
                                        <div class="hp-info pull-left" style="margin-right: 10%">
                                            <div class="hp-icon">
                                                <span class="icon-user"></span>
                                            </div>                                                 
                                            <span class="hp-sm">Name : {{$row->name}} </span> 
                                            <span class="hp-sm">Total Recycle : {{$row->total}} Kg.</span>                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                        <?php $batas++; if($batas == 5){break;} ?>
                        @endforeach

                    </div>
                </div>
            </div>            
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
       if($("#dash_chart_custome").length > 0){
        xhr_custome('{{route("get_chartweight")}}')
            .then(function (response) {                        
                var dash_chart_custome = $.plot($("#dash_chart_custome"),
                                    JSON.parse(response),
                                    { 
                                     series: {lines: { show: true }, points: { show: true }},
                                     grid: { hoverable: true, clickable: true, margin: {left: 0}},
                                     xaxis: {ticks: [[1,'Jan'],[2,'Feb'],[3,'Mar'],[4,'Apr'],[5,'May'],[6,'Jun'],[7,'Jul'],[8,'Aug'],[9,'Sep'],[10,'Oct'],[11,'Nov'],[12,'Dec']]},
                                     legend: {show: true}});
            })
            .catch(function (error) {
                console.log('Something went wrong', error);
            });
       }   


    })
</script>
@endsection
