@extends('layouts.back-end.sidebar')

@section('content') 
 <!-- Breadcrumb -->


<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('admin/category-list')}}">Recycle</a></li>                    
                    <li class="active">Request Report</li>
                </ol>
            </div>
        </div>    

        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                    <strong>{{ session('status') }}.</strong>
                            <button type="button" class="close" data-dismiss="alert">×</button>
                        
                    </div>
                 @endif
                <div class="block">
                    <div class="header">
                        <h2>REQUEST REPORT</h2>
                        <div class="side pull-right">                            
                            <ul class="buttons">
                                <li class="btn-group">                                    
                                                                                                            
                                </li>
                                <li><a href="#" class="block-toggle tip" title="Toggle content"><span class="icon-chevron-down"></span></a></li>
                               
                            </ul>
                        </div>                        
                    </div>
                    <hr>
                    <div class="content">
                        <div class="col-md-12">
                
                <div class="block">
                    <div class="header" align="center">REQUEST REPORT
                       
                    </div>
                    
                </div>                
            

           
                        </div>                          
                            </br>
                            </br>
                            </br>
                            <div class="examples">
                                <table class="table table-bordered table-striped table-hover" id="example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Request Number</th>
                                    <th>Request Date</th>                                    
                                    <th>Weight </th>
                                    <th>Donation </th>
                                    <th>Status </th>
                                    
                                </tr>
                            </thead>
                            <tbody> 
                             @foreach($test as $i=>$row)
                          <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{$row->id or 'Data Empty'}}</td>                            
                            <td>{{$row->order_date or 'Data Empty'}}</td>
                            <td>{{$row->detail->weight_estimate or 'Data Empty'}}</td>
                            <td>{{$row->donation->amount or 'Data Empty'}}</td>
                            <td>{{$row->history->status or 'Data Empty'}}</td>
                          </tr>  
                            @endforeach        
                            </tbody>
</table> 
                            </div>                  
                    </div>   
                </div> 
            </div>
        </div>
@endsection
@section('scripts')

<script type="text/javascript">
        //     $(document).ready(function(){
        //      var table = $('#example').DataTable();
        //     table.ajax.url('{{route("category.ajax.getdata")}}').load();
        // });

     $('#example').dataTable({
        // "aLengthMenu": [[10, 50, 75, -1], [10, 50, 75, "All"]]
        "aLengthMenu": [50,75,100]
    });
</script>

<script>
    // function loadajaweawe(){
   
    
    // $.ajax({        
    //     type: 'GET',
    //     url: "{{url('/reloadtable')}}"+params, 
    //     beforeSend: function() {
            
    //      },
    //     success: function(result){              
    //         $('.progress').remove()
    //         $('#examples').html(result)

    //     // $(window).scrollTop($("#rulestep").offset().top);
    //     // $("#rulestep").focus();
    //     $(window).scrollTop($("#rulestep").offset().top - 300);
    //         $("#rulestep").focus();
    //     // window.location.hash = '#rulestep';
    //     // $( "#rulestep" ).focus();
        
    //     }
    // });  
    // }
</script>
@endsection
