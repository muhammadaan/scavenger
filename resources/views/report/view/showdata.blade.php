@extends('layouts.back-end.sidebar')

@section('content') 
 <!-- Breadcrumb -->


<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('admin/category-list')}}">Recycle</a></li>                    
                    <li class="active">Category Report</li>
                </ol>
            </div>
        </div>    

        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                    <strong>{{ session('status') }}.</strong>
                            <button type="button" class="close" data-dismiss="alert">×</button>
                        
                    </div>
                 @endif
                <div class="block">
                    <div class="header">
                        <h2>CATEGORY REPORT</h2>
                        <div class="side pull-right">                            
                            <ul class="buttons">
                                <li class="btn-group">                                    
                                                                                                            
                                </li>
                                <li><a href="#" class="block-toggle tip" title="Toggle content"><span class="icon-chevron-down"></span></a></li>
                               
                            </ul>
                        </div>                        
                    </div>
                    <hr>
                    <div class="content">
                        <div class="col-md-12">
                
                <div class="block">
                    <div class="header" align="center">CATEGORY REPORT
                       
                    </div>
                    
                </div>                
            

           
                        </div>                          
                            </br>
                            </br>
                            </br>
                            <div class="examples">
                                <table class="table table-bordered table-striped table-hover" id="example" >
                             <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Recycle Category</th>                                    
                                    <th>Weight </th>
                                    
                                </tr>
                            </thead>
                            <tbody> 
                             @foreach($test as $i=>$row)
                          <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{$row->detail[0]->category_id}}</td>                            
                            <td>{{$row->detail[0]->weight_estimate}}</td>
                          </tr>  
                            @endforeach        
                            </tbody>
</table> 
                            </div>                  
                    </div>   
                </div> 
            </div>
        </div>
@endsection
@section('scripts')

<script type="text/javascript">

     $('#example').dataTable({
        // "aLengthMenu": [[10, 50, 75, -1], [10, 50, 75, "All"]]
        "aLengthMenu": [50,75,100]
    });
</script>

@endsection
