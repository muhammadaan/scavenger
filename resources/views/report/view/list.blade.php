@extends('layouts.back-end.sidebar')

@section('content') 
 <!-- Breadcrumb -->


<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('admin/category-list')}}">Recycle</a></li>                    
                    <li class="active">Category Report</li>
                </ol>
            </div>
        </div>    

        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                    <strong>{{ session('status') }}.</strong>
                            <button type="button" class="close" data-dismiss="alert">×</button>
                        
                    </div>
                 @endif
                <div class="block">
                    <div class="header">
                        <h2 style="color: rgb(0,146,69)">CATEGORY REPORT</h2>
                        <div class="side pull-right">                            
                            <ul class="buttons">
                                <li class="btn-group">                                    
                                                                                                            
                                </li>
                                <li><a href="#" class="block-toggle tip" title="Toggle content"><span class="icon-chevron-down"></span></a></li>
                               
                            </ul>
                        </div>                        
                    </div>
                    <hr>
                    <div class="content">
                        <div class="col-md-12">
                
                <div class="block">
                    <div class="header" align="center">CATEGORY REPORT
                       
                    </div>
                    <form class="form-horizontal" method="POST" action="{{ route('report.list-showdatareport') }}" id="validate_custom">                        {{ csrf_field() }}
                    <div class="content controls">                        
                            <div class="col-md-1">Tanggal:</div>
                                <div class="col-md-2">
                                <input type="date" name="start_date" class="form-control">
                                </div>
                            <div class="col-md-1">s/d:</div>
                                <div class="col-md-2">
                                <input type="date" name="end_date" class="form-control">
                                </div>                            
                        <div class="form-row">
                                <div class="side pull-right">
                                    <div class="col-md-1">
                                            <div class="btn-group">
                                                <button class="btn btn-primary" type="submit" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69) ;">Filter</button>
                                            </div>
                                    </div>
                                </div>
                        </div>

                    </div>
                </form>
                </div>                
            

           
    </div>                          
                            </br>
                         <!--    </br>
                            </br>
                           <table class="table table-bordered table-striped table-hover" id="example" >
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Image</th>
                                    <th>Recycle Category</th>                                    
                                    <th>Weight </th>
                                    
                                </tr>
                            </thead>
                            <tbody>         
                            </tbody>
                        </table>  -->                    
                    </div>   
                </div> 
            </div>
        </div>
@endsection
@section('scripts')

<script type="text/javascript">
        //     $(document).ready(function(){
        //      var table = $('#example').DataTable();
        //     table.ajax.url('{{route("category.ajax.getdata")}}').load();
        // });

     $('#example').dataTable({
        // "aLengthMenu": [[10, 50, 75, -1], [10, 50, 75, "All"]]
        "aLengthMenu": [50,75,100]
    });
</script>
@endsection
