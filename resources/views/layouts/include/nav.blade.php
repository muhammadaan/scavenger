   <nav class="navbar brb" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-reorder"></span>                            
                </button> 
                <a class="navbar-brand" href="{{route('dashboard')}}"><img src="{{ asset('public/back-end/img/scavenger.png') }}" height="85%"></a>                         
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">                                     
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="{{ route('dashboard') }}">
                            <span class="icon-home"></span> Dashboard
                        </a>
                    </li>                            
                       
                  
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-user"></span>User</a>

                         <ul class="dropdown-menu" style="background-color: rgb(0,146,69)">
                            <!-- <li><a href="">Master</a></li>  -->
                            <li>
                                <a href="#">Transaction<i class="icon-angle-right pull-right"></i></a>
                                        <ul class="dropdown-submenu" style="background-color: rgb(0,146,69)">
                                            <li><a href="{{ route('customer.list') }}">User List</a></li>
                                                                                        
                                        </ul>
                            </li>  
                                 
                            <!-- <li><a href="#">Report</a></li>           -->

                        </ul>
                    </li>  
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-trash"></span>RECYCLE</a>
                        <ul class="dropdown-menu" style="background-color: rgb(0,146,69)">
                            <li>
                                <a href="#">Master<i class="icon-angle-right pull-right"></i></a>
                                        <ul class="dropdown-submenu" style="background-color: rgb(0,146,69)">
                                            <li><a href="{{ route('category.list') }}">Category Setup</a></li>
                                                                                        
                                        </ul>
                            </li> 
                            <li>
                                <a href="#">Transaction<i class="icon-angle-right pull-right"></i></a>
                                        <ul class="dropdown-submenu" style="background-color: rgb(0,146,69)">
                                            <li><a href="{{ route('request.list') }}">Recycle List</a></li>
                                                                                        
                                        </ul>
                            </li>  
                                <ul class="dropdown-submenu">
                                    <li><a href="">Transaction</a></li>

                                </ul>  
                            <li>
                                <a href="#">Report<i class="icon-angle-right pull-right"></i></a>
                                        <ul class="dropdown-submenu" style="background-color: rgb(0,146,69)">
                                            <li><a href="{{ route('report.list') }}">Category Report</a></li>
                                            <li><a href="{{ route('report.list-requestreport') }}">Request Report</a></li>
                                                                                        
                                        </ul>
                            </li>           

                        </ul>
                    </li>   
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-trash"></span>Scavenger</a>
                        <ul class="dropdown-menu" style="background-color: rgb(0,146,69)">
                            <li>
                                <a href="#">Transaction<i class="icon-angle-right pull-right"></i></a>
                                        <ul class="dropdown-submenu" style="background-color: rgb(0,146,69)">
                                            <li><a href="{{ route('scavenger.list') }}">Scavenger</a></li>
                                                                                        
                                        </ul>
                            </li>  
                                <ul class="dropdown-submenu" style="background-color: rgb(0,146,69)">
                                    <li><a href="">Transaction</a></li>
                                </ul>           
                        </ul>
                    </li>        
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-trash"></span>Donation</a>
                        <ul class="dropdown-menu" style="background-color: rgb(0,146,69)">

                            <!-- <li><a href="#">Master</a></li> -->
                            <li>
                                <a href="#">Transaction<i class="icon-angle-right pull-right"></i></a>
                                        <ul class="dropdown-submenu" style="background-color: rgb(0,146,69)">
                                            <li><a href="{{ route('donation.list') }}">Donation</a></li>
                                        </ul>
                            </li>
                            <!-- <li><a href="#">Report</a></li> -->
                         
                        </ul>
                    </li>       
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="icon-cog"></span> Setting</a>
                        <ul class="dropdown-menu" style="background-color: rgb(0,146,69)">
                            <li><a href="{{ route('about.show') }}">About Us</a></li>          
                            <li><a href="{{ route('newsletter.list') }}">Newsletter</a></li>          
                            <li><a href="{{ route('vehicle.list') }}">Vehicle</a></li>
                            <li><a href="{{ route('partner.list') }}">Our Partner</a></li>
                            <li><a href="{{ route('users.list') }}">Admin List</a></li>        
                            <li><a href="{{ route('users-group') }}">Permision Group</a></li>        
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="{{ route('logout') }}" ><span class="icon-off"></span> Logout</a>
                       
                    </li>       
                     
                </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="search..."/>
                    </div>                            
                </form>                                            
            </div>
        </nav>  