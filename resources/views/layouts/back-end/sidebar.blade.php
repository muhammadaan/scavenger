<!DOCTYPE html>
<html lang="en">
<head>
    <title>Scavenger</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="_token" content="{!! csrf_token() !!}" />
    
    <link rel="icon" href="" sizes="50x50" /> 
    
    <link href="{{ url("public/back-end/css/stylesheets.css") }}" rel="stylesheet" type="text/css" />
    
    @stack('css')   
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/jquery.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/jquery-ui.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/jquery-migrate.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/globalize.js") }}'></script>    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/bootstrap/bootstrap.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/datatables/jquery.dataTables.min.js") }}'></script>
     <!-- <script type='text/javascript' src='https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js'></script> -->
     <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
     <script src="https://cdn.datatables.net/buttons/1.5.3/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" type="text/javascript"></script>

    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/uniform/jquery.uniform.min.js") }}'></script>
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/knob/jquery.knob.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/sparkline/jquery.sparkline.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/flot/jquery.flot.js") }}'></script>     
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/flot/jquery.flot.resize.js") }}'></script>
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins.js") }}'></script>    
    <script type='text/javascript' src='{{ url("public/back-end/js/actions.js") }}'></script>    
    <script type='text/javascript' src='{{ url("public/back-end/js/charts.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/settings.js") }}'></script>
    <!-- form validation -->
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/select2/select2.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/tagsinput/jquery.tagsinput.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/jquery-ui-timepicker-addon.js") }}'></script>
    <!-- <script type='text/javascript' src='{{ url("public/back-end/js/plugins/ibutton/jquery.ibutton.js") }}'></script> -->
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/validationengine/languages/jquery.validationEngine-en.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/validationengine/jquery.validationEngine.js") }}'></script>
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/maskedinput/jquery.maskedinput.min.js") }}'></script>    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/stepy/jquery.stepy.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/cleditor/jquery.cleditor.min.js") }}'></script>
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/codemirror/codemirror.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/codemirror/addon/edit/matchbrackets.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/codemirror/mode/htmlmixed/htmlmixed.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/codemirror/mode/xml/xml.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/codemirror/mode/javascript/javascript.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/codemirror/mode/css/css.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/codemirror/mode/clike/clike.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/codemirror/mode/php/php.js") }}'></script>    
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/uniform/jquery.uniform.min.js") }}'></script>
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/tinymce/tinymce.min.js") }}'></script>


    
    <style type="text/css">
        .load-bar {
				display:none;
                position: relative;
                width: 100%;
                height: 6px;
                background-color: #fdba2c;
                }
                .bar {
                content: "";
                display: inline;
                position: absolute;
                width: 0;
                height: 100%;
                left: 50%;
                text-align: center;
                }
                .bar:nth-child(1) {
                background-color: #da4733;
                animation: loading 3s linear infinite;
                }
                .bar:nth-child(2) {
                background-color: #3b78e7;
                animation: loading 3s linear 1s infinite;
                }
                .bar:nth-child(3) {
                background-color: #fdba2c;
                animation: loading 3s linear 2s infinite;
                }
                @keyframes loading {
                    from {left: 50%; width: 0;z-index:100;}
                    33.3333% {left: 0; width: 100%;z-index: 10;}
                    to {left: 0; width: 100%;}
                }
        /****** Style Star Rating Widget *****/
        #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
        }

        #myImg:hover {opacity: 0.7;}

        /* The Modal (background) */
        .modal-image {
           
            position: fixed; /* Stay in place */
            z-index: 999999; /* Sit on top */
            padding: 150px;
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
        }

        /* Modal Content (image) */
        .modal-content-image {
           /* padding: 50px;*/
            margin: auto;
            display: block;
            width: 80%;
            max-width: 500px;
            max-height: 500px;
        }


        /* Caption of Modal Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 20px 0;
            height: 150px;
        }

        /* Add Animation */
        .modal-content-image, #caption {    
            -webkit-animation-name: zoom;
            -webkit-animation-duration: 0.6s;
            animation-name: zoom;
            animation-duration: 0.6s;
        }

        @-webkit-keyframes zoom {
            from {-webkit-transform:scale(0)} 
            to {-webkit-transform:scale(1)}
        }

        @keyframes zoom {
            from {transform:scale(0)} 
            to {transform:scale(1)}
        }

        /* The Close Button */
        .modal-image .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }

        .modal-image .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }

        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
           .modal-image .modal-content {
                width: 100%;
            }
        }
        .rating { 
          border: none;
          float: left;
        }

        .rating > input { display: none; } 
        .rating > label:before { 
          margin: 5px;
          font-size: 1.25em;
          font-family: FontAwesome;
          display: inline-block;
          content: "\f005";
        }

        .rating > .half:before { 
          content: "\f089";
          position: absolute;
        }

        .rating > label { 
          color: #ddd; 
         float: right; 
        }
        .rotate90 {
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    transform: rotate(90deg);
  
    }
        /***** CSS Magic to Highlight Stars on Hover *****/

        .rating > input:checked ~ label, /* show gold star when clicked */
        .rating:not(:checked) > label:hover, /* hover current star */
        .rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

        .rating > input:checked + label:hover, /* hover current star when changing rating */
        .rating > input:checked ~ label:hover,
        .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
        .rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
        </style>
    
</head>
<body class="bg-img-num1" data-settings="open"> 
<div class="load-bar">
  <div class="bar"></div>
  <div class="bar"></div>
  <div class="bar"></div>
</div>
   <div class="container">        
    <div class="row">                   
        <div class="col-md-12">

            @include('layouts.include.nav')              

    </div>            
</div>

<!-- page content -->

    @yield('content')
    @stack('scripts')   
<!-- ./page content -->

<!-- page footer -->
<div class="row">
    <div class="page-footer">
    @include('layouts.back-end.footer')
    </div>
</div>
<!-- ./page footer -->
</div>

    
</body>
@yield('scripts')


</html>



