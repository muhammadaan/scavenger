<!DOCTYPE html>
<html lang="en">
<head>        
    <title>PGN MAS</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    
    <link href="{{ url("public/back-end/css/stylesheets.css") }}" rel="stylesheet" type="text/css" />        
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/jquery.uimin.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/jquery-.min.js") }}'></script>   
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/jquery-migrate.min.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/jquery/globalize.js") }}'></script>    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/bootstrap/bootstrap.min.js") }}'></script>
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins/uniform/jquery.uniform.min.js") }}'></script>
    
    <script type='text/javascript' src='{{ url("public/back-end/js/plugins.js") }}'></script>    
    <script type='text/javascript' src='{{ url("public/back-end/js/actions.js") }}'></script>
    <script type='text/javascript' src='{{ url("public/back-end/js/settings.js") }}'></script>
</head>

<body>
@yield('section')
</body>
</html>



