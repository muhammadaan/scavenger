@extends('layouts.back-end.sidebar')

@section('content')
        <!-- Breadcrumb -->
<style>
    select[name="recycle-table_length"] {
        width: 75px;
        background-color: rgb(0,146,69);
    }

    div[class="dataTables_length"] {
        width: 300px;
    }

    div[class="dataTables_filter"] {
        /*margin-top: 5px;*/
        margin-left: 90%;
    }

    div.dt-buttons{
        color: black;
        float: right;
    }

   button[class="dt-button buttons-excel buttons-html5"]{
    /*background-color: green;*/
      color: #ffffff;
      font-weight: bold;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
      background-image: -webkit-linear-gradient(top, #00cc00 0%, #00cc00 100%);
      background-image: -o-linear-gradient(top, #00cc00 0%, #00cc00 100%);
      background-image: linear-gradient(to bottom, #00cc00 0%, #00cc00 100%);
      /*background-color: #00cc00;*/
      background-image: -moz-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(rgb(0,146,69)), to(rgb(0,146,69)));
      background-image: -webkit-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: -o-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: linear-gradient(to bottom, rgb(0,146,69), rgb(0,146,69));
      background-repeat: repeat-x;
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff00cc00', endColorstr='#ffe34619', GradientType=0);
      border-color: #00cc00 #00cc00 #00cc00;
      border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
      background-color: rgb(0,146,69);
      /* Darken IE7 buttons by default so they stand out more given they won't have borders */
      filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
      width:50px;
      height: 30px;

    }

    button[class="dt-button buttons-pdf buttons-html5"]{
      color: #ffffff;
      font-weight: bold;
      text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
      background-image: -webkit-linear-gradient(top, rgb(0,146,69) 0%, rgb(0,146,69) 100%);
      background-image: -o-linear-gradient(top, rgb(0,146,69) 0%, rgb(0,146,69) 100%);
      background-image: linear-gradient(to bottom, rgb(0,146,69) 0%, rgb(0,146,69) 100%);
      background-color: rgb(0,146,69);
      background-image: -moz-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: -webkit-gradient(linear, 0 0, 0 100%, from(rgb(0,146,69)), to(rgb(0,146,69)));
      background-image: -webkit-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: -o-linear-gradient(top, rgb(0,146,69), rgb(0,146,69));
      background-image: linear-gradient(to bottom, rgb(0,146,69), rgb(0,146,69));
      background-repeat: repeat-x;
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffff0000', endColorstr='#ffe34619', GradientType=0);
      border-color: #ff0000 #ff0000 #ff0000;
      border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
      /**background-color: #ff0000;*/
      background-color: rgb(0,146,69);
      /* Darken IE7 buttons by default so they stand out more given they won't have borders */
      filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
      margin-left: 10px;
      width:50px;
      height: 30px;
    }
</style>


<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>
            <li><a href="{{url('admin/request-list')}}">Request</a></li>
            <li class="active">List</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        @if (session('status'))
            <div class="alert alert-success">
                <strong>{{ session('status') }}.</strong>
                <button type="button" class="close" data-dismiss="alert">×</button>

            </div>
        @endif
        <div class="block">
            <div class="header">
                <h2 style="color: rgb(0,146,69)">Recycle List</h2>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn btn-primary" onclick="reloadPage()" style="color: white; background-color: rgb(0,146,69);">Refresh</button>
                <div class="side pull-right">
                    <ul class="buttons">
                        <li class="btn-group">

                        </li>
                        <li><a href="#" class="block-toggle tip" title="Toggle content"><span
                                        class="icon-chevron-down"></span></a></li>

                    </ul>
                </div>
            </div>
            <hr>
            <div class="content">
                <div class="col-md-2">
                    {{-- <a href="{{ url('admin/users-add') }}"  class="btn btn-default btn-block btn-clean">New User</a> --}}
                </div>

                <table class="table table-bordered table-striped table-hover" id="recycle-table">

                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Request Number</th>
                        <th>Customer Name</th>
                        <th>Mobile Number</th>
                        <th>Weight Estimate (Kg)</th>
                        <th>Date Request</th>
                        <th>Status</th>
                        <th>Created At</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($requests))
                        @foreach($requests as $index=>$request)

                            <tr class="clicklable-row" data-href="{{url('/admin/request/detail', $request->id)}}">
                                <td>{{$index+1}}</td>
                                @if(@$request->detail[0]->request_id == null)
                                    <td>-</td>
                                @else
                                    <td>00{{$request->detail[0]->request_id}}</td>
                                @endif

                                @if($request->user->role_id == 1)
                                    <td>{{$request->user->name}}</td>
                                @else
                                    <td>-</td>
                                @endif

                                @if($request->user->mobile == null)
                                    <td>-</td>
                                @else
                                    <td>{{$request->user->mobile}}</td>
                                @endif

                                @if($request->weight_total_estimate == null)
                                    <td>-</td>
                                @else
                                    <td>{{$request->weight_total_estimate}}</td>
                                @endif

                                @if($request->order_date == null)
                                    <td>-</td>
                                @else
                                    <td>{{ \Carbon\Carbon::parse($request->order_date)->format('d M Y')}}</td>
                                @endif

                                @if($request->status == null)
                                    <td>-</td>
                                @elseif($request->status == 1)
                                    <td>Searching for scavenger</td>
                                @elseif($request->status == 2)
                                    <td>Waiting for pick up</td>
                                @elseif($request->status == 3)
                                    <td>Complete</td>
                                @else
                                    <td>Failed</td>
                                @endif

                                <td>{{ \Carbon\Carbon::parse($request->created_at)->format('d M Y - H:i:s') }}</td>
                            </tr>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script type="text/javascript">

        $('#recycle-table').dataTable({
            "aLengthMenu": [10, 50, 100],
            "dom": 'lBfrtip',
            "buttons": [
                {
                    extend: 'excelHtml5',
                    title: 'Recycle List'
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Recycle List'
                },
              ],
        });


        jQuery(document).ready(function ($) {
            $("#recycle-table").on('click', '.clicklable-row', function () {
                window.location = $(this).data("href");
            });
            $("#recycle-table").css('cursor', 'pointer');
            $("#recycle-table").hover(function () {
                $(this).css('cursor', 'pointer');
            });
        });

        function reloadPage()
        {
            location.reload();
        }


    </script>
@endsection
