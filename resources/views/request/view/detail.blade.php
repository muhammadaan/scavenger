@extends('layouts.back-end.sidebar')

@section('content')
        <!-- Breadcrumb -->
<style>
    div[class="content"] {
        height: 350px;
    }

    select[name="example_length"] {
        width: 75px;
    }

    div[class="dataTables_length"] {
        width: 300px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}">Home</a></li>
            <li><a href="{{url('admin/request-list')}}">Request</a></li>
            <li class="active">Request Detail</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="block">
            <div class="header">
                <h2 style="color: rgb(0,146,69)">Recycle Detail</h2>
            </div>

            <div class="content request-detail">
                <div class="row">
                    <div class="col-md-6">

                        <div class="block">
                            <div class="header">
                                <h2 style="color: rgb(0,146,69)">Request Information</h2>
                            </div>
                            <div class="content">

                                <div class="form-row">
                                    <div class="col-md-4">Recycle Number</div>
                                    <div class="col-md-8">
                                        @if(@$requests->detail[0]->request_id == null)
                                            -
                                        @else
                                            00{{$requests->detail[0]->request_id}}
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Customer Name</div>
                                    <div class="col-md-8">
                                        @if($requests->user->role_id == 1)
                                            {{$requests->user->name}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Mail</div>
                                    <div class="col-md-8">
                                        @if($requests->user->role_id == 1)
                                            {{$requests->user->email}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Mobile Number</div>
                                    <div class="col-md-8">
                                        @if($requests->user->mobile == null)
                                            -
                                        @else
                                            {{$requests->user->mobile}}
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Weight Estimate (Kg)</div>
                                    <div class="col-md-8">
                                        @if($requests->weight_total_estimate == null)
                                            -
                                        @else
                                            {{$requests->weight_total_estimate}}
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Status</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 1)
                                            <td>Searhing for scavenger</td>
                                        @elseif($requests->status == 2)
                                            <td>Waiting for pick up</td>
                                        @elseif($requests->status == 3)
                                            <td>Complete</td>
                                        @else
                                            <td>Failed</td>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <div class="header">
                                <h2 style="color: rgb(0,146,69)">Pickup Information</h2>
                            </div>
                            <div class="content">

                                <div class="form-row">
                                    <div class="col-md-4">Address</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 1 || $requests->status == 2 || $requests->status == 3)
                                            {{$requests->address}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Note</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 1 || $requests->status == 2 || $requests->status == 3)
                                            {{$requests->note}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Date Request Time Pickup</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 1 || $requests->status == 2 || $requests->status == 3)
                                            {{\Carbon\Carbon::parse($requests->date_request_pickup)->format('d M Y')}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Time</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 1 || $requests->status == 2 || $requests->status == 3)
                                            {{$requests->time_request_pickup}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Note</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 1 || $requests->status == 2 || $requests->status == 3)
                                            {{$requests->catatan}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Distance</div>
                                    <div class="col-md-8">
                                        {{\App\Http\Controllers\Request\RequestHelper::convertmtokm($requests->jarak)}} Km
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">

                        <div class="block">
                            <div class="header">
                                <h2 style="color: rgb(0,146,69)">Scavenger Information</h2>
                            </div>
                            <div class="content">

                                <div class="form-row">
                                    <div class="col-md-4">Scavenger Name</div>
                                    @if($requests->status == 2 || $requests->status == 3)
                                        <div class="col-md-8">
                                            @if($requests->scavenger->role_id == 2)
                                                {{$requests->scavenger->name}}
                                            @else
                                                -
                                            @endif
                                        </div>
                                    @else
                                        <div class="col-md-6">-</div>
                                    @endif
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Date Request Time Pickup</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 2 || $requests->status == 3)
                                            {{\Carbon\Carbon::parse($requests->date_request_pickup)->format('d M Y')}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Time</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 2 || $requests->status == 3)
                                            {{$requests->time_request_pickup}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Note</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 2 || $requests->status == 3)
                                            {{$requests->catatan}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Vehicle</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 2 || $requests->status == 3)
                                            {{$requests->vehicle}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="block">
                            <div class="header">
                                <h2 style="color: rgb(0,146,69)">Pickup Realisation</h2>
                            </div>
                            <div class="content">

                                <div class="form-row">
                                    <div class="col-md-4">Date Time Pickup</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 3)
                                            {{\Carbon\Carbon::parse($requests->updated_at)->format('d M Y - H:i')}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Weight Realisation (Kg)</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 3)
                                            {{$requests->berat_realisasi}}
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-4">Poin</div>
                                    <div class="col-md-8">
                                        @if($requests->status == 3)
                                            @if($requests->point_history == null)
                                                -
                                            @else
                                                {{$requests->point_history->topup_point}}
                                            @endif
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped table-hover" id="example" style="color: #FFFFFF; background-color: rgb(0,146,69);">
                            <thead>
                            <tr>
                                <th>Recycle Category</th>
                                <th>Weight Estimate</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($requests->detail as $key => $category)
                                <tr>
                                    <th>{{$category->category->category_name}}</th>
                                    <th>{{$category->weight_estimate}}</th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script type="text/javascript">

        var table = $('#example').DataTable({
            "aLengthMenu": [10, 50, 100]
        });
        $('#example .search th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        });

    </script>
@endsection