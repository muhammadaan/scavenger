@extends('layouts.back-end.sidebar')

@section('content') 
 <!-- Breadcrumb -->
 @push('scripts')

    

    @endpush


<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/users-list')}}">Users</a></li>                    
                    <li class="active">List</li>
                </ol>
            </div>
        </div>   

<div class="row">
            <div class="col-md-12">
               @if (session('status'))
                    <div class="alert alert-success">
                    <strong>{{ session('status') }}.</strong>
                            <button type="button" class="close" data-dismiss="alert">×</button>
                        
                    </div>
                 @endif
                <div class="block block-transparent">
                    <div class="header">
                        <h2 style="color: rgb(0,146,69)">Tinymce</h2>
                    </div>
                       <form id="validate" method="POST" action="{{ url('admin/about-update') }}">
                        {{ csrf_field() }}
                            <div class="form-row">
                                    <div class="col-md-2">Title:</div>
                                    <div class="col-md-10"><input type="text"  class="validate[required]" value="{{$config->key or null}}" name="key" /></div>
                                </div>

                            <div class="content np">

                              <div class="col-md-2">Descriptions:</div>
                              <div class="col-md-10">
                                <textarea class="tmce" name="value">
                                {{$config->value or null}}
                                </textarea>
                             </div>
                        </div>
                         <div class="footer">
                        <div class="side pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default" type="button" onClick="$('#validate').validationEngine('hide');" style="color: rgb(0,146,69);">Hide prompts</button>
                                <button class="btn btn-success" type="submit" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69);">Submit</button>
                            </div> 
                        </div>
                    </div>    
                    </form>
                </div>
            </div>
        </div>

@endsection