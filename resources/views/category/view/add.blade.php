 @extends('layouts.back-end.sidebar')

@section('content') 

 

<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/category-list')}}">Rycycle</a></li>                    
                    <li class="active">Add Recycle Category</li>
                </ol>
            </div>
        </div>   


 <div class="row">
            <div class="col-md-12">
                
                <div class="block">
                    <div class="header">
                        <h2>Recycle Category</h2>
                    </div>
                <form id="validate" method="POST" action="#">
                {{-- <form id="validate" method="POST" action="{{ url('/admin/category') }}/{{$data->id}}"> --}}
                {{ csrf_field() }}
                    <div class="content controls">
                        <div class="form-row">
                            <div class="col-md-3">Recycle Image:</div>
                            <div class="col-md-9"><input type="file" id="poto" class="validate[required]"  name="foto" value=""/></div>
                        </div>
                         <div class="form-row">
                            <div class="col-md-3">Recycle Category:</div>
                            <div class="col-md-9"><input type="text" id="nama" class="validate[required]" name="name" value=""/></div>
                        </div>
                     
                        <div class="form-row">
                            <div class="col-md-3">Recycle desc:</div>
                            <div class="col-md-9">
                            {{-- <div class="col-md-9"><input type="email"  value="{{$data->email}}" name="alamat" /></div> --}}
                            <textarea name="alamat" id="alamats" class="validate[required,maxSize[500]]" id="" cols="20" rows="10"></textarea>
                            </div>
                        </div>
                           
                        <input type="hidden" name="created_by" value="{{Auth::user()->name}}">
                           
                    </div>
                     <div class="footer">
                        <div class="side pull-right">
                            {{-- <div class="btn-group">
                                
                                <button class="btn btn-success" type="submit">Submit</button>
                            </div> --}}

                            <div class="btn-group">
                            {{-- <label class="input-group-addon"></label>                           --}}
                           <input type="submit" id="btnSubmit" staff="1" value="Edit" class="btn btn-primary"></input>
                            <input type="button" value="Cancel" id="btnTest" class="btn btn-danger"></input>
                        </div>
                        </div>
                    </div>    
                    </form>
                </div>       
            </div>
        </div>                

@endsection
@section('scripts')
    {{-- <script type="text/javascript">
           $('#form-password').show()
                $('#form-confpassword').show()
        $('#checkpass').click(function(){
               if(this.checked){
                // alert("checked")
                $('#form-password').show()
                $('#form-confpassword').show()
               }else{
                // alert("unchecked")
                $('#form-password').hide()
                $('#form-confpassword').hide()

               }     
        });

    </script> --}}
    <script>
    $(document).ready(function(){
     $("#btnTest").hide();
            $('#btnSubmit').click(function(e) {


                if($(this).attr("staff") == 1){
                    $(this).attr("staff",0)
                    e.preventDefault()
                }else{
                    $(this).unbind('click')
                }

                $(this).val('Save Staff');

                    $("#btnTest").show();
                        if($('#poto').prop('disabled') && $('#nama').prop('disabled') && $('#alamats').prop('disabled'))
                    {
                        $('#poto').prop('disabled', false)
                        $('#nama').prop('disabled', false)
                        $('#alamats').prop('disabled', false)
                       
                       
                    }
                        else{

                        $('#poto').prop('disabled', false)
                        $('#nama').prop('disabled', false)
                        $('#alamats').prop('disabled', false)
                       
                    }
                });

         
         $('#btnTest').click(function() {
                    
                    if($("#btnSubmit").attr('staff') == 0){

                         // $()
                         $("#btnSubmit").attr('staff',1)
                         $("#btnTest").hide();
                         $("#btnSubmit").val('Edit');
                         $('#poto').prop('disabled', true)
                        $('#nama').prop('disabled', true)
                        $('#alamats').prop('disabled', true)

                    }

                     
                     
                 
                });
         });

</script>
@endsection