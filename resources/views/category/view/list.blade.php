@extends('layouts.back-end.sidebar')

@section('content') 
 <!-- Breadcrumb -->
<style>
    select[name="example_length"] { width: 75px; }
    div[class="dataTables_length"] { width:300px; }
</style>


<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('admin/category-list')}}">Recycle</a></li>                    
                    <li class="active">Category Setup</li>
                </ol>
            </div>
        </div>    

        <div class="row">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success">
                    <strong>{{ session('status') }}.</strong>
                            <button type="button" class="close" data-dismiss="alert">×</button>
                        
                    </div>
                 @endif
                <div class="block">
                    <div class="header">
                        <h2 style="color: rgb(0,146,69)">Master Category</h2>
                        <div class="side pull-right">                            
                            <ul class="buttons">
                                <li class="btn-group">                                    
                                                                                                            
                                </li>
                                <li><a href="#" class="block-toggle tip" title="Toggle content"><span class="icon-chevron-down"></span></a></li>
                               
                            </ul>
                        </div>                        
                    </div>
                    <hr>
                    <div class="content">
                          <div class="col-md-2">
                               <a href="{{route('category.add')}}"><button type="button" class="btn btn-info" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69) ;"> <i class="icon-plus"></i> Add New Category</button></a>
                            </div>
                            </br>
                            </br>
                            </br>
                           <table class="table table-bordered table-striped table-hover" id="example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Image</th>
                                    <th>Recycle Category</th>                                    
                                    <th>Created At</th>
                                </tr>
                            </thead>
                           <tbody>
                           @foreach($categories as $index=>$category)

                               <tr class="clicklable-row" data-href="{{url('/admin/category/edit', $category->id)}}">
                                   <td>{{$index+1}}</td>
                                   <td>{{$category->image}}</td>
                                   <td>{{$category->category_name}}</td>
                                   <td>{{$category->created_at}}</td>
                               </tr>

                           @endforeach
                           </tbody>
                        </table>                     
                    </div>   
                </div> 
            </div>
        </div>
@endsection
@section('scripts')

<script type="text/javascript">

    jQuery(document).ready(function($) {
        $("#example").on('click','.clicklable-row', function() {
            window.location = $(this).data("href");
        });
        $("#example").css( 'cursor', 'pointer' );
        $("#example").hover(function() {
            $(this).css('cursor','pointer');
        });
    });

    {{--$(document).ready(function(){--}}
        {{--var table = $('#example').DataTable();--}}
        {{--table.ajax.url('{{route("category.ajax.getdata")}}').load();--}}
    {{--});--}}

     $('#example').dataTable({
        // "aLengthMenu": [[10, 50, 75, -1], [10, 50, 75, "All"]]
        "aLengthMenu": [10,50,100]
    });

</script>
@endsection
