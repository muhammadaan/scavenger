@extends('mails.layout')
@section('content')
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top" class="textContent">
			<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">

				<h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Hello {{$request->user->name}}! <br><br>
				</h3>

				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					We’re successfully pickup your recyclable trash and ready to be recycle. <br><br>
					
				</p>				
				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Detail Info : <br><br>
				</p>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="100px" valign="top">Date & Time</td>
						<td valign="top">:</td>
						<td>{{\Carbon\Carbon::parse($request->date_request_pickup)->format('l d F Y')}}, {{$request->pickup_time}}</td>
					</tr>
					<tr>
						<td valign="top">Fleet Type</td>
						<td valign="top">:</td>
						<td>{{$request->realisasi_pickup}}</td>
						<!-- <td>-</td> -->
					</tr>
					<tr>
						<td valign="top">Driver</td>
						<td valign="top">:</td>
						<td>{{$request->scavanger->name}}</td>
					</tr>
					<tr>
						<td valign="top">Member</td>
						<td valign="top">:</td>
						<td>{{$request->user->name}}</td>
					</tr>
					<tr>
						<td valign="top">Location</td>
						<td valign="top">:</td>
						<td>{{$request->address}}</td>
					</tr>
					<tr>
						<td valign="top">Recycle Trash</td>
						<td valign="top">:</td>
						<td>{{$request->category}}</td>
					</tr>
					<tr>
						<td valign="top">Weight/Total</td>
						<td valign="top">:</td>
						<td>{{$request->berat_realisasi}} kg.</td>
					</tr>
				</table>				
			</div>
		</td>
	</tr>
</table>
@endsection