@extends('mails.layout')
@section('content')
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top" class="textContent">
			<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">

				<h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Hello {{$donation->user->name}}! <br><br>
				</h3>

				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					We received notification that you already giving us donation in amount of Rp. {{number_format($donation->amount,2,',','.')}} <br><br>
					
				</p>								
				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					If you want to give a donation via bank transfer, please make a donation confirmation on our Scavenger Apps.<br><br>
				</p>
			</div>
		</td>
	</tr>
</table>
@endsection