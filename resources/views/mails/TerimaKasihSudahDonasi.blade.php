@extends('mails.layout')
@section('content')
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top" class="textContent">
			<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">

				<h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Hello {{$donation->user->name}}! <br><br>
				</h3>								
				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Thank you for your donation!<br><br>
				</p>								
				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					We already received donations from :<br><br>
				</p>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="130px" valign="top">Name</td>
						<td valign="top">:</td>
						<td>{{$donation->user->name}}</td>
					</tr>
					<tr>
						<td valign="top">Amount</td>
						<td valign="top">:</td>
						<td>Rp. {{number_format($donation->amount,2,',','.')}}</td>
						<!-- <td>-</td> -->
					</tr>
					<tr>
						<td valign="top">Payment Method</td>
						<td valign="top">:</td>
						<td>{{$donation->payment_method}}</td>
					</tr>
					
				</table>
				<br>				
				<br>
				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Your participation will be used to support the various activities of our non profit organization<br><br>
				</p>
			</div>
		</td>
	</tr>
</table>
@endsection