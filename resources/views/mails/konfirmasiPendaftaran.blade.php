@extends('mails.layout')
@section('content')
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top" class="textContent">
			<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">

				<h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Hello {{$user->name}}! <br><br>
				</h3>

				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Thank you for registering Scavenger account! <br>
					Please confirm your email address to activate your account <br><br>
				</p>
				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;text-align: center;">
					<a href="{{$link}}">Click here to confirmation your account</a><br><br>
				</p>
				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					We are happy to announce you are now become a part of our green movement, let’s increase our awareness to keep the environment clean by collecting recycle waste with us.
				</p>
			</div>
		</td>
	</tr>
</table>
@endsection