@extends('mails.layout')
@section('content')
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top" class="textContent">
			<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">

				<h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Hello {{$request->user->name}}! <br><br>
				</h3>

				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Thank you for requesting recyclable trash pick up. <br><br>
					
				</p>

				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					In accordance with your request for scavenger to pick up recyclable trash at your location. then we will look for scavanger that is near from your location. <br><br>
					
				</p>				
				<p style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
					Your Request Detail Info : <br><br>
				</p>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td valign="top">Recycle type</td>
						<td valign="top">:</td>						
						<td>{{$request->category}}</td>
					</tr>
					<tr>
						<td valign="top">Recycle weight</td>
						<td valign="top">:</td>
						<!-- <td>$request->vehicle</td> -->
						<td>{{$request->weight_total_estimate}}</td>
					</tr>
					<tr>
						<td valign="top">Address </td>
						<td valign="top">:</td>
						<td>{{$request->address}}</td>
					</tr>
					<tr>
						<td width="120px" valign="top">Date & Time</td>
						<td valign="top">:</td>
						<td>{{\Carbon\Carbon::parse($request->date_request_pickup)->format('l d F Y')}}, {{$request->pickup_time}}</td>
					</tr>					
				</table>				
			</div>
		</td>
	</tr>
</table>
@endsection