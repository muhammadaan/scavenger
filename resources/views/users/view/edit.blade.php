 @extends('layouts.back-end.sidebar')

@section('content') 

 

<div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin/home')}}">Home</a></li>                    
                    <li><a href="{{url('/admin/users-list')}}">Users</a></li>                    
                    <li class="active">List</li>
                </ol>
            </div>
        </div>   


 <div class="row">
            <div class="col-md-12">
                
                <div class="block">
                    <div class="header">
                        <h2 style="color: rgb(0,146,69);">Edit Users</h2>
                    </div>
                <form id="validate" method="POST" action="{{ url('/admin/users') }}/{{$data->id}}">
                {{ csrf_field() }}
                    <div class="content controls">
                        <div class="form-row">
                            <div class="col-md-3">Name:</div>
                            <div class="col-md-9"><input type="text"  class="validate[required]"  name="name" value="{{$data->name}}"/></div>
                        </div>
                         <div class="form-row">
                            <div class="col-md-3">Username:</div>
                            <div class="col-md-9"><input type="text" class="validate[required]" name="username" value="{{$data->username}}"/></div>
                        </div>
                      <div class="form-row">
                            <div class="col-md-3">E-mail:</div>
                            <div class="col-md-9"><input type="email" class="validate[required,custom[email]]" value="{{$data->email}}" name="email" /></div>
                        </div>


                         <div class="form-row">
                            <div class="col-md-3">Edit Password:</div>
                            <div class="col-md-9">
                                <div class="checkbox-inline">
                                    <label><input type="checkbox" name="check_ex1" id="checkpass" checked="checked"/> </label>
                                </div>                                
                            </div>
                        </div>


                         <div class="form-row" id="form-password">
                            <div class="col-md-3">Password:</div>
                            <div class="col-md-9"><input type="password" name="password" class="validate[required,minSize[5],maxSize[10]] form-control" id="password" /></div>
                        </div>

                     <div class="form-row" id="form-confpassword">
                            <div class="col-md-3">Confirm Password:</div>
                            <div class="col-md-9"> <input type="password" class="validate[required,equals[password]] form-control"/></div>
                        </div>
                           
                        <div class="form-row">
                            <div class="col-md-3">User Group:</div>
                            <div class="col-md-9">
                                <select class="form-control" name="roles" class="validate[required] form-control">
                                    @foreach(\App\Models\Admin::all() as $row)
                                    @if($row->id == $data->roles)
                                    <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                    @else
                                    <option value="{{$row->id}}" >{{$row->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="created_by" value="{{Auth::user()->name}}">
                           
                    </div>
                     <div class="footer">
                        <div class="side pull-right">
                            <div class="btn-group">
                                <button class="btn btn-default" type="button" onClick="$('#validate').validationEngine('hide');" style="border-color: rgb(0,146,69);">Hide prompts</button>
                                <button class="btn btn-success" type="submit" style="background-color: rgb(0,146,69); border-color: rgb(0,146,69)">Submit</button>
                            </div>
                        </div>
                    </div>    
                    </form>
                </div>       
            </div>
        </div>                

@endsection
@section('scripts')
    <script type="text/javascript">
           $('#form-password').show()
                $('#form-confpassword').show()
        $('#checkpass').click(function(){
               if(this.checked){
                // alert("checked")
                $('#form-password').show()
                $('#form-confpassword').show()
               }else{
                // alert("unchecked")
                $('#form-password').hide()
                $('#form-confpassword').hide()

               }     
        });

    </script>
@endsection