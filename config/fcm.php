<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAAqLodFOw:APA91bF5EgjZ4ajduEVNoHIft1_1tXxi534SPrK1_gNUOml1KE1X0YmL6O0Z9ldQyTPi126TUJwlgPLbSXV1dJ9s-uVCQMeGS6y-Mp2lSGB74FBHcHOVoMElVt3kAI4-ukSdMtf1ZDhM'),
        'sender_id' => env('FCM_SENDER_ID', '724676973804'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 10.0, // in second
    ],
];
