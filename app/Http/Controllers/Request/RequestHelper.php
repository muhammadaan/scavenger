<?php

namespace App\Http\Controllers\Request;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestHelper extends Controller
{
    static public function storeImage($image, $image2, $image3){
    	
    	define('UPLOAD_DIR', 'storage/app/images/');
        

        $data_image         = array($image,$image2,$image3);
        $datas         = array();
        if(!$image && !$image2 && !$image3){
            return $req;
        }
        else {
            for ($i=0; $i < 3; $i++) { 
                if($data_image[$i]!=''){
                    array_push($datas,$data_image[$i]);
                }
           
            }
            
        }
    	
    	return $datas;
    }
    static public function showImage(){
    	$request = \App\Models\Request::find();
    	return response()->file($request->image);
    }

    public static function uploadpicture($input='')
    {
        # code...
        if(count($input['picture'])>0){
            define('UPLOAD_DIR', 'storage/app/images/');
            $image          = uniqid().".jpeg";
            $imgpicture     = $input['picture'];
            $datapicture    = base64_decode($imgpicture);
            $path           = UPLOAD_DIR . $image;
            $success        = file_put_contents($path, $datapicture);
            return $image;
        }

        return false;
    }

    public static function convertmtokm($value='')
    {
        # code...
        return number_format($value / 1000,2,",",".");
    }
}
