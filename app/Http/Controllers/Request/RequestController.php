<?php

namespace App\Http\Controllers\Request;

use App\Models\Category;
use App\Models\PointHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;

class RequestController extends Controller
{

//      public function __construct()
//     {
//         $this->middleware('auth');
//     }

    public function index(){
        $requests = \App\Models\Request::orderBy('created_at','desc')->get();
        $this->data['title'] = "Recycle List";

        return view('request.view.list', ['requests'=>$requests],$this->data);

    }

    public function getDatarequest(){

    $users = \App\Models\Request::all();
    // $donat = \App\Models\Donation::all();
    // $reqdet = \App\Models\RequestDetail::all();

     $i		= 0;
     $data	= [];
     $output = array(
           "data" => []
       );

     foreach ($users as $index=>$list){
        $reqdet = \App\Models\RequestDetail::where('request_id',$list->id)->sum('weight_estimate');
        $reqid = \App\Models\RequestDetail::where('request_id',$list->id)->sum('request_id');
        $link_edit = url('/admin/request/edit').'/'.$list->id ;     	     
        $link_delete = url('/admin/request/delete').'/'.$list->id ; 
        // $edits = User::where('id',$list->id)->first();    	     
        // $roles = \App\UserGroup::find($list->roles);
            $output['data'][$i][]    = $index+1;
            $output['data'][$i][]    = $reqid;           

            if ($list->status == 1){
                $output['data'][$i][] = 'Searching for scavenger';
            }
            else if ($list->status == 2){
                $output['data'][$i][] = 'Waiting for pick up';
            }
            else if ($list->status == 3){
                $output['data'][$i][] = 'Complete';
            }
            else{
                $output['data'][$i][] = 'Failed';
            }

            $output['data'][$i][]    = $list->address;
            $output['data'][$i][]    = $reqdet;
            $output['data'][$i][]    = Carbon::parse($list->created_at)->format('d M Y - H:i:s');
            $output['data'][$i][]    = '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit users"><i class="icon-edit"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete users"><i class="icon-remove-sign"></i></a></center>';
                $i++;
        }
        return json_encode($output);

    }

    public function RequestAdd(){

        // return view('Users.view.add');
    }

    public function show($id){
        $requests = \App\Models\Request::find($id);
        $this->data['title'] = "Recycle Detail";
        
        return view('request.view.detail',['requests'=>$requests], $this->data);

    }

    public function RequestPost(Request $request){

        // $input                        = $request->all();
        // $input['password']            = bcrypt($request->password);
        // $input['image']               = "";

        // $store                        = \App\Models\Admin::create($input);

        //  return redirect('/admin/users-list')->with('status', 'User Success Added!');;
    }

    public function Requestdestroy($id){

      // $data= \App\Models\Admin::find($id);
      // $data->delete();
      // return redirect('/admin/users-list')->with('status', 'User Success Deleted!');
    }

    public function Requestedit($id){
        
        $this->data['title'] = "Request Edit";
        $this->data['data'] = \App\Models\User::find($id);
        return view('request.view.edit',$this->data);
   }

    public function update(Request $request, $id){

        // $input                      = $request->all();
        // $update                     = \App\Models\Admin::find($id);
        
        // if($request->password == null){
        //     $input['password']      = $update->password;
        // }else{
        //     $input['password']      = bcrypt($request->password);
        // }

        // $update->update($input);

        // return redirect('/admin/users-list')->with('status', 'User Success Edited!');


     }

     public static function updaterequestsemua(Request $request)
     {

        try {
            
            \DB::beginTransaction();

            $input = $request->all();
            $data  = \App\Models\Request::find($input['request_id']);

            $data->update($input);

            if($input['status'] == 3)
            {
               $point     = \App\Http\Controllers\Point\PointController::getpointcompleterequest($data, $input['status']);

               $sendnotif = \App\Http\Controllers\Notification\NotificationController::sendfirebase([
                'user_id' => $data->customer_id,
                'type' => 3,
                'tittle' => config('app.name'),
                'content' => 'your request has been complete',
               ]);

               
            }

            \DB::commit();

            $result = array('result'=> 1,'firebase' => array_flatten($sendnotif), 'message'=> 'success', 'data' => $data);


        } catch (\Exception $e) {

            \DB::rollback();

            $result = array('result'=>0, 'message'=> $e->getMessage().' on file '.$e->getFile(). ' line '.$e->getLine(), 'data' => []);
        }

        return $result;

     }



}
