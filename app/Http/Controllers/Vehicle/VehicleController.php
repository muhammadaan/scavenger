<?php

namespace App\Http\Controllers\Vehicle;

use App\Models\Vehicle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehicleController extends Controller
{
    public function index()
    {
        $vehicles = \App\Models\Vehicle::orderBy('created_at','desc')->get();
        $this->data['title'] = "Vehicle List";

        return view('vehicle.view.list', ['vehicles'=>$vehicles] ,$this->data);

    }

    public function VehicleAdd()
    {
        return view('vehicle.view.add');
    }

    public function VehiclePost(Request $request)
    {
        $save = new \App\Models\Vehicle;
        $save->vehicle_name = $request->vehicleName;
        $save->desc = $request->desc;
        $save->status = $request->status;
        $save->created_by = $request->createdBy;

        $save->save();

        return redirect('/admin/vehicle-list')->with('status', 'Vehicle Success Added!');
    }

    public function show($id)
    {
        $vehicles = \App\Models\Vehicle::find($id);
        $this->data['title'] = 'Vehicle Detail';

        return view('vehicle.view.detail', ['vehicles' => $vehicles], $this->data);
    }

    public function VehicleUpdate(Request $request, $id)
    {
        $update = new \App\Models\Vehicle;
        $update = Vehicle::where('id', $id)->first();
        $update->vehicle_name = $request->vehicleName;
        $update->desc = $request->desc;
        $update->status = $request->status;
        $update->created_by = $request->createdBy;

        $update->update();

        return redirect('/admin/vehicle-list')->with('status', 'Vehicle Updated');
    }

    public function getDataVehicle($value='')
    {
        # code...
        $data   = \App\Models\Vehicle::where('status', 1)->get();

        if(count($data) > 0){

            return $result      = array('result' => 1, 'error'=> 'Fetching data success', 'data' => $data);
        }

        return $result      = array('result' => 0, 'error'=> 'Fetching data success', 'data' => []);
    }
}
