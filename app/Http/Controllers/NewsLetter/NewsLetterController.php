<?php

namespace App\Http\Controllers\NewsLetter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class NewsLetterController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(){

     	if (\Request::ajax()){
	   		$partner = \App\Models\Newsletter::orderBy('id','desc')->get();

	        $i		= 0;
	        $data	= [];
	        $output = array(
	  			"data" => []
	  		);

	        foreach ($partner as $index => $list){

                $link_edit            = url('admin/newsletter/edit/').'/'.$list->id ;
                $link_delete          = url('admin/newsletter/delete/').'/'.$list->id ;
		        
                $output['data'][$i][] = $index+1;
                $output['data'][$i][] = $list->title;
                $output['data'][$i][] = $list->category;
                $output['data'][$i][] = Carbon::parse($list->created_at)->format('d M Y - H:i:s');
                if ($list->status == 1){
                    $output['data'][$i][] = 'Active';
                }
                else{
                    $output['data'][$i][] = 'Inactive';
                }
                $output['data'][$i][] = Carbon::parse($list->mulai_dari)->format('d M Y');
                $output['data'][$i][] = Carbon::parse($list->sampai_dengan)->format('d M Y');

                $output['data'][$i][] = '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit Location"><i class="icon-edit" style="color: rgb(0,146,69)"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete Location"><i class="icon-remove-sign" style="color: rgb(0,146,69)"></i></a></center>';
	    			$i++;
	    	}
	        return json_encode($output);
    	}

    	return view('newsletter.view.list');

    }

    public function NewsLetterAdd(){

        return view('newsletter.view.add');
    }

    public function NewsLetterPost(Request $request){

        $input                  = $request->all();

        $store 					= \App\Models\Newsletter::create($input);

        return redirect('/admin/newsletter-list')->with('status', 'Newsletter Success Added!');;
    }

    public function NewsLetterdestroy($id){

      $data= \App\Models\Newsletter::find($id);
      $data->delete();
      return redirect('/admin/newsletter-list')->with('status', 'Newsletter Success Deleted!');
    }

    public function NewsLetteredit($id){
        // return $id;
        $this->data['title'] = "Newsletter Edit";
        $this->data['data'] = \App\Models\Newsletter::find($id);
        return view('newsletter.view.edit',$this->data);
   }

    public function update(Request $request, $id){

        $input                      = $request->all();
        
        $update                     = \App\Models\Newsletter::find($id);
        
        $update->update($input);

        return redirect('/admin/newsletter-list')->with('status', 'Newsletter Success Edited!');


     }
}
