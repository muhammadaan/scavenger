<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::check()){            
            if(Auth::user()->role_id == 0){
                // return $next($request);
                return redirect()->route('dashboard');
            }                        
        }

        return redirect()->route('logout');
        

    } 
    public function dashboard()
    {
        return view('dashboard.index');
    }

    public function logout(){
        Auth::logout();
        return view('auth.login');
     }
}
