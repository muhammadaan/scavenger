<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerHelper extends Controller
{
    //
    public function verificationuser($value='')
    {
    	# code...
    	$value 	= base64_decode($value);
    	$data 	= \App\User::find($value);

    	if(count($data) > 0)
    	{
    		$data->status 	= 1;
    		$data->save();

    		$this->data['name']   = $data->first_name.' '.$data->last_name;
    		$this->data['status'] = 1;

            $user = $data;

    		// return view('customer.view.verificationuser', $this->data);
            return view('mails.verifikasiAkun', compact('user'));
    		
    	}

    	$this->data['status'] = 0;
    	return view('customer.view.verificationuser', $this->data);    	
    }
}
