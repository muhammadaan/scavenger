<?php

namespace App\Http\Controllers\Customer;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Auth;

class CustomerController extends Controller
{

    //  public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    
    public function index()
    {
        $customers = \App\Models\User::whereNotIn('role_id', [0])->orderBy('created_at','desc')->get();
        $this->data['title'] = "Customer List";

        return view('customer.view.list', ['customers'=>$customers], $this->data);
 
    }

    public function CustomerShow($id)
    {
        $customerById = \App\Models\User::find($id);
        $this->data['title'] = "Customer List";

        return view('customer.view.detail', compact('customerById'), $this->data);
    }

    public function CustomerAdd(){

        // return view('Users.view.add');
    }

    public function CustomerPost(Request $request){

        // $input                        = $request->all();
        // $input['password']            = bcrypt($request->password);
        // $input['image']               = "";

        // $store                        = \App\Models\Admin::create($input);

        //  return redirect('/admin/users-list')->with('status', 'User Success Added!');;
    }

    public function Customerdestroy($id){

      // $data= \App\Models\Admin::find($id);
      // $data->delete();
      // return redirect('/admin/users-list')->with('status', 'User Success Deleted!');
    }

    public function Customeredit($id){
        
        $this->data['title'] = "Scavenger Edit";
        $this->data['data'] = \App\Models\User::find($id);

        return view('customer.view.edit',$this->data);
   }

    public function update(Request $request, $id){

        // $input                      = $request->all();
        // $update                     = \App\Models\Admin::find($id);
        
        // if($request->password == null){
        //     $input['password']      = $update->password;
        // }else{
        //     $input['password']      = bcrypt($request->password);
        // }

        // $update->update($input);

        // return redirect('/admin/users-list')->with('status', 'User Success Edited!');


     }

    public function myRecycle(){
        $user_id = Auth::user()->id;
        // $user_id = 1;
        $data = array();
        $dataDetails = array();
        $totalWeight = array();
        $categories = \App\Models\Category::all();
        $requests_user = \App\Models\Request::where('customer_id', $user_id)->with('detail')->get();

        foreach ($requests_user as $each_request) {
            $temp_request = $each_request->detail;
            foreach ($temp_request as $detail) {
                array_push($dataDetails, $detail);
            }
        }
        foreach ($categories as $category) {
            $tempWeight = array();
            foreach ($dataDetails as $dataDetail) {
                if($category->id == $dataDetail->category_id){
                array_push($tempWeight, $dataDetail->weight_estimate);
            }
        }
        $data[$category->id] =array('id'=>$category->id, 'name'=>$category->category_name, 'weight'=>array_sum($tempWeight));
            array_push($totalWeight, $data[$category->id]['weight']);
        }
        return $result = array('total_weight'=>array_sum($totalWeight), 'total_request'=>count($requests_user), 'data'=>$data);
    }

}
