<?php

namespace App\Http\Controllers\Donation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;

class ConfirmationDonationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function confirmation_donations(Request $request)
    {
        # code...
        try {

            \DB::beginTransaction();        

            $input            = $request->all();
            $uploadpicture    = \App\Http\Controllers\Request\RequestHelper::uploadpicture($input);
            $input['picture'] = $uploadpicture;
            $data             = \App\Models\ConfirmationDonation::create($input);

            \DB::commit();

            $result = array('result'=> 1, 'message'=> 'success', 'data' => $data);

        } catch (\Exception $e) {

            \DB::rollback();

            Log::error('confirmation_donations '.$e->getMessage(). ' '. $e->getFile().' '. $e->getLine());
            $result = array('result'=>0, 'message'=> $e->getMessage().' on file '.$e->getFile(). ' line '.$e->getLine(), 'data' => []);
        } 

        return $result;
    }
}
