<?php

namespace App\Http\Controllers\Donation;

use App\Models\Donation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Donation\DonationHelper;

class DonationController extends Controller
{
    public function index()
    {
        $donations = \App\Models\Donation::orderBy('created_at','desc')->get();
//return $donations;
        $no = 0;
        $this->data['title'] = "Donation List";

        return view('donation.view.list',compact('no'), ['donations'=>$donations], $this->data);
    }


    public function show($id)
    {
        $donationById = \App\Models\Donation::find($id);
        $confirmDonation = \App\Models\ConfirmationDonation::where('donation_id', '=', $id)->first();
//        return $confirmDonation;

        $this->data['title'] = "Donation Detail";

        return view('donation.view.detail', compact('donationById', 'confirmDonation'), $this->data);
    }

    public function edit($id)
    {
        $donationById = Donation::find($id);

        return view('donation.view.detail', compact('donationById'));
    }

    public function update(Request $request, $id)
    {
        try {

            \DB::beginTransaction();

            $input  = $request->all();

            $updDonation = new \App\Models\Donation;
            $updDonation = Donation::where('id', $id)->first();            

            if($updDonation->request_id == 0 && $input['status'] == 1){
                $gamification   = \App\Models\Gamification::where('user_id', $updDonation->user_id)->first();

                if(Count($gamification) == 0){
                    $newGamification    = \App\Models\Gamification::create([
                                            'user_id' => $updDonation->user_id,
                                            'watering' => 1,
                                            'fertilizer' => $updDonation->amount,
                                        ]);

                } else {

                    if($updDonation->status == 0){
                        $gamification->watering   = $gamification->watering + 1;
                        $gamification->fertilizer = $gamification->fertilizer + $updDonation->amount;
                        $gamification->save();
                    }
                }
            }

            $updDonation->status = $input['status'];
            $updDonation->note   = $input['note'];
            $updDonation->save();

            $pintDonation        = DonationHelper::pointdonation($updDonation);

            \DB::commit();

            if($updDonation->status == 1){
                $mail = \App\Http\Controllers\MailService\MailServiceController::TerimaKasihSudahDonasi($updDonation);
            }

            return redirect('admin/donation-list');
        } catch (\Exception $e) {

            \DB::rollback();
            return $e->getMessage().' '.$e->getFile().' '.$e->getLine();
        }
    }
}
