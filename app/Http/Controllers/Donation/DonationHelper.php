<?php

namespace App\Http\Controllers\Donation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Donation\DonationController;
use App\Http\Controllers\Point\PointController;



/*
*) donations
add field user_id di table donations



*) donations
-> user request donation setelah selesai request recycle, bawa request_id. save di table donation status 0.
-> admin verifikasi donation, status berubah jadi 1 dan donation menjadi (Poin = (Poin recycle + poin per  weight)  * points score)


->untuk donation yg tidak recycle
->Jika user hanya melakukan donasi dan tidak melakukan recycle maupun donasi dengan melakukan recycle, maka ketika donasi sudah verified user akan mendapatkan booster dengan ketentuan sebagai berikut :

-> add field booster_usage_flush (0 & 1)
-> setelah update 3 booster_usage_flush menjadi 1, setiap mendapat penambahan point 15. Di point history desc diberi keterangan "Point Gamification Fertilizer 1"

-> add field booster_usage_substrate (total amount donation yg belum di substratte, setiap keliapatan 300.000 dapat di eksekusi. setiap eksekusi di update dikurangi 300.000. seperti history amount)
-> setelah eksekusi update dikurangi dan penambahan point 20, di point history desc beri keterangan "Point Gamification Substrate 300000"

/////////////////////////////|
/////////////////////////////|
*) point score //////////////|
/////////////////////////////|
donation   = point score ////|
0-150K     = 25% ////////////|
151-300K   = 50% ////////////|
301-450K   = 75% ////////////|
above 450K = 150% ///////////|
/////////////////////////////|
/////////////////////////////|

 */



class DonationHelper extends Controller
{
    ///
    public static function pointdonation($donation='')
    {
    	# code...
    	$pointrecycle 	= PointController::pointrecycle();
    	$pointperkg 	= 1;

    	if($donation->status == 1){

    		if($donation->request_id > 0){
    			//Poin donation = (Poin recycle + poin per  weight)  * points score
    			//
    			$pointdonation    = ($pointrecycle + (round($donation->request->berat_realisasi) * $pointperkg)) * self::pointscore($donation->amount);
    			$getPointDonation = PointController::getPointDonation($donation, round($pointdonation), 'Point donation with recyle '.$donation->request_id);
    		}
    	}
    }

    public static function pointscore($value='')
    {
    	# code...
    	if($value > 0 && $value <= 150000){
    		return 25/100;
    	}
    	if($value > 150000 && $value <= 300000){
    		return 50/100;
    	}
    	if($value > 300000 && $value <= 450000){
    		return 75/100;
    	}
    	if($value > 450000){
    		return 150/100;
    	}
    }
}
