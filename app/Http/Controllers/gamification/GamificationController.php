<?php

namespace App\Http\Controllers\gamification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GamificationController extends Controller
{
    //
    public function watering(Request $requests)
    {
    	# code...
    	try {
    		
	    	$input 	= $requests->all();

	    	\DB::beginTransaction();
	    		$gamification 	= \App\Models\Gamification::where('user_id', $input['user_id'])->firstOrFail();

	    		if($gamification->watering >= 3){

	    			$gamification->watering = $gamification->watering - 3;
	    			$gamification->save();

	    			$addpoint 	= \App\Http\Controllers\Point\PointController::storePoint($gamification->user, 15, 'Point Gamification Watering');
	    		}

	    	\DB::commit();

	    	return $result = array('result'=> 1, 'error'=>'Fetching data success!', 'data'=> '');
    	} catch (\Exception $e) {
    		\DB::rollback();

    		return $result = array('result'=> 0, 'error'=>$e->getMessage().' '.$e->getFile().' '.$e->getLine(), 'data'=> '');    		
    	}    	
    }

    public function fertilizer(Request $requests)
    {
    	# code...
    	try {
    		
	    	$input 	    = $requests->all();
	    	$fertilizer = 200000;

	    	\DB::beginTransaction();
	    		$gamification 	= \App\Models\Gamification::where('user_id', $input['user_id'])->firstOrFail();

	    		if($gamification->fertilizer >= $fertilizer){

	    			$gamification->fertilizer = $gamification->fertilizer - $fertilizer;
	    			$gamification->save();

	    			$addpoint 	= \App\Http\Controllers\Point\PointController::storePoint($gamification->user, 20, 'Point Gamification Fertilizer');
	    		}

	    	\DB::commit();

	    	return $result = array('result'=> 1, 'error'=>'Fetching data success!', 'data'=> '');
    	} catch (\Exception $e) {
    		\DB::rollback();

    		return $result = array('result'=> 0, 'error'=>$e->getMessage().' '.$e->getFile().' '.$e->getLine(), 'data'=> '');    		
    	}    	
    }
}
