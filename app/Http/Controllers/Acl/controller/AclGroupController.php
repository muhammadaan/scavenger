<?php

namespace App\Http\Controllers\Acl\controller;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
// use App\Model\Role;

class AclGroupController extends Controller
{
    //
    public function index(){
        // return 1;
    	if (\Request::ajax()){
	   		$location = \App\Models\Role::orderBy('name', 'asc')->get();

	        $i		= 0;
	        $data	= [];
	        $output = array(
	  			"data" => []
	  		);

	        foreach ($location as $index => $list){

	            $link_edit   = url('admin/users-group').'/'.$list->id ;     	
	            $link_delete = url('admin/users-group/delete').'/'.$list->id ;     	
		        
		        $output['data'][$i][]	= $index+1;
				$output['data'][$i][]	= $list->name;
				$output['data'][$i][]	= $list->desc;			
				if($list->status == 1){
					$output['data'][$i][]	= 'Active';
				} else {
					$output['data'][$i][]	= 'Inactive';
				}
		        $output['data'][$i][]	= '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit Location"><i class="icon-edit" style="color: rgb(0,146,69);"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete Location"><i class="icon-remove-sign" style="color: rgb(0,146,69);"></i></a></center>';
	    			$i++;
	    	}
	        return json_encode($output);
    	}

    	return view('aclgroup.index');
    }

    public function create(){

    	return view('aclgroup.form.create');
    }

    public function store(Request $request){

    	$input 	                      = $request->all();
    	$input['route_access_list']	  = json_encode(['routelist' => $request->input('routelist')]);
    	$input['user_id']	          = 0;
    	$input['permission_list_id']  = 0;
    	$input['status']	          = 1;

    	$store 	                      = \App\Models\Role::create($input);

    	return Redirect::to('admin/users-group');
    }

    public function show($id){

    	$data 	= \App\Models\Role::find($id);
    	
    	return view('aclgroup.form.edit')->with(['data' => $data]);
    }

    public function update(Request $request, $id){
    	// return $request;
    	$input 	                    = $request->all();
    	$input['route_access_list'] = json_encode(['routelist' => $request->input('routelist')]);

    	$update 	                = \App\Models\Role::find($id);
    	$update->update($input);

        return redirect('admin/users-group');
    	// return redirect::to('users-group');

    }

    public function delete($id){

          $data= \App\Models\Role::find($id);
          $data->delete();
          return redirect::to('admin/users-group')->with('status', 'UserGroup Success Deleted!');
    
    }

    
}
