<?php

namespace App\Http\Controllers\Point;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Point;
use App\Models\PointHistory;

class PointController extends Controller
{
    //
    //
   	public static function getpointcompleterequest($data, $status)
   	{
   		# code...
         $recylclecomplete = self::pointrecycle();
         $perkg            = self::perkg();

         $weight           = round($data->berat_realisasi);

         $totalpoint       = $recylclecomplete + ($perkg * $weight);

         $sotepoint        = self::storePoint($data->user, $totalpoint, $status);
   		
   	}

      public static function getPointDonation($donation='', $totalpoint, $status)
      {
         # code...
         $sotepoint        = self::storePoint($donation->request->user, $totalpoint, $status);
      }

      public static function storePoint($customer, $totalpoint, $status)
      {
         # code...
         #     
         $pointsekrg   = 0;
         $currentpoint = Point::where('user_id', $customer->id)->first();
         
         if($currentpoint != null){
            $pointsekrg  = $currentpoint->point;  
         }
            $point   = Point::updateOrCreate([
               'user_id' => $customer->id,
            ],[
               'point' => $pointsekrg + $totalpoint,
            ]);                        

         $requesthistory   = self::storepointhistory($point, $customer, $totalpoint, $status);
      }

   	public static function storepointhistory($point, $customer, $totalpoint, $status)
   	{
   		# code...
   		if($status == '3')
   		{
   			$desc 	= 'Recycle Complete';
   		} else {
   			$desc 	= $status;
   		}

   		$point_histories 	= PointHistory::create([
   			'user_id' => $customer->id,
   			'point_id' => $point->id,
   			'request_id' => 0,
   			'topup_point' => $totalpoint,
   			'desc' => $desc,
   		]);
   	}

      public static function pointrecycle($value='')
      {
         # code...
         return 5;
      }

      public static function perkg($value='')
      {
         # code...
         return 3;
      }

      //////////////////////////// point scavenger
      ///
      
      public static function pointscavenger($rating)
      {
         # code...
         /*
         Poin  = (jumlah jarak (km) * 200)  + (jumlah berat (kg)*1000).
         Total score poin = (Poin * Presentase) / 100.
         */
         $poin    = (($rating->request->jarak / 1000) * 200) + ($rating->request->berat_realisasi * 1000);
         $totalscorepoin = ($poin * $this->presentaseratingscave($rating->rating)) / 100;

         $storepoint    = self::storePoint(\App\Models\User::find($rating->scavenger_id), $totalscorepoin, 3);
      }

      public function presentaseratingscave($value='')
      {
         # code..
         # 
         $value = $value / 20;

         if ($value <= 1) {
            # code...
            return (90 / 100);
         }
         if ($value > 1 && $value <= 2) {
            # code...
            return (95 / 100);
         }
         if ($value > 2 && $value <= 3) {
            # code...
            return (100 / 100);
         }
         if ($value > 3 && $value <= 4) {
            # code...
            return (105 / 100);
         }
         if ($value > 4) {
            # code...
            return (115 / 100);
         }
      }
}
