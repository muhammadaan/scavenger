<?php

namespace App\Http\Controllers\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index(){

    	$config 			= \App\Models\AboutUs::first();

    	return view('about.view.show',compact('config'));
    }

    public function update(Request $request){
    	$input 				= $request->all();

    	$config 			= \App\Models\AboutUs::first()->update($input);

    	return redirect('/admin/about-list')->with('status', 'About Success Edited!');

    }
}
