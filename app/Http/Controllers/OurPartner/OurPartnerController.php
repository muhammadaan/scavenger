<?php

namespace App\Http\Controllers\OurPartner;

use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;


class OurPartnerController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(){

     	if (\Request::ajax()){
	   		$partner = \App\Models\Partner::orderBy('name', 'asc')->get();

	        $i		= 0;
	        $data	= [];
	        $output = array(
	  			"data" => []
	  		);

	        foreach ($partner as $index => $list){

	            $link_edit   = url('admin/partner/edit/').'/'.$list->id ;     	
	            $link_delete = url('admin/partner/delete/').'/'.$list->id ;     	
		        
		        $output['data'][$i][]	= $index+1;
				$output['data'][$i][]	= $list->name;
				$output['data'][$i][]	= $list->desc;			
				$output['data'][$i][]	= "<img src='".url('/storage/app/images')."/".$list->logo."' class='img-responsive'>";
			
		        $output['data'][$i][]	= '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit Location"><i class="icon-edit" style="color: rgb(0,146,69);"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete Location"><i class="icon-remove-sign" style="color: rgb(0,146,69);"></i></a></center>';
	    			$i++;
	    	}
	        return json_encode($output);
    	}
    	return view('ourpartner.view.list');

    }

    public function PartnerAdd(){

        return view('ourpartner.view.add');
    }

    public function PartnerPost(Request $request){

        define('UPLOAD_DIR', 'storage/app/');
        $image       = uniqid().".png";
        $imgpicture  = $request->file('image')->storeAs('images', $image);

        $partner       = new \App\Models\Partner;
        $partner->name = $request->name;
        $partner->desc = $request->desc;
        $partner->logo = $image;
        $partner->save();
        return redirect('/admin/partner-list')->with('status', 'Partner Success Added!');;
    }

    public function Partnerdestroy($id){

      $data= \App\Models\Partner::find($id);
      $data->delete();
      return redirect('/admin/partner-list')->with('status', 'Partner Success Deleted!');
    }

    public function Partneredit($id){
        // return $id;
        $this->data['title'] = "Partner Edit";
        $this->data['data'] = \App\Models\Partner::find($id);
        return view('ourpartner.view.edit',$this->data);
   }

    public function update(Request $request, $id){

        $input                      = $request->all();

        define('UPLOAD_DIR', 'storage/app/');
        $image       = uniqid().".png";
        $imgpicture  = $request->file('image')->storeAs('images', $image);

        $partner       = new \App\Models\Partner;
        $partner       = Partner::where('id', $id)->first();
        $partner->name = $request->name;
        $partner->desc = $request->desc;
        $partner->logo = $image;
        $partner->update();

        return redirect('/admin/partner-list')->with('status', 'User Success Edited!');


     }
}
