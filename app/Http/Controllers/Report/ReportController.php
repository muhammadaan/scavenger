<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;

class ReportController extends Controller
{

    //  public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    
    public function index(){
        $this->data['title'] = "Report";
        return view('report.view.list',$this->data);
 
    }

    public function getDataReport(){

    // $users = \App\Models\Request::all();
    // // $donat = \App\Models\Donation::all();
    // // $reqdet = \App\Models\RequestDetail::all();


    //  $i		= 0;
    //  $data	= [];
    //  $output = array(
    //        "data" => []
    //    );

    //  foreach ($users as $index=>$list){
    //     $reqdet = \App\Models\RequestDetail::where('request_id',$list->id)->sum('weight_estimate');
    //     $reqid = \App\Models\RequestDetail::where('request_id',$list->id)->sum('request_id');
    //     $link_edit = url('/admin/request/edit').'/'.$list->id ;     	     
    //     $link_delete = url('/admin/request/delete').'/'.$list->id ; 
    //     // $edits = User::where('id',$list->id)->first();    	     
    //     // $roles = \App\UserGroup::find($list->roles);
    //         $output['data'][$i][]    = $index+1;
    //         $output['data'][$i][]    = $reqid;           
    //         $output['data'][$i][]    = $list->status;
    //         $output['data'][$i][]    = $list->address;
    //         $output['data'][$i][]    = $reqdet;
    //         $output['data'][$i][]    = $list->created_at;            
    //         $output['data'][$i][]    = '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit users"><i class="icon-edit"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete users"><i class="icon-remove-sign"></i></a></center>';
    //             $i++;
    //     }
    //     return json_encode($output);

    }

    public function ReportAdd(){

        // return view('Users.view.add');
    }

    public function ReportPost(Request $request){

        // $input                        = $request->all();
        // $input['password']            = bcrypt($request->password);
        // $input['image']               = "";

        // $store                        = \App\Models\Admin::create($input);

        //  return redirect('/admin/users-list')->with('status', 'User Success Added!');;
    }

    public function Reportdestroy($id){

      // $data= \App\Models\Admin::find($id);
      // $data->delete();
      // return redirect('/admin/users-list')->with('status', 'User Success Deleted!');
    }

    public function Reportedit($id){
        
        // $this->data['title'] = "Scavenger Edit";
        // $this->data['data'] = \App\Models\User::find($id);
        // return view('scavenger.view.edit',$this->data);
   }

    public function update(Request $request, $id){

        // $input                      = $request->all();
        // $update                     = \App\Models\Admin::find($id);
        
        // if($request->password == null){
        //     $input['password']      = $update->password;
        // }else{
        //     $input['password']      = bcrypt($request->password);
        // }

        // $update->update($input);

        // return redirect('/admin/users-list')->with('status', 'User Success Edited!');


     }

     public function showrequest()
     {
        $this->data['title'] = "Request Report";
        // $this->data['member'] = \App\student2::orderBy('created_at', 'asc')->get();
        return view('report.request.view.list',$this->data);
     }

    //   public function getDataReportRequest(){

    // $users = \App\Models\Request::all();
    // // $donat = \App\Models\Donation::all();
    // // $reqdet = \App\Models\RequestDetail::all();
    // // $reqhis = \App\Models\RequestHistory::all();


    //  $i		= 0;
    //  $data	= [];
    //  $output = array(
    //        "data" => []
    //    );

    //  foreach ($users as $index=>$list){
    //     $reqdet = \App\Models\RequestDetail::where('request_id',$list->id)->sum('weight_estimate');
    //     $reqid = \App\Models\RequestDetail::where('request_id',$list->id)->sum('request_id');
    //     $reqhis = \App\Models\RequestHistory::where('request_id',$list->id)->sum('request_id');
    //     $link_edit = url('/admin/request/edit').'/'.$list->id ;     	     
    //     $link_delete = url('/admin/request/delete').'/'.$list->id ; 
    //     // $edits = User::where('id',$list->id)->first();    	     
    //     // $roles = \App\UserGroup::find($list->roles);
    //         $output['data'][$i][]    = $index+1;
    //         $output['data'][$i][]    = $reqid;           
    //         $output['data'][$i][]    = $list->status;
    //         $output['data'][$i][]    = $list->address;
    //         $output['data'][$i][]    = $reqdet;
    //         $output['data'][$i][]    = $list->created_at;            
    //         $output['data'][$i][]    = '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit users"><i class="icon-edit"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete users"><i class="icon-remove-sign"></i></a></center>';
    //             $i++;
    //     }
    //     return json_encode($output);

    // }

     public function getDataReportRequest(Request $request){

    // $province = $request->input('province');
    

    $query = Request::selectRaw("*");
    if ($request->has('start_date')) {
      $start = date('Y-m-d H:i:s', strtotime($request->input('start_date')));
      $query->whereRaw('DATE(order_date) >= "'.$start.'"');
    }
    if ($request->has('end_date')) {
      $start = date('Y-m-d H:i:s', strtotime($request->input('end_date')));
      $query->whereRaw('DATE(order_date) <= "'.$start.'"');
    }
    if ($request->has('status')) {
      $query->where('status', $request->input('status'));
    }
    

    // if ($request->has('province')) {
    //   $query->whereHas('student', function($q) use ($province){
    //                                             $q->where('province', $province);
    //                                         });
    // }
    
   
    if ($query->count() > 0) {
      $test = $query->get();
      return view('report.request.view.list',compact('test'));
    } else {
      
      return redirect('report.request.view.list')->with('status', 'No Data Found.');      
    }
          
  }

  public function realoadtable(){

     // $this->data['status'] = \App\RequestHistory::orderBy('status', 'asc')->get();
     // return view('report.request.view.list',$this->data);
  	 $this->data['title'] = "Request Report";
        // $this->data['member'] = \App\student2::orderBy('created_at', 'asc')->get();
     return view('report.request.view.table',$this->data);

  } 

  function showdata(){

       $fromDate = Input::get('start_date');
        // $toDate   = \Carbon\Carbon::parse(Input::get('end_date'));
        $status    = Input::get('status');        

         // return $toDate;

        if($fromDate == ''){
          if ($status == '') {
            $test     = \App\Models\Request::all();
          }else{
            $test     = \App\Models\Request::where('status','=',  $status)->get();
          }
          if(count($test) > 0){
                      
                  return view('report.request.view.table',compact('test'));

               }else{
return view('report.request.view.table',compact('test'));
          }  
        }else{
          $fromDate = \Carbon\Carbon::parse($fromDate);
          if ($status == '') {           
          $test     = \App\Models\Request::where('order_date' ,'>',  $fromDate)->get();
          }else{
            $test     = \App\Models\Request::where('order_date' ,'>',  $fromDate)->where('status','=',  $status)->get();
          }
                  // return $test;

                if(count($test) > 0){

                  return view('report.request.view.table',compact('test'));

               }else{
                  return $fromDate;
                 echo "kosong";
          }  
        }
        
          
  }


  public function showdatareport()
  {
     $fromDate = Input::get('start_date');
        // $toDate   = \Carbon\Carbon::parse(Input::get('end_date'));
        // $status    = Input::get('status');        

         // return $toDate;

        if($fromDate == ''){
           $test     = \App\Models\Request::all();
               // return $test;
                  return view('report.view.showdata',compact('test'));               
        }else{
          $fromDate = \Carbon\Carbon::parse($fromDate);                   
          $test     = \App\Models\Request::where('order_date' ,'>',  $fromDate)->get();
        // return $test;
                if(count($test) > 0){

                  return view('report.view.showdata',compact('test'));

               }else{
                  return $fromDate;
                 echo "kosong";
          }  
        }
  }



}
