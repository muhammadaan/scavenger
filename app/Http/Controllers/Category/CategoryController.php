<?php

namespace App\Http\Controllers\Category;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;

class CategoryController extends Controller
{

    //  public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    
    public function index(){
        $categories = \App\Models\Category::all();
        $this->data['title'] = "Request List";
        return view('category.view.list',['categories'=>$categories],$this->data);
 
    }


//    public function getDatacategory(){
//
//    $users = \App\Models\Category::all();
//    // $donat = \App\Models\Donation::all();
//    // $reqdet = \App\Models\RequestDetail::all();
//
//
//     $i		= 0;
//     $data	= [];
//     $output = array(
//           "data" => []
//       );
//
//     foreach ($users as $index=>$list){
//        // $reqdet = \App\Models\Category::where('request_id',$list->id)->sum('weight_estimate');
//        // $reqid = \App\Models\Category::where('request_id',$list->id)->sum('request_id');
//        $link_edit = url('/admin/category/edit').'/'.$list->id ;
//        $link_delete = url('/admin/category/delete').'/'.$list->id ;
//        // $edits = User::where('id',$list->id)->first();
//        // $roles = \App\UserGroup::find($list->roles);
//            $output['data'][$i][]    = $index+1;
//            $output['data'][$i][]    = "<img src='".url('/storage/app/images')."/".$list->image."' class='img-responsive'>";;
//            $output['data'][$i][]    = $list->category_name;
//            // $output['data'][$i][]    = $list->$desc;
//            $output['data'][$i][]    = Carbon::parse($list->created_at)->format('d M Y - H:i:s');
//            $output['data'][$i][]    = '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit users"><i class="icon-edit"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete users"><i class="icon-remove-sign"></i></a></center>';
//                $i++;
//        }
//        return json_encode($output);
//
//    }


    public function CategoryAdd(){

        return view('category.view.add');
    }

    public function CategoryPost(Request $request){

        $input                        = $request->all();
        $input['poto']      = $update->image;
        $input['nama']      = $update->category_name;
        $input['nama']      = $update->desc;
        $input['nama']      = $update->weight_total_category;

        $store                        = \App\Models\Admin::create($input);

         return redirect('/category-list')->with('status', 'User Success Added!');;
    }

    public function Categorydestroy($id){

      // $data= \App\Models\Admin::find($id);
      // $data->delete();
      // return redirect('/admin/users-list')->with('status', 'User Success Deleted!');
    }

    public function Categoryedit($id){
        
        $this->data['title'] = "Category Edit";
        $this->data['data'] = \App\Models\Category::find($id);
        return view('category.view.edit',$this->data);
   }

    public function update(Request $request, $id){

        $input                      = $request->all();
        $update                     = \App\Models\Category::find($id);
        
        
        $input['poto']      = $update->image;
        $input['nama']      = $update->category_name;
        $input['nama']      = $update->desc;
        $input['nama']      = $update->weight_total_category;
        
        $update->update($input);

        return redirect('/admin/category-list')->with('status', 'Category Success Edited!');


     }



}
