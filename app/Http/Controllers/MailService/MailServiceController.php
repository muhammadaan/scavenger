<?php

namespace App\Http\Controllers\MailService;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;

class MailServiceController extends Controller
{
    //send mail semua taro di sini yaa
    public static function KonfirmasiPendaftaran ($user) {
    	try {
    		
		    $to      = $user->email;
		    $from    = 'no-reply@scavenger.id';
		    $subject = 'Confirm Registration';
		    $link  	 = route('verificationuser', [base64_encode($user->id)]);
		    // $link    = url('/api').'/'.'verificationuser'.'/' . base64_encode($user->id);

		    \Mail::send('mails.konfirmasiPendaftaran', compact('user','link'), function ($message) use ($to,$from,$subject) {
		        $message->from($from)
		                ->to($to)
		                ->subject($subject);
		    });

    	} catch (\Exception $e) {

    		Log::error('Send mail error KonfirmasiPendaftaran '.$e->getMessage().' '.$e->getFile().' '.$e->getLine());
    		
    	}
    }

    public static function PermintaanPengambilanBarang ($request) {
    	try {
    		
		    $to      = $request->user->email;
		    $from    = 'no-reply@scavenger.id';
		    $subject = 'Request for Taking Items';		    

		    \Mail::send('mails.requestRecycle', compact('request'), function ($message) use ($to,$from,$subject) {
		        $message->from($from)
		                ->to($to)
		                ->subject($subject);
		    });
		    
    	} catch (\Exception $e) {

    		Log::error('Send mail error PermintaanPengambilanBarang '.$e->getMessage().' '.$e->getFile().' '.$e->getLine());
    		
    	}
    }

    public static function VerifikasiDonasi($donation) {
    	try {
    		
		    $to      = $donation->user->email;
		    $from    = 'no-reply@scavenger.id';
		    $subject = 'Verify Donate';		    

		    \Mail::send('mails.VerifikasiDonasi', compact('donation'), function ($message) use ($to,$from,$subject) {
		        $message->from($from)
		                ->to($to)
		                ->subject($subject);
		    });
		    
    	} catch (\Exception $e) {

    		Log::error('Send mail error PermintaanPengambilanBarang '.$e->getMessage().' '.$e->getFile().' '.$e->getLine());
    		
    	}
    }

    public static function TerimaKasihSudahDonasi($donation) {
    	try {
    		
		    $to      = $donation->user->email;
		    $from    = 'no-reply@scavenger.id';
		    $subject = 'Thank You for Donation';		    

		    \Mail::send('mails.TerimaKasihSudahDonasi', compact('donation'), function ($message) use ($to,$from,$subject) {
		        $message->from($from)
		                ->to($to)
		                ->subject($subject);
		    });
		    
    	} catch (\Exception $e) {

    		Log::error('Send mail error PermintaanPengambilanBarang '.$e->getMessage().' '.$e->getFile().' '.$e->getLine());
    		
    	}
    }

    public static function acceptOrder ($request) {
    	try {
    		
		    $to      = $request->user->email;
		    $from    = 'no-reply@scavenger.id';
		    $subject = 'Request for Taking Items';		    

		    \Mail::send('mails.acceptOrder', compact('request'), function ($message) use ($to,$from,$subject) {
		        $message->from($from)
		                ->to($to)
		                ->subject($subject);
		    });
		    
    	} catch (\Exception $e) {

    		Log::error('Send mail error acceptOrder '.$e->getMessage().' '.$e->getFile().' '.$e->getLine());
    		
    	}
    }

    public function forgotpassword(Request $request) {
    	try {

    		$input 	= $request->all();
    		
		    $to      = $input['email'];
		    $from    = 'no-reply@scavenger.id';
		    $subject = 'Request for reset password';		    
		    $link  	 = route('changepassword', [base64_encode($to)]);

		    \Mail::send('mails.forgotpassword', compact('to', 'link'), function ($message) use ($to,$from,$subject) {
		        $message->from($from)
		                ->to($to)
		                ->subject($subject);
		    });

		    return json_encode(["0" => "Success sending email", "result" => 1]);
		    
    	} catch (\Exception $e) {

    		Log::error('Send mail error acceptOrder '.$e->getMessage().' '.$e->getFile().' '.$e->getLine());
    		
    	}
    }

    public function forgotpasswordform($value='')
    {
    	# code...
    	$this->data['email'] = base64_decode($value);
    	return view('users.view.resetpassword', $this->data);
    }

    public function forgotpasswordpost(Request $request)
    {
    	# code...
    	$input = $request->all();
    	$reset = \App\Models\User::whereEmail($input['email'])->get();
    	if(count($reset) > 0){
    		foreach ($reset as $key => $value) {
    			# code...
    			$value->password = bcrypt($input['password']);
    			$value->save();
    		}
    	}

    	return \Redirect::to('http://scavenger.id/');
    	return $input['email'];
    }
}
