<?php

namespace App\Http\Controllers\Rating;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Rating\RatingHelper;

class RatingController extends Controller
{
    //
    public function __construct()
    {
    	$this->helper = new RatingHelper;    	
    }

   	public function ratingRequest(Request $request)
   	{
   		# code...
   		try {
   		    
   		    \DB::beginTransaction();

          $input     = $request->all();
          $data      = $this->helper->storeRating($input);
          $pointscav = \App\Http\Controllers\Point\PointController::pointscavenger($data);

   		    \DB::commit();

   		    $result = array('result'=> 1, 'message'=> 'success', 'data' => $data);


   		} catch (\Exception $e) {

   		    \DB::rollback();

   		    $result = array('result'=>0, 'message'=> $e->getMessage().' on file '.$e->getFile(). ' line '.$e->getLine(), 'data' => []);
   		}

   		return $result;
   	}
}
