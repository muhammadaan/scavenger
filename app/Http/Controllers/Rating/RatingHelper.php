<?php

namespace App\Http\Controllers\Rating;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Rating;

class RatingHelper extends Controller
{
    //    

    public function storeRating($value='')
    {
    	# code...
    	// return $this->model->get();	

		$request 	             = \App\Models\Request::findOrFail($value['request_id']);
        $value['user_id']        = $request->customer_id;
    	$value['scavenger_id'] 	 = $request->scavanger_id;

		$resultratingcounting 	 = $this->getLastRating($value);
		
		$store 	= Rating::create([
			'request_id' => $value['request_id'],
            'user_id' => $value['user_id'],
			'scavenger_id' => $request->scavanger_id,
			'rating' => $value['rating'],
			'rating_avg' => $resultratingcounting['rating_avg'],
			'rating_count' => $resultratingcounting['rating_count'],
			'note' => $value['note'],
		]);		

		return $store;
    }

    public function getLastRating($value='')
    {
    	# code...
    	try {
    		
    		$rating 	= Rating::where('scavenger_id', $value['scavenger_id'])->orderBy('id', 'desc')->firstOrFail();    		
    		$rating_avg = ($value['rating'] + ($rating->rating_avg * $rating->rating_count)) / ($rating->rating_count + 1);

    		return [ 'rating_avg' => $rating_avg, 'rating_count' => ($rating->rating_count + 1) ];

    	} catch (\Exception $e) {
    		
    		return [ 'rating_avg' => ($value['rating'] / 1), 'rating_count' => 1 ];
    	}
    }
}
