<?php

namespace App\Http\Controllers\Rank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RankUserController extends Controller
{
    //
    public function rankuser(Request $request)
    {
    	# code...
    	// $data      = \App\Models\Request::selectRaw('*, count(*) as ranking')->groupBy('customer_id')->orderByRaw('count(*) DESC')->take(50)->get();
    	$data = self::queryranktop();

    	$user = \DB::select(\DB::raw('SELECT t5.* FROM (SELECT t4.*, @rank := @rank + 1 AS rank FROM (SELECT *, (t3.total + (t3.point / 100000)) AS total_akhir FROM (SELECT 
t1.*, 
CASE WHEN t2.point IS NOT NULL 
       THEN t2.point
       ELSE 0
END AS point,
CONCAT(t6.first_name," ",t6.last_name) AS name
FROM (SELECT
    customer_id,    
    COUNT(*) as total    
FROM scavanger_requests
GROUP BY  customer_id ) t1 LEFT JOIN scavanger_points t2 ON t1.customer_id = t2.user_id INNER JOIN scavanger_users t6 ON t6.id = t1.customer_id WHERE t6.role_id = 1) t3  ORDER BY (t3.total + (t3.point / 100000)) DESC) t4 , (SELECT @rank := 0) r) t5 WHERE t5.customer_id = "'.$request->input('user_id').'" ORDER BY t5.rank'));

        $ranking = [];
        $rank    = 1;
        $loop    = 10;

        if(count($data) < 9){
            $loop = count($data);
        }
// dd($data[0]->rank);
        foreach ($data as $key => $value) {
            # code...
            array_push($ranking, 
                [
                    'id' => $value->customer_id,
                    'position' => $value->rank,
                    'point' => $value->point,
                    'name' => $value->name,
                    'count' => $value->total,
                    'total_akhir' => $value->total_akhir,
                ]);
            $rank = $rank + 1;
            if($rank == 11){
                break;
            }
        }   
        

        if(count($user) > 0){
            $user  = $user[0];
        } else {
            $cust = \App\Models\User::find($request->input('user_id'));
            $user = [        
                "customer_id"=> $request->input('user_id'),
                'position' => count($data) + 1,
                "total"=> 0,
                "rank"=> 0,
                "point"=> 0,
                "name"=> $cust->first_name.' '.$cust->last_name,
            ];    
        }
    	
    	
    	$result = array('result'=> 1, 'message'=> 'success', 'data' => ['rangking' => $ranking, 'user' => $user]);
    	return $result;
    }

    public static function queryranktop($value='')
    {
        # code...
        $data = \DB::select(\DB::raw('SELECT t5.* FROM (SELECT t4.*, @rank := @rank + 1 AS rank FROM (SELECT *, (t3.total + (t3.point / 100000)) AS total_akhir FROM (SELECT 
t1.*, 
t1.customer_id AS id,
CASE WHEN t2.point IS NOT NULL 
       THEN t2.point
       ELSE 0
END AS point,
CONCAT(t6.first_name," ",t6.last_name) AS name
FROM (SELECT
    customer_id,    
    COUNT(*) as total    
FROM scavanger_requests
GROUP BY  customer_id ) t1 LEFT JOIN scavanger_points t2 ON t1.customer_id = t2.user_id INNER JOIN scavanger_users t6 ON t6.id = t1.customer_id WHERE t6.role_id = 1) t3  ORDER BY (t3.total + (t3.point / 100000)) DESC) t4 , (SELECT @rank := 0) r) t5 ORDER BY t5.rank'));

        return $data;

    }


    public static function queryranktopscav($value='')
    {
        # code...
        $data = \DB::select(\DB::raw('SELECT t5.* FROM (SELECT t4.*, @rank := @rank + 1 AS rank FROM (SELECT *, (t3.total + (t3.point / 100000)) AS total_akhir FROM (SELECT 
t1.*, 
t1.scavanger_id AS id,
CASE WHEN t2.point IS NOT NULL 
       THEN t2.point
       ELSE 0
END AS point,
CONCAT(t6.first_name," ",t6.last_name) AS name
FROM (SELECT
    scavanger_id,        
    CASE WHEN SUM(berat_realisasi) IS NOT NULL 
           THEN SUM(berat_realisasi)
           ELSE 0
    END AS total
FROM scavanger_requests
GROUP BY  scavanger_id ) t1 LEFT JOIN scavanger_points t2 ON t1.scavanger_id = t2.user_id INNER JOIN scavanger_users t6 ON t6.id = t1.scavanger_id WHERE t6.role_id = 2) t3  ORDER BY (t3.total + (t3.point / 100000)) DESC) t4 , (SELECT @rank := 0) r) t5 ORDER BY t5.rank'));

        return $data;
    }
}
