<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        /*return User::create([
            'username' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'first_name' => 'yufen',
            'last_name' => 'charles',
            'gender' => "L",
            'date_of_birth' => '2017-09-06',
            'role_id' => 1,
            'mobile' => '3435435436',
            'image' => 'tesssst',
            'status' => 'Verified',
            'address' => 'jl aja',
            'facebook_email' => 'yufen.charles@yahoo.com',
            'gmail_email' => 'charlesyufen@gmail.com',
            'facebook_id' => 1,
            'gmail_id' => 1,
        ]);*/

        return User::create([
            // 'username' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'first_name' => "yufen",
            'last_name' => "charles",
            'gender' => "L",
            'mobile' => "08484540",
            'role_id' => 1,
            'address' => "jl. testes",
            'status' => 'Verified',
        ]);
    }
}
