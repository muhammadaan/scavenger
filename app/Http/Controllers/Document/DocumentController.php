<?php

namespace App\Http\Controllers\Document;

use App\Models\Document;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentController extends Controller
{
    public function documentShow($id)
    {
        try {
            $doc = Document::find($id);
            return view('document.view.detail', compact('doc'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function getData()
    {
//
        $users = \App\Models\Document::all();

        $i = 0;
        $data = [];
        $output = array(
            "data" => []
        );


        foreach ($users as $index => $list) {

            $link_edit = url('/admin/scavenger/edit') . '/' . $list->id;
            $link_delete = url('/admin/scavenger/delete') . '/' . $list->id;

            $output['data'][$i][] = $i + 1;
            $output['data'][$i][] = $list->image;
            $output['data'][$i][] = $list->type;
            $output['data'][$i][] = '<center><a href="' . $link_edit . '" data-popup="tooltip" title="Edit users"><i class="icon-edit"></i></a> || <a href="' . $link_delete . '" data-popup="tooltip" title="Delete users"><i class="icon-remove-sign"></i></a></center>';
            $i++;
        }
        return json_encode($output);

    }

    public function getModal($id)
    {
//

        $i = 0;
        $data = [];
        $output = array(
            "data" => []
        );
        $documents = \App\Models\Document::find($id);
        $output['data'][$i]['id'] = $documents->id;
        $output['data'][$i]['image'] = $documents->image;
        $output['data'][$i]['type'] = $documents->type;

        return json_encode($output);

    }

    public function documentUpdate(Request $request, $id)
    {
        $input = $request->all();
        define('UPLOAD_DIR', 'storage/app/');
        $image      = uniqid() . ".png";

//        return $request->id.''.$request->image.''.$request->type;
        $path       = $request->file('image')->storeAs('images', $image);

        $updateDoc              = Document::find($id);
        $updateDoc->image       = $image;
        $updateDoc->type        = $input['type'];
        $updateDoc->save();
////
//        return redirect()->route('scavenger.detail', $updateDoc->scavenger_id);
    }
    public function getInput(Request $request){
        return $request->all();
    }
    public function documentDestroy($id)
    {
        $doc = Document::find($id);
//        return $doc;
        $doc->delete();

        return redirect()->route('scavenger.detail', $doc->scavenger_id);
    }
}
