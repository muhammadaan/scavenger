<?php

namespace App\Http\Controllers\Notification;
// App\Http\Controllers\Notification\NotificationController::sendfirebase($value='')

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Notification;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;	
use Illuminate\Support\Facades\Log;
use Auth;

class NotificationController extends Controller
{
	public function usergetnotif(Request $request)
	{
		# code.
		$data 	        = Notification::where('user_id', $request->input('user_id'))->orderBy('id', 'desc')->get();

		return $result = array('result'=> 1, 'message'=> 'success', 'data' => $data);
	}
    //
    public static function sendfirebase($value='')
    {
    	# code...
       try {
            
        	$store 	= Notification::create([
        		'user_id' => $value['user_id'],
        		'type' => $value['type'],
        		'tittle' => $value['tittle'],
        		'content' => $value['content'],
        	]);

        	$user 	= \App\User::findOrFail($value['user_id']);

    		$optionBuilder                  = new OptionsBuilder();
    		$optionBuilder->setTimeToLive(60*20);

    		$notificationBuilder            = new PayloadNotificationBuilder($value['tittle']);
    		$notificationBuilder->setBody($value['content'])
    						    ->setSound('default');
    					    
    		$dataBuilder                    = new PayloadDataBuilder();
    		$dataBuilder->addData(['a_data' => 'my_data']);

    		$data                           = $dataBuilder->build();
    		$option                         = $optionBuilder->build();
    		$notification                   = $notificationBuilder->build();


    		$token                          = $user->fcm_token;
    		$downstreamResponse             = FCM::sendTo($token, $option, $notification, $data);

    		$sukses                         = $downstreamResponse->numberSuccess();
    		$fail                           = $downstreamResponse->numberFailure();
    		$modif                          = $downstreamResponse->numberModification();

            return array('sukses : ' . $sukses . ' fail : ' .$fail);

        } catch (\Exception $e) {
            Log::error('sendfirebase '.$e->getMessage(). ' '. $e->getFile().' '. $e->getLine());
        } 
    	
    }
   //  public function sendSMS (Request $request){
 		// $credentials = new \Nexmo\Client\Credentials\Basic('17073650', '394e2b258e78ff15');
   //      $client = new \Nexmo\Client($credentials);
   //      $message = $client->message()->send([
 		// 	'to' => $request->phone,
   //  		'from' => config('app.name'),
   //  		'text'=> 'Dibales dong mba Noenoe',
   //      ]);
   //      return array('result' => 1, 'data'=>$message);
   //  }
    public function sms_recycle_pickup(Request $request) {
    	$credentials = new \Nexmo\Client\Credentials\Basic('17073650', '394e2b258e78ff15');
    	$text = strtoupper(config('app.name')). ' : ' .'Hello! '. Auth::user()->first_name . ' Thank you for requesting recycle trash pick up. 
				Please check your application for futher notifaction about your request to pick up your recyclable trash. For further information, please visit our website wearescavenger.come';
        $client = new \Nexmo\Client($credentials);
        $data = array(
        	'phone' => $request->phone,
        	'from' => config('app.name') , 
        	'content' => $text
        );
        $message = $client->message()->send([
 			'to' => $request->phone,
    		'from' => config('app.name'),
    		'text'=> $text,
        ]);
        return array('result' => 1 , 'data' => $data);
    }
    public function sms_confirmation(Request $request) {
    	$credentials = new \Nexmo\Client\Credentials\Basic('17073650', '394e2b258e78ff15');
    	$text = strtoupper(config('app.name')). ' : ' .'Thank you for registering Scavenger account! We are happy to announce you are now become a part of our green movement. Visit our website wearescavenger.com';
        $client = new \Nexmo\Client($credentials);
        $data = array(
        	'phone' => $request->phone,
        	'from' => config('app.name') , 
        	'content' => $text
        );
        $message = $client->message()->send([
 			'to' => $request->phone,
    		'from' => config('app.name'),
    		'text'=> $text,
        ]);
        return array('result' => 1 , 'data' => $data);
    }

    public function sms_complete_recycle(Request $request) {
    	$credentials = new \Nexmo\Client\Credentials\Basic('17073650', '394e2b258e78ff15');
        $client = new \Nexmo\Client($credentials);
        // $text = urlencode('Hello!+We’re+successfully+pickup+your+recyclable+trash+and+ready+to+be+recycle.+Keep+recycle+	for+a+greener+Indonesia.+Visit+our+website+wearecavenger.com');

        $text = html_entity_decode(strtoupper(config('app.name')). ' : ' .'Hello! We are successfully pickup your recyclable trash and ready to be recycle. Keep recycle for a greener Indonesia. Visit our website wearecavenger.com');
        $data = array(
        	'phone' => $request->phone,
        	'from' => config('app.name') , 
        	'content' => $text
        );
        $message = $client->message()->send([
 			'to' => $request->phone,
    		'from' => config('app.name'),
    		'text'=> $text,
        ]);
        return array('result' => 1, 'data' => $data);
    }
    public function sms_donation(Request $request) {
    	$credentials = new \Nexmo\Client\Credentials\Basic('17073650', '394e2b258e78ff15');
        $client = new \Nexmo\Client($credentials);
        // $text = urlencode('Hello!+We’re+successfully+pickup+your+recyclable+trash+and+ready+to+be+recycle.+Keep+recycle+	for+a+greener+Indonesia.+Visit+our+website+wearecavenger.com');

        $text = html_entity_decode(strtoupper(config('app.name')). ' : ' .'Thank you for your donation. Your participation will be used to support the various activities of our non profit organization');
        $data = array(
        	'phone' => $request->phone,
        	'from' => config('app.name') , 
        	'content' => $text
        );
        $message = $client->message()->send([
 			'to' => $request->phone,
    		'from' => config('app.name'),
    		'text'=> $text,
        ]);
        return array('result' => 1, 'data' => $data);
    }
    public function sms_donation_verification(Request $request) {
    	$credentials = new \Nexmo\Client\Credentials\Basic('17073650', '394e2b258e78ff15');
        $client = new \Nexmo\Client($credentials);
        // $text = urlencode('Hello!+We’re+successfully+pickup+your+recyclable+trash+and+ready+to+be+recycle.+Keep+recycle+	for+a+greener+Indonesia.+Visit+our+website+wearecavenger.com');

        $text = html_entity_decode(strtoupper(config('app.name')). ' : ' .'Donation Verifications!
			We received notification that you already giving us donation in amount of Rp 100.000,-
			If you want to give a donation via bank transfer, please make a donation confirmation on our Scavenger Apps.
			For further information, please contact :
			Phone		: '. Auth::user()->mobile .'	(working hour ... - ...)
			E – Mail 	: '. Auth::user()->email
		);
        $data = array(
        	'phone' => $request->phone,
        	'from' => config('app.name') , 
        	'content' => $text
        );
        $message = $client->message()->send([
 			'to' => $request->phone,
    		'from' => config('app.name'),
    		'text'=> $text,
        ]);
        return array('result' => 1, 'data' => $data);
    }


}
