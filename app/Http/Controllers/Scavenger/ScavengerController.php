<?php

namespace App\Http\Controllers\Scavenger;

use App\Models\Document;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;

class ScavengerController extends Controller
{

      public function __construct()
     {
         $this->middleware('auth', ['except' => ['total_obp_driver']]);
     }

    
    public function index(){
        $scavengers          = User::orderBy('created_at','desc')->get();
        $this->data['title'] = "Scavenger List";

        return view('scavenger.view.list', ['scavengers'=>$scavengers], $this->data);
 
    }

    public function show($id)
    {
        try{
            $scavengers            = \App\Models\User::find($id);
            $this->data['title']   = 'Scavenger Detail';
            $this->data['ratings'] = \App\Models\Rating::whereScavengerId($id)->get();

            return view('scavenger.view.detail', compact('scavengers'), $this->data);
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
    }

//    public function getData(){
//
//    $users = \App\Models\User::all();
//
//     $i		= 0;
//     $data	= [];
//     $output = array(
//           "data" => []
//       );
//
//
//     foreach ($users as $index=>$list){
//         if ($list->role_id == 1){
//               $link_edit = url('/admin/scavenger/edit').'/'.$list->id ;
////             $link_delete = url('/admin/scavenger/delete').'/'.$list->id ;
//             // $edits = User::where('id',$list->id)->first();
//             // $roles = \App\UserGroup::find($list->roles);
//             $output['data'][$i][]    = $i+1;
//             $output['data'][$i][]    = $list->first_name;
//             // $output['data'][$i][]    = '<a href="'.$link_edit.'" >'.$list->first_name.'</a>';
//             if ($list->gender == 'L'){
//                 $output['data'][$i][] = 'Laki laki';
//             }
//             elseif ($list->gender == 'P' || $list->gender == 'W'){
//                 $output['data'][$i][] = 'Perempuan';
//             }else{
//                 $output['data'][$i][] = '';
//             }
////             $output['data'][$i][]    = $list->gender;
//             $output['data'][$i][]    = $list->date_of_birth;
//             $output['data'][$i][]    = $list->email;
//             $output['data'][$i][]    = $list->mobile;
//             $output['data'][$i][]    = $list->status;
//             $output['data'][$i][]    = Carbon::parse($list->created_at)->format('d M Y - H:i:s');
////             $output['data'][$i][]    = $list->created_by;
////             $output['data'][$i][]    = '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit users"><i class="icon-edit"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete users"><i class="icon-remove-sign"></i></a></center>';
//             $i++;
//         }
//         else{
//             '';
//         }
//        }
//        return json_encode($output);
//
//    }

    public function ScavengerAdd(){

        // return view('Users.view.add');
    }

    public function ScavengerPost(Request $request){

        // $input                        = $request->all();
        // $input['password']            = bcrypt($request->password);
        // $input['image']               = "";

        // $store                        = \App\Models\Admin::create($input);

        //  return redirect('/admin/users-list')->with('status', 'User Success Added!');;
    }

    public function Scavengerdestroy($id){

//       $data= \App\Models\Admin::find($id);
//       $data->delete();
//       return redirect('/admin/users-list')->with('status', 'User Success Deleted!');
    }

    public function Scavengersedit($id){
        
        $this->data['title'] = "Scavenger Edit";
        $this->data['data'] = \App\Models\User::find($id);
        return view('scavenger.view.edit',$this->data);
   }

    public function update(Request $request, $id)
    {
        $update = new \App\Models\User;
        $update = User::find($id);
        $update->status = $request->status;
        $update->note_verify = $request->note_verify;

        $count = $request->countDoc;
        for($i=0; $i<=$count; $i++){
            $updDoc = new \App\Models\Document;
            $updDoc->scavenger_id = $request->idScavenger;
            define('UPLOAD_DIR'.$i, 'storage/app/');
            $image       = uniqid().".png";


            if($i==0){
                $imgpicture  = $request->file('image')->storeAs('images', $image);
                $updDoc->image = $image;
                $updDoc->type = $request->type;
            }else{

                $img = 'image'.$i;
                $type = 'type'.$i;
                $imgpicture  = $request->file(''.$img)->storeAs('images', $image);
                $updDoc->image = $image;
                $updDoc->type = $request->$type;
            }
            $updDoc->save();
        }

        $update->update();

         return redirect('/admin/scavenger-list')->with('status', 'Scavenger Success Edited!');
     }

    public function total_obp_driver(Request $request)
    {
        # code...
        $id     = $request->input('scavenger_id');
        $data   = \DB::select('SELECT t5.* FROM (SELECT t4.*, @rank := @rank + 1 AS rank FROM (SELECT *, (t3.total_order + (t3.point / 100000)) AS total_akhir FROM (SELECT 
t1.*, 
CASE WHEN t2.point IS NOT NULL 
       THEN t2.point
       ELSE 0
END AS point,
CONCAT(t6.first_name," ",t6.last_name) AS name
FROM (SELECT
    scavanger_id,        
    CASE WHEN SUM(berat_realisasi) IS NOT NULL 
           THEN SUM(berat_realisasi)
           ELSE 0
    END AS total_weight,
    COUNT(*) AS total_order
FROM scavanger_requests WHERE scavanger_id = '.$id.'
GROUP BY  scavanger_id ) t1 LEFT JOIN scavanger_points t2 ON t1.scavanger_id = t2.user_id INNER JOIN scavanger_users t6 ON t6.id = t1.scavanger_id WHERE t6.role_id = 2) t3  ORDER BY (t3.total_order + (t3.point / 100000)) DESC) t4 , (SELECT @rank := 0) r) t5 ORDER BY t5.rank');

        if(count($data) > 0){

            $result = array('result'=> 1, 'message'=> 'success', 'data' => [
                'total_order' => $data[0]->total_order,
                'total_weight' => $data[0]->total_weight,
                'total_point' => $data[0]->point,
            ]);
        } else {
            $result = array('result'=> 1, 'message'=> 'success', 'data' => [
                'total_order' => 0,
                'total_weight' => 0,
                'total_point' => 0,
            ]);
        }

        return $result;
    }
}