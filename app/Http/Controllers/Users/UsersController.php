<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Carbon\Carbon;
use \App\Models\User;

class UsersController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(){
        $this->data['title'] = "Users List";
        return view('users.view.list',$this->data);
 
    }

    function getData(){

    $users = \App\Models\User::filteradmin()->get();

     $i		= 0;
     $data	= [];
     $output = array(
           "data" => []
       );

     foreach ($users as $index=>$list){

        $link_edit = url('/admin/users/edit').'/'.$list->id ;     	     
        $link_delete = url('/admin/users/delete').'/'.$list->id ;     	     
        // $roles = \App\UserGroup::find($list->roles);
            $output['data'][$i][]    = $index+1;            
            $output['data'][$i][]    = $list->username;
            $output['data'][$i][]    = $list->email;
            $output['data'][$i][]    = $list->roles;
            $output['data'][$i][]    = Carbon::parse($list->created_at)->format('d M Y - H:i:s');
            // $output['data'][$i][]    = $list->created_at->format('d M Y - H:i:s');            
            // $output['data'][$i][] = $list->password;
            $output['data'][$i][]    = '<center><a href="'.$link_edit.'" data-popup="tooltip" title="Edit users"><i class="icon-edit" style="color: rgb(0,146,69);"></i></a> || <a href="'.$link_delete.'" data-popup="tooltip" title="Delete users"><i class="icon-remove-sign" style="color: rgb(0,146,69);"></i></a></center>';
                $i++;
        }
        return json_encode($output);

    }

    public function UsersAdd(){

        return view('users.view.add');
    }

    public function UsersPost(Request $request){

        $input                        = $request->all();
        $input['password']            = bcrypt($request->input('password'));
        $input['image']               = "";
        $input['role_id']             = 0;        

        $store                        = User::create($input);

         return redirect('/admin/users-list')->with('status', 'User Success Added!');;
    }

    public function Usersdestroy($id){

      $data= \App\Models\User::find($id);
      $data->delete();
      return redirect('/admin/users-list')->with('status', 'User Success Deleted!');
    }

    public function Usersedit($id){
        // return $id;
        $this->data['title'] = "Users Edit";
        $this->data['data'] = \App\Models\User::find($id);
        return view('users.view.edit',$this->data);
   }

    public function update(Request $request, $id){

        $input                      = $request->all();
        $update                     = User::find($id);
        
        if(!empty($request->input('password'))){
            $input['password']      = bcrypt($request->input('password'));
        }

        $update->update($input);

        return redirect('/admin/users-list')->with('status', 'User Success Edited!');


     }

     public function usergetpoint(Request $request)
     {
         # code...
         try {

            $data = \App\Models\Point::where('user_id', $request->input('user_id'))->firstOrFail();

            $result = array('result'=> 1, 'message'=> 'success', 'data' => $data);


        } catch (\Exception $e) {            

            $result = array('result'=>0, 'message'=> 'user not found', 'data' => []);
        }

        return $result;
     }

     public static function updatefcm($value='', $fcm)
     {
         # code...
         $value->fcm_token  = $fcm;
         $value->update();
     }



}
