<?php

namespace App\Http\Controllers\Crontab;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CrontabController extends Controller
{
    //
    public function cancelrequest($value='')
    {
    	# code...
    	// $date 	= \Carbon\Carbon::parse('2018-05-15')->subDays(2)->toDateString();
    	$scs 	= [];

    	$date 	= \Carbon\Carbon::now()->subDays(2)->toDateString();
    	$data 	= \App\Models\Request::whereStatus(1)->where('order_date', '<', $date)->get();

    	foreach ($data as $key => $value) {
    		# code...
    		$value->status 	= 4;
    		$value->save();

    		array_push($scs, $value->id);
    	}


    	return response()->json([
    	    'status' => 100,
    	    'success' => count($scs)
    	]);
    }
}
