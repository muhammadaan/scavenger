<?php

namespace App\Http\Controllers\Open;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OpenController extends Controller
{
    //
    public function get_chartweight(Request $request)
    {
    	# code...
    	try {
    		
	    	$year = \Carbon\Carbon::now()->year;
	    	$data = \DB::select(\DB::raw('SELECT
	  a.category_id,
	  b.category_name,
	  sum(if(month(a.created_at) = 1, weight_estimate, 0))  AS Jan,
	  sum(if(month(a.created_at) = 2, weight_estimate, 0))  AS Feb,
	  sum(if(month(a.created_at) = 3, weight_estimate, 0))  AS Mar,
	  sum(if(month(a.created_at) = 4, weight_estimate, 0))  AS Apr,
	  sum(if(month(a.created_at) = 5, weight_estimate, 0))  AS May,
	  sum(if(month(a.created_at) = 6, weight_estimate, 0))  AS Jun,
	  sum(if(month(a.created_at) = 7, weight_estimate, 0))  AS Jul,
	  sum(if(month(a.created_at) = 8, weight_estimate, 0))  AS Aug,
	  sum(if(month(a.created_at) = 9, weight_estimate, 0))  AS Sep,
	  sum(if(month(a.created_at) = 10, weight_estimate, 0)) AS Oct,
	  sum(if(month(a.created_at) = 11, weight_estimate, 0)) AS Nov,
	  sum(if(month(a.created_at) = 12, weight_estimate, 0)) AS `Dec`
	FROM scavanger_request_details a INNER JOIN scavanger_categories b ON a.category_id = b.id WHERE YEAR(a.created_at) = '.$year.'
	GROUP BY a.category_id'));
	    	
	    	$plot = [];

	    	for ($i=0; $i < count($data); $i++) { 
	    		# code...
	    		
	    		array_push($plot, [
	    			'data' => [
	    				[1, $data[$i]->Jan],
	    				[2, $data[$i]->Feb],
	    				[3, $data[$i]->Mar],
	    				[4, $data[$i]->Apr],
	    				[5, $data[$i]->May],
	    				[6, $data[$i]->Jun],
	    				[7, $data[$i]->Jul],
	    				[8, $data[$i]->Aug],
	    				[9, $data[$i]->Sep],
	    				[10, $data[$i]->Oct],
	    				[11, $data[$i]->Nov],
	    				[12, $data[$i]->Dec],
	    			],
	    			'label' => $data[$i]->category_name,
	    		]);
	    		    	
	    	}

	    	// dd($plot);
	    	// sleep(3);
	    	return json_encode($plot);

    	} catch (\Exception $e) {
    		echo $e->getMessage(). ' '. $e->getFile(). ' '. $e->getLine();
    	}
    }
}
