<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'gender', 'mobile', 'status', 'first_name', 'last_name', 'date_of_birth', 'gmail_email', 'facebook_email', 'image', 'address', 'role_id', 'gmail_id', 'facebook_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['rating'];

    public function request()
    {
        return $this->hasMany('App\Models\Request','customer_id','id');
    }

    public function role()
    {
        return $this->hasOne('App\Models\Role','id','role_id');
    }

    // public function sendPasswordResetNotification($token)
    // {
    //     $this->notify(new ResetPasswordNotification($token));
    // }
    // 
    public function getRatingAttribute($value='')
    {
        # code...
        try {
            if($this->role_id == 2){
                $rating = \App\Models\Rating::where('scavenger_id', $this->id)->orderBy('id', 'desc')->firstOrFail();
                return $rating->rating_avg;
            }
        } catch (Exception $e) {
            return 0;
        }
    }
}
