<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    //
    protected $fillable = [
    	'request_id',
    	'user_id',
    	'rating',
    	'rating_avg',
    	'rating_count',
    	'note',
        'scavenger_id',
    ];

    public function user($value='')
    {
    	# code...
    	return $this->belongsTo('App\Models\User');
    }

    public function request($value='')
    {
    	# code...
    	return $this->belongsTo('\App\Models\Request');
    }
}
