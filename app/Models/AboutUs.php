<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    public $table = 'configs';
    protected $fillable = [
        'key',
        'value'
    ];

    protected $appends 	= ['image_url'];

    public function getImageUrlAttribute($value='')
    {
    	# code...
    	if($this->key == 'ABOUT US'){
    		return '';
    	}
    	if($this->key == 'VISION'){
    		return 'http://www.scavenger.id/wp-content/uploads/2017/11/Group-616-300x297.png';
    	}
    	if($this->key == 'MISSION'){
    		return 'http://www.scavenger.id/wp-content/uploads/2016/12/Group-617.png';
    	}

    	return '';
    }
}
