<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
     public $table = 'newsletters';

	    protected $fillable = [
	        'title', 'desc', 'status','category','mulai_dari','sampai_dengan'
	    ];
}
