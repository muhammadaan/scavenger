<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    public $table = 'donations';

    protected $fillable = [

        'request_id',
        'user_id',
        'amount',
        'payment_method',
        'status',
        'note',
        'booster_usage_flush', //ubntuk gamification bosoteer (0 & 1)
        'booster_usage_substrate', //ubntuk gamification substrate (history amount total donation)        
    ];

    protected $appends = ['confirmation_donations'];

    public function request()
    {
        return $this->belongsTo('App\Models\Request','request_id','id');
    }

    public function history()
    {
        return $this->hasOne('App\Models\RequestHistory','request_id','id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\RequestDetail','request_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id','id');
    }

    public function confirmation($value='')
    {
        # code...
        return $this->hasMany('\App\Models\ConfirmationDonation');
    }

    public function getConfirmationDonationsAttribute($value='')
    {
        # code...
        return \App\Models\ConfirmationDonation::where('donation_id', $this->id)->orderBy('id', 'desc')->first();
    }
}
