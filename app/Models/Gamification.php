<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gamification extends Model
{
    //
    protected $fillable = [
    	'user_id',
    	'watering',
    	'fertilizer',
    ];

    public function user($value='')
    {
    	# code...
    	return $this->belongsTo('\App\Models\User');
    }
}
