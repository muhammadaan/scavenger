<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestHistory extends Model
{
    public $table = 'request_histories';

    public function request()
    {
        return $this->belongsTo('App\Models\Request','request_id','id');
    }
}
