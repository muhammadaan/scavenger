<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    public $table = 'partners';

    protected $fillable = [
        'name', 'desc', 'logo',
    ];

    protected $appends = ['url_logo'];

    public function getUrlLogoAttribute($value='')
    {
    	# code...
    	return url('/').'/storage/app/images/'.$this->logo;
    }
}
