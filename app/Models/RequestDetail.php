<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestDetail extends Model
{
    public $table = 'request_details';

    protected $fillable = [
        'request_id',
        'weight_estimate',
        'category_id'
    ];

    public function request()
    {
        return $this->belongsTo('App\Models\Request','request_id','customer_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id','id');
    }
}
