<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public $table = 'vehicles';

    protected $fillable = [
        'vehicle_name',
        'desc',
        'status',
        'created_by'
    ];
}
