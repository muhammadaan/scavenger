<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    
    protected $fillable = [
   	 'name', 'status', 'route_access_list', 'desc'
    ];


    public function user()
    {
        return $this->belongsTo('App\Models\User','id','role_id');
    }
}
