<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    public $table = 'requests';

    protected $fillable = [

        // 'customer_id','longitude','latitude','order_date','date_request_pickup','time_request_pickup','image','image2','image3','note','address','vehicle','weight_total_estimate','status','pickup_date','pickup_time','scavanger_id','catatan'


        'customer_id',
        'longitude',
        'latitude',
        'order_date',
        'date_request_pickup',
        'time_request_pickup',
        'note',
        'image',
        'pickup_date',
        'pickup_time',
        'vehicle',
        'status',
        'weight_total_estimate',
        'scavanger_id',        
        'address',
        'catatan',
        'image2',
        'image3',
        'realisasi_pickup',
        'berat_realisasi', //dalam satuan kg
        'longitude_pickup',
        'langitude_pickup',
        'jarak', //dalam satuan meter
    ];

    protected $appends = ['category', 'rating', 'point_history'];


    public function user()
    {
        return $this->belongsTo('App\Models\User','customer_id','id');
    }

    public function donation()
    {
        return $this->hasOne('App\Models\Donation','request_id','id');
    }

    public function history()
    {
        return $this->hasOne('App\Models\RequestHistory','request_id','id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\RequestDetail','request_id','id');
    }

    public function pointHistory()
    {
        return $this->hasMany('App\Models\PointHistory','request_id','id');
    }

    public function getCategoryAttribute($value='')
    {
        # code...
        $data = '';
        $i = 0;
        foreach ($this->detail as $key => $value) {
            # code...
            if($i > 0){

                $data .= ', '.$value->category->category_name;
            } else {
                $data .= $value->category->category_name;

            }
            $i++;
        }

        return $data;
    }

    public function getPointHistoryAttribute($value='')
    {
        return \App\Models\PointHistory::where('request_id', $this->id)->first();
    }

    public function getScavengerAttribute($value='')
    {
        return \App\Models\User::where('id', $this->scavanger_id)->first();
    }

    public function scavenger($value='')
    {
        # code...
        return $this->belongsTo('App\Models\User','scavanger_id','id');
    }

    public function scavanger($value='')
    {
        # code...
        return $this->belongsTo('App\Models\User','scavanger_id','id');
    }

    public function getRatingAttribute($value='')
    {
        # code...
        try {
            
            return $this->hasOne('App\Models\Rating')->first()->rating;
        } catch (\Exception $e) {
            return 0;
        }
    }

    

}
