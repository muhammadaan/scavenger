<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    //
    protected $fillable 	= [
    	'user_id',
    	// 'point_history_id',
    	'point',
    ];

    public function history($value='')
    {
    	# code...
    	return $this->hasMany('\App\Models\PointHistory');
    }
}
