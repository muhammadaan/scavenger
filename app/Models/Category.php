<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = 'categories';

    protected $fillable = [
        'category_name',
        'desc',
        'image',
        'weight_total_category',
    ];

    public function requestDetail()
    {
        return $this->hasMany('App\Models\RequestDetail','category_id','id');
    }
}
