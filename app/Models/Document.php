<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public $table = 'documents';

    protected $fillable = [
        'scavenger_id',
        'image',
        'type'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','scavenger_id','id');
    }
}
