<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class User extends Model
{

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'gender', 'mobile', 'status', 'first_name', 'last_name', 'date_of_birth', 'gmail_email', 'facebook_email', 'image', 'address', 'role_id', 'gmail_id', 'facebook_id','created_at',
        // 'created_by'
    ];

    protected $appends  = ['name', 'document','rating'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeFilteradmin($value='')
    {
        # code...
        return $value->where('role_id', 0);
    }

    public function scopeFilteruser($value='')
    {
        # code...
        return $value->whereNotIn('role_id', [0]);
    }

    public function request()
    {
        return $this->hasMany('App\Models\Request','customer_id','id');
    }

    public function role()
    {
        return $this->hasOne('App\Models\Role','id','role_id');
    }

    public function document()
    {
        return $this->hasMany('App\Models\Document', 'id', 'scavenger_id');
    }

    public function getDocumentAttribute($value='')
    {
        try {

            return \App\Models\Document::where('scavenger_id', $this->id)->get();
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getNameAttribute($value='')
    {
        # code...
        return $this->first_name.' '.$this->last_name;
    }

    public function getRatingAttribute($value='')
    {
        # code...
        try {
            if($this->role_id == 2){
                $rating = \App\Models\Rating::where('scavenger_id', $this->id)->orderBy('id', 'desc')->firstOrFail();
                return $rating;
            }
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getRequestAttribute()
    {
        try {

            return \App\Models\Request::where('customer_id', $this->id)->get();
        } catch (\Exception $e) {
            return 0;
        }
    }

}
 