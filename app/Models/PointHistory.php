<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointHistory extends Model
{
    //
    protected $fillable 	= [
    	'user_id',
    	'point_id',
    	'request_id',
    	'topup_point',
    	'desc',
    ];

	public function request()
	{
		return $this->belongsTo('App\Models\Request','request_id','id');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User','user_id','id');
	}
}
