<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfirmationDonation extends Model
{
    public $table = 'confirmation_donations';

    protected $fillable =
        [
            'donation_id',
            'bank_account_name',
            'register_name',
            'bank_account_number',
            'transfer_to',
            'amount_of_donation',
            'note',            
            'email',
            'name_of_bank',
            'trasnfer_date',
            'picture',
            'donation_datetime',
        ];

    public function donation()
    {
        return $this->belongsTo('App\Models\Donation', 'donation_id', 'id');
    }
}
