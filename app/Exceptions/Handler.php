<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        

        // if($this->isHttpException($e)) {
        //     switch (intval($e->getStatusCode())) {
        //         case 400:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '400 Bad Request', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 401:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '401 Unauthorized ', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 403:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '403 Forbidden', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 404:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '404 Not Found', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 405:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '405 Method Not Allowed', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 408:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '408 Request Timeout', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 414:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '414 URI Too Long', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 500:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '500 Internal Server Error', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 502:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '502 Bad Gateway', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 503:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '503 Service Unavailable', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;
        //         case 504:                    
        //             return \Response::view('errors.error_admin', ['statusCode' => '504 Gateway Timeout', 'errorMessage' => "Sorry, an error has occured, ".$e->getMessage()]);
        //             break;

        //         default:
        //             return $this->renderHttpException($e);
        //             break;
        //     }
        // }
        // else {
        //     return parent::render($request, $e);
        // }

        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
