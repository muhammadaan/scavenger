<?php


// Auth::loginUsingId(1);

// Auth::routes();
    
Route::group(array('domain' => Config("subdom.second_dom")), function () {
    
    Route::get('/590d38f0603ef5a8a7be6c452438d609/{id}', 'Customer\CustomerHelper@verificationuser')->name('verificationuser');
    Route::get('/donate', function(){
            return \view('donateview');
    })->name('donateview');

    // Password Reset Routes...
    // Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    // Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    // Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::get('user/change-password/{token}', 'MailService\MailServiceController@forgotpasswordform')->name('changepassword');
    Route::post('user/change-password', 'MailService\MailServiceController@forgotpasswordpost')->name('changepasswordpost');
    Route::post('user/reset-password', 'MailService\MailServiceController@forgotpassword');

});

    Route::get('testt', function(){
        $user = \App\Models\User::find(239);
        return \App\Http\Controllers\MailService\MailServiceController::KonfirmasiPendaftaran($user);
    });
Route::group(array('domain' => Config("subdom.first_dom")), function () {



    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    // Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/admin/logout','HomeController@logout')->name('logout');
    // Registration Routes...
    // Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    // Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');


    Route::get('/','HomeController@index')->name('index');
    Route::group(['prefix' => 'admin', 'middleware' => 'superadmin'], function() {
                
        Route::get('/home', 'HomeController@dashboard')->name('dashboard');
            
            // USERS //

            Route::group(['namespace' => 'Users', 'as' => 'users.'], function () {
                Route::get('users-list', 'UsersController@index')->name('list');
                Route::get('users-add', 'UsersController@UsersAdd')->name('store');
                Route::post('users-add', 'UsersController@UsersPost')->name('add');
                Route::get('users/detail/{id}', 'UsersController@show')->name('detail');
                Route::get('users/edit/{id}', 'UsersController@Usersedit')->name('edit');
                Route::get('users/delete/{id}', 'UsersController@Usersdestroy')->name('delete');
                Route::post('users/{id}', 'UsersController@update')->name('update');
                Route::get('users-ajax-getdata', 'UsersController@getData')->name('ajax.getdata');
                Route::get('users/detail/ajax{id}', 'UsersController@getDataDetail')->name('ajax.getdata.detail');
                Route::get('users/detail/delete/{id}', 'UsersController@detailDelete')->name('detail.delete');

            });    

             // OUR PARTNER //

                Route::group(['namespace' => 'OurPartner', 'as' => 'partner.'], function () {
                    Route::get('partner-list', 'OurPartnerController@index')->name('list');
                    Route::get('partner-add', 'OurPartnerController@PartnerAdd')->name('store');
                    Route::post('partner-add', 'OurPartnerController@PartnerPost')->name('add');

                    Route::get('partner/edit/{id}', 'OurPartnerController@Partneredit')->name('edit');
                    Route::get('partner/delete/{id}', 'OurPartnerController@Partnerdestroy')->name('delete');
                    Route::post('partner/{id}', 'OurPartnerController@update')->name('update');
                    Route::get('partner-ajax-getdata', 'OurPartnerController@getData')->name('ajax.getdata');
                    Route::get('partner/detail/ajax{id}', 'OurPartnerController@getDataDetail')->name('ajax.getdata.detail');
                    Route::get('partner/detail/delete/{id}', 'OurPartnerController@detailDelete')->name('detail.delete');

                });    
        
           // NEWSLETTER //

                Route::group(['namespace' => 'NewsLetter', 'as' => 'newsletter.'], function () {
                    Route::get('newsletter-list', 'NewsLetterController@index')->name('list');
                    Route::get('newsletter-add', 'NewsLetterController@NewsLetterAdd')->name('store');
                    Route::post('newsletter-add', 'NewsLetterController@NewsLetterPost')->name('add');
                    Route::get('newsletter/edit/{id}', 'NewsLetterController@NewsLetteredit')->name('edit');
                    Route::get('newsletter/delete/{id}', 'NewsLetterController@NewsLetterdestroy')->name('delete');
                    Route::post('newsletter/{id}', 'NewsLetterController@update')->name('update');
                });    

            // ABOUT //

            Route::group(['namespace' => 'About', 'as' => 'about.'], function () {
                Route::get('about-list', 'AboutController@index')->name('show');
                Route::post('about-update', 'AboutController@update')->name('update');

            });   

            // ACL //

            Route::group(['namespace' => 'Acl'], function () {

                Route::get('/users-group', 'controller\AclGroupController@index')->name('users-group');
                Route::get('/users-group/create', 'controller\AclGroupController@create')->name('users-group.create');
                Route::post('/users-group', 'controller\AclGroupController@store')->name('users-group.store');
                Route::get('/users-group/{id}', 'controller\AclGroupController@show')->name('users-group.show');
                Route::put('/users-group/{id}', 'controller\AclGroupController@update')->name('users-group.update');
                Route::get('/users-group/delete/{id}', 'controller\AclGroupController@delete')->name('users-group.delete');
            });   


            //SCAVENGER


            Route::group(['namespace' => 'Scavenger', 'as' => 'scavenger.'], function () {

                Route::get('scavenger-list', 'ScavengerController@index')->name('list');
                Route::get('scavenger-add', 'ScavengerController@ScavengerAdd')->name('store');
                Route::post('scavenger-add', 'ScavengerController@ScavengerPost')->name('add');
                Route::get('scavenger/detail/{id}', 'ScavengerController@show')->name('detail');
                Route::get('scavenger/edit/{id}', 'ScavengerController@Scavengersedit')->name('edit');
                Route::post('scavenger/{id}', 'ScavengerController@ScavengerUpdate')->name('update');
                Route::get('scavenger/delete/{id}', 'ScavengerController@Scavengerdestroy')->name('delete');
                Route::post('scavenger/{id}', 'ScavengerController@update')->name('update');
                Route::get('scavenger-ajax-getdata', 'ScavengerController@getData')->name('ajax.getdata');
                Route::get('scavenger/detail/ajax{id}', 'ScavengerController@getDataDetail')->name('ajax.getdata.detail');
                Route::get('scavenger/detail/delete/{id}', 'ScavengerController@detailDelete')->name('detail.delete');
            });

            //DOCUMENT
            Route::group(['namespace' => 'Document', 'as' => 'document.'], function () {
                Route::get('document/detail/{id}', 'DocumentController@documentShow')->name('detail');
                Route::post('document/{id}', 'DocumentController@documentUpdate')->name('update');
                Route::get('document/delete/{id}', 'DocumentController@documentDestroy')->name('delete');
                Route::get('document-ajax-getdata', 'DocumentController@getData')->name('ajax.getdata');
                Route::get('document-ajax-getmodal/{id}', 'DocumentController@getModal')->name('ajax.getmodal');
            });

            //DONATION
            Route::group(['namespace' => 'Donation', 'as' => 'donation.'], function () {
                Route::get('donation-list', 'DonationController@index')->name('list');
                Route::get('donation-ajax-getdata', 'DonationController@getData')->name('ajax.getdata');
                Route::get('donation/detail/{id}', 'DonationController@show')->name('donation.show');
                Route::get('donation/{id}/edit', 'DonationController@edit')->name('donation.edit');
                Route::post('donation/{id}', 'DonationController@update')->name('donation.update');
            });

            //VEHICLE
            Route::group(['namespace' => 'Vehicle', 'as' => 'vehicle.'], function () {
                Route::get('vehicle-list', 'VehicleController@index')->name('list');
                Route::get('vehicle-add', 'VehicleController@VehicleAdd')->name('store');
                Route::post('vehicle-add', 'VehicleController@VehiclePost')->name('add');
                Route::get('vehicle/detail/{id}', 'VehicleController@show')->name('show');
                Route::get('vehicle/{id}/edit', 'VehicleController@edit')->name('edit');
                Route::post('vehicle/{id}', 'VehicleController@VehicleUpdate')->name('update');
            });

            //CUSTOMER


             Route::group(['namespace' => 'Customer', 'as' => 'customer.'], function () {
                Route::get('customer-list', 'CustomerController@index')->name('list');
                Route::get('customer-add', 'CustomerController@CustomerAdd')->name('store');
                Route::post('customer-add', 'CustomerController@CustomerPost')->name('add');
                Route::get('customer/detail/{id}', 'CustomerController@CustomerShow')->name('show');
                Route::get('customer/edit/{id}', 'CustomerController@Customeredit')->name('edit');
                Route::get('customer/delete/{id}', 'CustomerController@Customerdestroy')->name('delete');
                Route::post('customer/{id}', 'CustomerController@update')->name('update');
                Route::get('customer-ajax-getdata', 'CustomerController@getData')->name('ajax.getdata');
                Route::get('customer/detail/ajax{id}', 'CustomerController@getDataDetail')->name('ajax.getdata.detail');
                Route::get('customer/detail/delete/{id}', 'CustomerController@detailDelete')->name('detail.delete');
                Route::get('customer/myrecycle', 'CustomerController@myRecycle')->name('myrecycle');


            });

             //REQUEST


             Route::group(['namespace' => 'Request', 'as' => 'request.'], function () {
                Route::get('request-list', 'RequestController@index')->name('list');
                Route::get('request-add', 'RequestController@RequestAdd')->name('store');
                Route::post('request-add', 'RequestController@RequestPost')->name('add');
                Route::get('request/detail/{id}', 'RequestController@show')->name('detail');
                Route::get('request/edit/{id}', 'RequestController@Requestedit')->name('edit');
                Route::get('request/delete/{id}', 'RequestController@Requestdestroy')->name('delete');
                Route::post('request/{id}', 'RequestController@update')->name('update');
                Route::get('request-ajax-getdata', 'RequestController@getDatarequest')->name('ajax.getdata');
                Route::get('request/detail/ajax{id}', 'RequestController@getDataDetail')->name('ajax.getdata.detail');
                Route::get('request/detail/delete/{id}', 'RequestController@detailDelete')->name('detail.delete');

            });
             
    //category
              Route::group(['namespace' => 'Category', 'as' => 'category.'], function () {
                Route::get('category-list', 'CategoryController@index')->name('list');
                // Route::get('category-test', 'CategoryController@index')->name('list');
                Route::get('category-add', 'CategoryController@CategoryAdd')->name('add');
                Route::post('category-store', 'CategoryController@categoryPost')->name('store');
                Route::get('category/detail/{id}', 'CategoryController@show')->name('detail');
                
                Route::get('category/edit/{id}', 'CategoryController@Categoryedit')->name('edit');
                Route::get('category/delete/{id}', 'CategoryController@categorydestroy')->name('delete');
                Route::post('category/{id}', 'CategoryController@update')->name('update');
                Route::get('category-ajax-getdata', 'CategoryController@getDatacategory')->name('ajax.getdata');
                Route::get('category/detail/ajax{id}', 'CategoryController@getDataDetail')->name('ajax.getdata.detail');
                Route::get('category/detail/delete/{id}', 'CategoryController@detailDelete')->name('detail.delete');

            });

            //report
              Route::group(['namespace' => 'Report', 'as' => 'report.'], function () {
                Route::get('requestreport-list', 'ReportController@showrequest')->name('list-requestreport');
                Route::post('showdata', 'ReportController@showdata')->name('list-showdata');
                
                Route::get('requestreport-ajax-getdata', 'ReportController@getDataReportRequest')->name('ajax.getdata.rr');
                Route::post('reloadtable', 'ReportController@realoadtable')->name('realoadtable.apakek');
                Route::post('show-data', 'ReportController@realoadtable')->name('realoadtable.apakek');

                Route::get('report-list', 'ReportController@index')->name('list');
                Route::post('showdatareport', 'ReportController@showdatareport')->name('list-showdatareport');
                Route::post('filterreport', 'ReportController@showdatareport')->name('show-showdatareport');

                // Route::get('category-test', 'CategoryController@index')->name('list');
                // Route::get('category-add', 'CategoryController@CategoryAdd')->name('add');
                // Route::post('category-store', 'CategoryController@categoryPost')->name('store');
                // Route::get('category/detail/{id}', 'CategoryController@show')->name('detail');
                
                // Route::get('category/edit/{id}', 'CategoryController@Categoryedit')->name('edit');
                // Route::get('category/delete/{id}', 'CategoryController@categorydestroy')->name('delete');
                // Route::post('category/{id}', 'CategoryController@update')->name('update');
                // Route::get('category-ajax-getdata', 'CategoryController@getDatacategory')->name('ajax.getdata');
                // Route::get('category/detail/ajax{id}', 'CategoryController@getDataDetail')->name('ajax.getdata.detail');
                // Route::get('category/detail/delete/{id}', 'CategoryrController@detailDelete')->name('detail.delete');

            });

    });   

    Route::group(['namespace' => 'Crontab', 'as' => 'crontab.'], function () {
        Route::get('crontab/cancelrequest', 'CrontabController@cancelrequest')->name('cancelrequest');
    });

    Route::get('open/get_chartweight', 'Open\OpenController@get_chartweight')->name('get_chartweight');
});









