<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Http\Controllers\Auth\RegisterController;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;


// user = 1
// scavenger = 2

//Route::group(['middleware' => 'auth:api'], function()
//{	
	Route::post('user/changepassword', function() {
        $input = Input::all();
        
        $validator =  Validator::make(Input::all(), [
            'new_password' => 'required',
        ]);

        if ($validator->fails())
        {
            $error = $validator->errors();
            $message = 'Your data is not complete..';
            $result = array('result'=>0,'error'=>$message,'errorField'=>$error);
            return $result;
        }else{
            $user = App\Models\User::find($input['user_id']);
            $user->password = bcrypt($input['new_password']);
            $user->update();

            $result =  array('result' => 1, 'message' => "Password changed successfully!");
            return $result;
        }

        return $input;
    });

    Route::post('scavanger/changepassword', function() {
        $input = Input::all();
        
        $validator =  Validator::make(Input::all(), [
            'new_password' => 'required',
        ]);

        if ($validator->fails())
        {
            $error = $validator->errors();
            $message = 'Your data is not complete..';
            $result = array('result'=>0,'error'=>$message,'errorField'=>$error);
            return $result;
        }else{
            $user = App\Models\User::find($input['user_id']);
            $user->password = bcrypt($input['new_password']);
            $user->update();

            $result =  array('result' => 1, 'message' => "Password changed successfully!");
            return $result;
        }

        return $input;
    });

	Route::post('user/googleLogin', function() {    
        $input = Input::all();
        $check = App\Models\User::where('email', $input['email'])->where('role_id', 1)->first();

        if($check!=null){
	        if($check->gmail_id==$input['gmail_id']){
	        	//login if user already registered with gmail
	        	//
	    	} else{
	    		//login if user already registered but gmail_id not in the record
	    		$check->gmail_id = $input['gmail_id'];
	    		$check->update();
	    	}

	   		// $result =  array('result' => 1, 'error' => "Login success!", 'data' => $check);
	   		\App\Http\Controllers\Users\UsersController::updatefcm($check, $input['fcm_token']);
	   		$result =  array('result' => 1, 'error' => "Login success!", 'data' => $check);
	    }

    	else{
        	//register if new user
	        $user = App\Models\User::create([
		        'username' => $input['email'],
		        'email' => $input['email'],
		        'password' => "",
		        'first_name' => $input['first_name'],
		        'last_name' => $input['last_name'],
		        'gender' => "",
		        'mobile' => "",
		        'address' => "",
		        'role_id' => 1,
		        'gmail_id' => $input['gmail_id'],
		        'status' => 1,
		        'fcm_token' => $input['fcm_token'],
		    ]);
		    $to = $user->email;
		    $from = 'System@Scavenger.com';
		    $subject = 'Konfirmasi Pendaftaran';
        	$link = url('/api').'/'.'verificationuser'.'/' . base64_encode($user->id);

		    $mails = \App\Http\Controllers\MailService\MailServiceController::KonfirmasiPendaftaran($user);


	   		$result =  array('result' => 1, 'error' => "Register success! Please check your email", 'data' => $user);
	   	}
	   	return $result;
	});

	Route::post('user/facebookLogin', function() {    
        $input = Input::all();
        // $check = App\Models\User::where('facebook_id', $input['facebook_id'])->where('role_id', 2)->first();
        $check = App\Models\User::where('email', $input['email'])->where('role_id', 1)->first();

        if($check!=null){
        	if($check->facebook_id==$input['facebook_id']){
	        	//login if user already registered
		   		// $result =  array('result' => 1, 'error' => "Login success!", 'data' => $check);
	   		}
	   		else{
	    		//login if user already registered but facebook_id not in the record
	   			$check->facebook_id = $input['facebook_id'];
	   			$check->update();
	   		}

	   		\App\Http\Controllers\Users\UsersController::updatefcm($check, $input['fcm_token']);

		   	$result =  array('result' => 1, 'error' => "Login success!", 'data' => $check);
    	}
    	else{
        	//register if new user
	        $user = App\Models\User::create([
		        'username' => $input['email'],
		        'email' => $input['email'],
		        'password' => "",
		        'first_name' => $input['first_name'],
		        'last_name' => $input['last_name'],
		        'address' => "",
		        'gender' => "",
		        'mobile' => "",
		        'role_id' => 1,
		        'facebook_id' => $input['facebook_id'],
		        'status' => 1,
		        'fcm_token' => $input['fcm_token'],
		    ]);
         	$to = $user->email;
		    $from = 'System@Scavenger.com';
		    $subject = 'Konfirmasi Pendaftaran';
        	$link = url('/api').'/'.'verificationuser'.'/' . base64_encode($user->id);

		    $mails = \App\Http\Controllers\MailService\MailServiceController::KonfirmasiPendaftaran($user);

	   		$result =  array('result' => 1, 'error' => "Register success! Please check your email", 'data' => $user);
	   	}
	   	return $result;
	});

	Route::post('scavanger/googleLogin', function() {    
        $input = Input::all();
        // $check = App\Models\User::where('gmail_id', $input['gmail_id'])->where('role_id', 1)->first();
        $check = App\Models\User::where('email', $input['email'])->where('role_id', 2)->first();

    	if($check!=null){
	        if($check->gmail_id==$input['gmail_id']){
	        	//login if user already registered with gmail
		   		// $result =  array('result' => 1, 'error' => "Login success!", 'data' => $check);
	    	}

	    	else{
	    		//login if user already registered but gmail_id not in the record
	    		$check->gmail_id = $input['gmail_id'];
	    		$check->update();
	    	}

	    	\App\Http\Controllers\Users\UsersController::updatefcm($check, $input['fcm_token']);

		   	$result =  array('result' => 1, 'error' => "Login success!", 'data' => $check);
	    }
    	else{
        	//register if new scavanger
	        $user = App\Models\User::create([
		        'username' => $input['email'],
		        'email' => $input['email'],
		        'password' => "",
		        'first_name' => "",
		        'last_name' => "",
		        'address' => "",
		        'gender' => "",
		        'mobile' => "",
		        'role_id' => 1,
		        'gmail_id' => $input['gmail_id'],
		        'status' => 1,
		        'fcm_token' => $input['fcm_token'],
		    ]);

	   		$result =  array('result' => 1, 'error' => "Register success! Please check your email", 'data' => $user);
	   	}
	   	return $result;
	});

	Route::post('scavenger/facebookLogin', function() {    
        $input = Input::all();
        // $check = App\Models\User::where('facebook_id', $input['facebook_id'])->where('role_id', 1)->first();
        $check = App\Models\User::where('email', $input['email'])->where('role_id', 2)->first();
        
    	if($check!=null){
        	if($check->facebook_id==$input['facebook_id']){
	        	//login if user already registered
		   		// $result =  array('result' => 1, 'error' => "Login success!", 'data' => $check);
	   		}
	   		else{
	    		//login if user already registered but facebook_id not in the record
	   			$check->facebook_id = $input['facebook_id'];
	   			$check->update();
	   		}

	   		\App\Http\Controllers\Users\UsersController::updatefcm($check, $input['fcm_token']);

		   	$result =  array('result' => 1, 'error' => "Login success!", 'data' => $check);
    	}
    	else{
        	//register if new user
	        $user = App\Models\User::create([
		        'username' => $input['email'],
		        'email' => $input['email'],
		        'password' => "",
		        'first_name' => "",
		        'last_name' => "",
		        'address' => "	",
		        'gender' => "",
		        'mobile' => "",
		        'role_id' => 2,
		        'facebook_id' => $input['facebook_id'],
		        'status' => 0,
		        'fcm_token' => $input['fcm_token'],
		    ]);

	   		$result =  array('result' => 1, 'error' => "Register success! Please check your email", 'data' => $user);
	   	}
	   	return $result;
	});

	Route::post('scavanger/login', function() {
		$validator =  Validator::make(Input::all(), [
	        'email' => 'required',
	        'password' => 'required',
	    ]);
	    
	    if ($validator->fails()){
	        $error = $validator->errors();
	        $message = 'Login failed..';
	        $result = array('result'=>0,'error'=>$message, 'errorField'=>$error);
	    }

	    else{
	        $input = Input::all();
	        $check = App\Models\User::where('email', $input['email'])->where('role_id', 2)->first();
            
            if($check!=null){
            	if (\Hash::check($input['password'], $check->password)){
            		// $check->image = url('/').'/storage/app/images/'.$check->image;
            		if($check->status == 0){

            			$result =  array('result' => 2, 'error' => "Register failed! Email not yet verification", 'data' => $check);
            		}

            		if($check->status == 1){

	            		\App\Http\Controllers\Users\UsersController::updatefcm($check, $input['fcm_token']);
		   				$result =  array('result' => 1, 'error' => "Login success!", 'data'=> $check);
            		}
		        }
		        else{
	   				$result =  array('result' => 0, 'error' => "Email and password did not match..");
		        }
            }else{
	   			$result =  array('result' => 0, 'error' => "User not found..");
            }
		}

		return $result;
	});


	Route::post('user/login', function() {
		$validator =  Validator::make(Input::all(), [
	        'email' => 'required',
	        'password' => 'required',
	    ]);
	    if ($validator->fails()){
	        $error = $validator->errors();
	        $message = 'Login failed..';
	        $result = array('result'=>0,'error'=>$message, 'errorField'=>$error);
	    }
	    else{
	        $input = Input::all();
	        $check = App\Models\User::where('email', $input['email'])->where('role_id', 1)->first();

            if($check!=null){	        	

            	if (\Hash::check($input['password'], $check->password)){

            		\App\Http\Controllers\Users\UsersController::updatefcm($check, $input['fcm_token']);
	   				$result =  array('result' => 1, 'error' => "Login success!", 'data'=> $check);
		        }
		        else{
	   				$result =  array('result' => 0, 'error' => "Email and password did not match..");
		        }

            }else{
	   			$result =  array('result' => 0, 'error' => "User not found..");
            }
		}
		return $result;
	});

	Route::post('scavanger/register', function() {

		$checkmail 	= \App\Models\User::where('email', Input::get('email'))->where('role_id', 2)->first();

		if(count($checkmail) == 0){

			$validator =  Validator::make(Input::all(), [
		        'username' => 'required',
		        'email' => 'required',
		        'password' => 'required',
		        'first_name' => 'required',
		        // 'last_name' => 'required',
		        'gender' => 'required',
		        'mobile' => 'required|max:14',
				'address' => 'required',
		        // 'image' => 'required',
			    // 'date_of_birth' => 'required',
				// 'status' => 'required',
				// 'facebook_email' => 'required',
				// 'gmail_email' => 'required',
				// 'facebook_id' => 'required',
				// 'gmail_id' => 'required',
		    ]);
		    
		    if ($validator->fails()){
		        $error = $validator->errors();
		        $message = 'Your data is not complete..';
		        $result = array('result'=>0,'error'=>$message,'errorField'=>$error);
		    }

		    else{
		        $data = Input::all();

	    		$check = App\Models\User::where('email', $data['email'])->where('role_id', 2)->first();
	        	if($check!=null){
			   		$result =  array('result' => 1, 'error' => "Register failed! You already registered as Scavenger");
		    	}
		    	else{
		    		/*if(count($data['image'])>0){
				        define('UPLOAD_DIR', 'storage/app/images/');
				        $image =  uniqid().".jpeg";
				        $imgpicture = $data['image'];
				        $datapicture = base64_decode($imgpicture);
				        $path = UPLOAD_DIR . $image;
				        $success = file_put_contents($path, $datapicture);
			    	}*/

				    $user = App\Models\User::create([
				        'username' => $data['username'],
				        'email' => $data['email'],
				        'password' => bcrypt($data['password']),
				        'first_name' => $data['first_name'],
				        'last_name' => $data['last_name'],
				        'gender' => $data['gender'],
				        'mobile' => $data['mobile'],
				        'role_id' => 2,
				        'status' => 0,
				        'address' => $data['address'],
				        'fcm_token' => $data['fcm_token'],
				        // 'image' => $image,
				        // 'date_of_birth' => $data['date_of_birth'],
				        // 'facebook_email' => $data['facebook_email'],
				        // 'gmail_email' => $data['gmail_email'],
				        // 'facebook_id' => $data['facebook_id'],
				        // 'gmail_id' => $data['gmail_id'],
				    ]);

				    //send mail

			   		$result =  array('result' => 1, 'data' =>$user, 'error' => "Register success! Please check your email");
			   	}
			}

		} else {

			if($checkmail->status == 0){

				$result =  array('result' => 2, 'error' => "Register failed! Email already registered as Scavenger and not yet verification", 'data' => \App\Models\User::where('email', Input::get('email'))->first());
			}
			if($checkmail->status == 1){

				$result =  array('result' => 0, 'error' => "Register failed! Email already registered as Scavenger");
			}

		}

		return $result;
	});

	Route::post('user/register', function() {

		$checkmail 	= \App\Models\User::where('email', Input::get('username'))->where('role_id', 1)->count();

		if($checkmail == 0){

			$validator =  Validator::make(Input::all(), [
		        // 'username' => 'required',
		        'email' => 'required',
		        'password' => 'required',
		        'first_name' => 'required',
		        // 'last_name' => 'required',
		        'gender' => 'required',
		        'mobile' => 'required|max:14',
			    'date_of_birth' => 'required',
				// 'status' => 'required',
		        // 'image' => 'required',
				// 'facebook_email' => 'required',
				// 'gmail_email' => 'required',
				// 'facebook_id' => 'required',
				// 'gmail_id' => 'required',
		    ]);
		    
		    if ($validator->fails()){
		        $error = $validator->errors();
		        $message = 'Your data is not complete..';
		        $result = array('result'=>0, 'error'=>$message, 'errorField'=>$error);
		    }
		    else{
		        $data = Input::all();

		    	$check = App\Models\User::where('email', $data['email'])->where('role_id', 1)->first();
	        	if($check!=null){
			   		$result =  array('result' => 2, 'error' => "Register failed! You already registered as User");
		    	}
		    	else{
		    		$dateFormat= Carbon::createFromFormat('d/m/Y', $data['date_of_birth']);
				    $user = App\Models\User::create([
				        // 'username' => $data['username'],
				        'username' => $data['email'],
				        'email' => $data['email'],
				        'password' => bcrypt($data['password']),
				        'first_name' => $data['first_name'],
				        'last_name' => $data['last_name'],
				        'gender' => $data['gender'],
				        'mobile' => $data['mobile'],
				        'role_id' => 1,
				        'status' => 0,
				        'address' => '',
				        'fcm_token' => $data['fcm_token'],
				        'date_of_birth' => Carbon::parse($dateFormat)->format('Y-m-d'),
				        // 'image' => $data['last_name'],
				        // 'facebook_email' => $data['facebook_email'],
				        // 'gmail_email' => $data['gmail_email'],
				        // 'facebook_id' => $data['facebook_id'],
				        // 'gmail_id' => $data['gmail_id'],
				    ]);

				    //send mail
				    $mails = \App\Http\Controllers\MailService\MailServiceController::KonfirmasiPendaftaran($user);

			   		$result =  array('result' => 1, 'error' => "Register success! Please check your email");
			   	}
			}

		} else {
			$result =  array('result' => 2, 'error' => "Register failed! Email already registered as User");
		}
		return $result;
	});

	
	Route::post('/donations', function(Request $request) {
		$input = Input::all();
		if( isset($input['user_id']) && !empty($input['user_id'] )){
			$users = App\Models\User::where('id', $input['user_id'])->orderBy('id', 'desc')->get();
			if($users->isNotEmpty() ) {
				// $donations = App\Models\Request::where('customer_id', $input['user_id'] )->with('donation')->get();		
				$donations = App\Models\Donation::where('user_id', $input['user_id'] )->orderBy('id', 'desc')->get();		
				return array('result' => 1, 'data' => $donations, 'user' => $users);
			}
			else return array('result' => 0, 'error' => 'ID tidak ditemukan');
		}
		else return array('result' => 0, 'error' => 'Data Tidak Diisi', 'data' => []);			
    });

    Route::post('/donation/create', function() {
		$validator =  Validator::make(Input::all(), [
	        'amount' => 'required',
	        'payment_method' => 'required',
	    ]);

	    if ($validator->fails()){
	        $error = $validator->errors();
	        $message = 'Your data is not complete..';
	        $result = array('result'=>0, 'error'=>$message, 'errorField'=>$error);
	    }
	    else{
	    	$input = Input::all();

	    	$donation = App\Models\Donation::create([
		        'request_id' 	=> $input['request_id'],
		        'amount' 		=> $input['amount'],
		        'payment_method'=> $input['payment_method'],
		        'status' 		=> 0,
		        'user_id' 		=> $input['user_id'],
		        'donation_datetime' 		=> $input['donation_datetime'],
		    ]);

	        $result = array('result' => 1, 'data' => $donation);

	        $mail 	= \App\Http\Controllers\MailService\MailServiceController::VerifikasiDonasi($donation);
	    }
	    return $result;
    });

	Route::post('/partners', function() {
        $partners =  App\Models\Partner::all();
        return array('result' => 1, 'data' => $partners);
    });

	Route::post('/partner/create', function() {
	    $validator =  Validator::make(Input::all(), [
	        'name' => 'required',
	        'logo' => 'required',
	        'desc' => 'required'
	    ]);

	    if ($validator->fails()){
	        $error = $validator->errors();
	        $message = 'Your data is not complete..';
	        $result = array('result'=>0, 'error'=>$message, 'errorField'=>$error);
	    }
	    else{

	        define('UPLOAD_DIR', 'storage/app/images/');
	    	$input 			= Input::all();
	        $image 			=  uniqid().".png";
	        $imgpicture 	= $input['logo'];
	        $datapicture 	= base64_decode($imgpicture);
	        $path 			= UPLOAD_DIR . $image;
	        $success 		= file_put_contents($path, $datapicture);

	    	$categories 	= App\Models\Partner::create([
		        'name' => $input['name'],
		        'desc' => $input['desc'],
		        'logo' => $path,
		    ]);

	        $result = array('result' => 1, 'data' => $categories);
	    }
	    return $result;
    });	

    Route::post('/aboutUs', function() {
        $aboutus  = App\Models\AboutUs::find(1);
        $result   = array('result' => 1, 'data' => $aboutus);

        $result['data']['additional'] 	= App\Models\AboutUs::whereNotIn('id', [$aboutus->id])->orderBy('id', 'desc')->get();

        return $result;
    });

    Route::post('/aboutUs/create', function() {
        $aboutus = App\Models\AboutUs::create([
	        'key' => "LOREM IPSUM",
	        'value' => "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
	    ]);
        return array('result' => 1, 'data' => $aboutus);
    });

    Route::post('/newsletters', function() {
        $newsletters =  App\Models\Newsletter::where('mulai_dari', '<=', \Carbon\Carbon::now()->toDateString())->where('sampai_dengan', '>=', \Carbon\Carbon::now()->toDateString())->orderBy('id', 'desc')->get();
        return array('result' => 1, 'data' => $newsletters);
    });

    Route::post('user/request', function() {
    	$user_id = Input::get('user_id');


        $requests = App\Models\Request::with('scavenger')->where('customer_id', $user_id)->whereIn('status', [1,2])->orderBy('created_at', 'desc')->get();
        foreach ($requests as $request) {
        	$image = str_after($request->image, 'images/');
        	$request->image = $image;
        	$image2 = str_after($request->image2, 'images/');
        	$request->image2 = $image2;
        }
        
        return array('result' => 1, 'data' => $requests);
    });

    Route::post('user/request-complete', function() {
    	$user_id = Input::get('user_id');
    	
        $requests = App\Models\Request::with('scavenger')->where('customer_id', $user_id)->whereIn('status', [3, 4])->orderBy('created_at', 'desc')->get();

        return array('result' => 1, 'data' => $requests);
    });

    Route::post('scavanger/order', function() {
        $requests = App\Models\Request::where('status', 1)->with('user')->orderBy('created_at', 'desc')->get();

        foreach ($requests as $request) {
        	$image = str_after($request->image, 'images/');
        	$request->image = $image;	
        }
        return array('result' => 1, 'data' => $requests);

    });
    

    Route::post('user/profile', function() {
    	$user_id = Input::get('user_id');

        $user = App\Models\User::find($user_id);
        return array('result' => 1, 'data' => $user);
    });

    Route::post('user/update-profile', function() {
    	$validator =  Validator::make(Input::all(), [
	        'email' => 'required',
	        'first_name' => 'required',
	        // 'last_name' => 'required',
	        'gender' => 'required',
	        'mobile' => 'required|max:14',
		    // 'date_of_birth' => 'required',
	    ]);

	    if ($validator->fails()){
	        $error = $validator->errors();
	        $message = 'Your data is not complete..';
	        $result = array('result'=>0, 'error'=>$message, 'errorField'=>$error);
	    }
	    else{
	    	
	        $input = Input::all();
	        $user = App\Models\User::find($input['user_id']);


            if(isset($input['image']) && !empty($input['image'])){
		        define('UPLOAD_DIR', 'storage/app/images/');
		        $image 			= uniqid().".jpeg";
		        $imgpicture 	= $input['image'];
		        $imgpicture 	= str_replace('data:image/jpeg;base64,', '', $imgpicture);
		        $imgpicture 	= str_replace(' ', '+', $imgpicture);
		        $datapicture 	= base64_decode($imgpicture);
		        $path 			= UPLOAD_DIR . $image;
		        $success 		= file_put_contents($path, $datapicture);
		        $user->image 	= $path;    
		    }
		    
	        $user->username 	= $input['email'];
	        $user->email 		= $input['email'];
	        $user->first_name 	= $input['first_name'];
	        $user->last_name 	= $input['last_name'];
	        $user->gender 		= $input['gender'];
	        $user->mobile 		= $input['mobile'];


	        if (isset($input['date_of_birth']) && !empty($input['date_of_birth'])) {
	        	$dateFormat 			= Carbon::createFromFormat('d/m/Y', $input['date_of_birth']);
	        	$user->date_of_birth 	= Carbon::parse($dateFormat)->format('Y-m-d');
	        }

	        $user->update();
	        $user 	= App\Models\User::find($input['user_id']);
	        $result = array('result' => 1, 'error' => "Profile updated successfully!", 'data' => $user);
	    }
	    return $result;
    });

    Route::post('scavanger/update-profile', function() {
    	$validator =  Validator::make(Input::all(), [
	        'email' 		=> 'required',
	        'first_name' 	=> 'required',
	        // 'last_name' => 'required',
	        'gender' 		=> 'required',
	        'mobile' 		=> 'required|max:14',
		    // 'date_of_birth' => 'required',
	    ]);

	    if ($validator->fails()){
	        $error 		= $validator->errors();
	        $message 	= 'Your data is not complete..';
	        $result 	= array('result'=>0, 'error'=>$message, 'errorField'=>$error);
	    }
	    else{
	        $input 		= Input::all();
	        $user 		= App\Models\User::find($input['user_id']);
	        // $dateFormat= Carbon::createFromFormat('d/m/Y', $input['date_of_birth']);

            /*if(count($input['image'])>0){
		        define('UPLOAD_DIR', 'storage/app/images/');
		        $image = uniqid().".jpeg";
		        $imgpicture = $input['image'];
		        $imgpicture = str_replace('data:image/jpeg;base64,', '', $imgpicture);
		        $imgpicture = str_replace(' ', '+', $imgpicture);
		        $datapicture = base64_decode($imgpicture);
		        $path = UPLOAD_DIR . $image;
		        $success = file_put_contents($path, $datapicture);
	        	$user->image = $image;
		    }*/

	        $user->username 	= $input['email'];
	        $user->email 		= $input['email'];
	        $user->first_name 	= $input['first_name'];
	        $user->last_name 	= $input['last_name'];
	        $user->gender 		= $input['gender'];
	        $user->mobile 		= $input['mobile'];
	        // $user->date_of_birth = Carbon::parse($dateFormat)->format('Y-m-d');
	        $user->address 		= $input['address'];
	        $user->update();

	        $user = App\Models\User::find($input['user_id']);
	        // $user->image = url('/').'/storage/app/images/'.$user->image;
	        $result = array('result' => 1, 'error' => "Profile updated successfully!" , 'data' => $user);
	    }

	    return $result;
    });

    Route::post('/category', function() {
        $categories =  App\Models\Category::all();
        return array('result' => 1, 'data' => $categories);
    });

    Route::post('/category/create', function() {
	    $validator =  Validator::make(Input::all(), [
	        'category_name' => 'required',
	        'desc' 			=> 'required'
	    ]);

	    if ($validator->fails()){
	        $error 		= $validator->errors();
	        $message 	= 'Your data is not complete..';
	        $result 	= array('result'=>0, 'error'=>$message, 'errorField'=>$error);
	    }
	    else{
	    	$input = Input::all();
	    	$categories 		= App\Models\Category::create([
		        'category_name' => $input['category_name'],
		        'desc' 			=> $input['desc'],
		    ]);
	        $result = array('result' => 1, 'data' => $categories);
	    }
	    return $result;
    });

    Route::post('/request/create', function() {

    	$cehecrole 	= \App\Models\User::whereId(Input::get('customer_id'))->whereRoleId(2)->count();
    	if($cehecrole > 0){
    		return 'pake customer_id nya user';
    	}

    	$validator 	=  Validator::make(Input::all(), [
		    'customer_id' 			=> 'required',
			'longitude' 			=> 'required',
			'latitude' 				=> 'required',
			'order_date' 			=> 'required',
			// 'image' 				=> 'required',
			'address' 				=> 'required',
			'weight_total_estimate' => 'required',
	    ]);

		if ($validator->fails()){
	        $error   = $validator->errors();
	        $message = 'Your data is not complete..';
	        $result  = array('result'=>0, 'error'=>$message, 'errorField'=>$error);
	    }
    	else {
			$data                = [];
			$dataRecycle         = [];
			$dataDetails         = array();
			$totalWeight         = array();
			$input               = Input::all();
			$temp_sampah         = json_decode($input['sampah'], true);
			$dateFormatOrderDate = Carbon::createFromFormat('d/m/Y', $input['order_date']);
			$categories          = App\Models\Category::all();

		    
		    if(isset($input['date_request_pickup'])){
		    	$dateFormatDateRequestPickup = Carbon::createFromFormat('d/m/Y', $input['date_request_pickup']);
		    }

		    // if(isset($input['image'])){
		    //     $image = uniqid().".jpeg";
		    //     $imgpicture = $input['image'];
		    //     $datapicture = base64_decode($imgpicture);
		    //     $path = UPLOAD_DIR . $image;
		    //     $success = file_put_contents($path, $datapicture);
		    // } 

		    $request                        = new App\Models\Request;
		    $request->customer_id           = $input['customer_id'];

		    $request->longitude             = $input['longitude'];
		    $request->latitude              = $input['latitude'];
		    $request->order_date            = Carbon::parse($dateFormatOrderDate)->format('Y-m-d');
		    // 'image'                      = > $image;
		    $request->note                  = $input['note'];
		    $request->catatan               = $input['catatan'];
		    $request->address               = $input['address'];
		    $request->weight_total_estimate = $input['weight_total_estimate'];
		    $request->status                = 1;
			
		    
	        $id_img1       	= uniqid().".jpeg";
	        $id_img2       	= uniqid().".jpeg";
	        $id_img3       	= uniqid().".jpeg";
	        $uniqid        	= array($id_img1, $id_img2, $id_img3);
		    $datas 			= \App\Http\Controllers\Request\RequestHelper::storeImage($input['image'], $input['image2'], $input['image3']);

		    foreach($datas as $key => $images){
                $datapicture    	= base64_decode($images);
                $path           	= UPLOAD_DIR . $uniqid[$key];
                file_put_contents($path, $datapicture);
                if($key	==	0){
                    $request->image = $uniqid[$key];
                }
                if($key	==	1){
                    $request->image2= $uniqid[$key];
                }
                if($key	==	2){
                    $request->image3= $uniqid[$key];
                }
            }
		    
		    $request->save();

		    // $request->image = $image[0];
		    // $request->image2 = $image[1];
		    // $request->image3 = $image[2	];
		    
		    // return $request;
			// $request->save();

		    if(isset($input['date_request_pickup'])){
		        $request->date_request_pickup = Carbon::parse($dateFormatDateRequestPickup)->format('Y-m-d');
		        $request->save();
		    }

		    if(isset($input['time_request_pickup'])){
		        $request->time_request_pickup = $input['time_request_pickup'];
		        $request->save();	
		    }

		    \Illuminate\Support\Facades\Log::info(json_encode(['cari di sini ' => $temp_sampah]));

		    foreach ($temp_sampah as $key => $value) {
		    	if($value > 0){
		    		
			    	$category = App\Models\Category::where('category_name', $key)->first();

			    	$detail_sampah = App\Models\RequestDetail::create([
				        'category_id' 		=> $category->id,
				        'request_id' 		=> $request->id,
				        'weight_estimate' 	=> $value,
				    ]);
		    	}
		    }

    		$requests_user = App\Models\Request::where('customer_id', Input::get('customer_id'))->with('detail')->orderBy('id', 'desc')->get();

        	foreach ($requests_user as $each_request) {
	    		$temp_request = $each_request->detail;
	    		foreach ($temp_request as $detail) {
	    			array_push($dataDetails, $detail);
	    		}
	    	}
	    	foreach ($categories as $category) {
	    		$tempWeight = array();
	    		foreach ($dataDetails as $dataDetail) {
	    			if($category->id == $dataDetail->category_id){
		    			array_push($tempWeight, $dataDetail->weight_estimate);
		    		}
	    		}
	    		$data[$category->id] =array('id'=>$category->id, 'name'=>$category->category_name, 'weight'=>array_sum($tempWeight));
				array_push($totalWeight, $data[$category->id]['weight']);
	    	}
	    	foreach ($data as $value) {
	    		array_push($dataRecycle, $value);
	    	}


	    	$request['category'] = $dataRecycle;

	        // $result = array('result'=>1, 'error'=>'Request was submitted successfully', 'data'=> $dataRecycle);
	        $result = array('result'=>1, 'error'=>'Request was submitted successfully', 'data'=> $request);
	        $mails  = \App\Http\Controllers\MailService\MailServiceController::PermintaanPengambilanBarang($request);
	        return $result;
	    }
	    return $result;
    });

    Route::post('user/my_recycle', function() {

		$data          = array();
		$dataRecycle   = array();
		$dataDetails   = array();
		$totalWeight   = array();
		$user_id       = Input::get('user_id');

		$categories    = App\Models\Category::all();
		$requests_user = App\Models\Request::where('customer_id', $user_id)->where('status' , 3)->with('detail')->orderBy('id', 'desc')->get();
		

		if(count($requests_user) > 0){
	    	foreach ($requests_user as $each_request) {
	    		$temp_request = $each_request->detail;
	    		array_push($totalWeight, $each_request->berat_realisasi);
	    		foreach ($temp_request as $detail) {
	    			array_push($dataDetails, $detail);
	    		}
	    	}
	    	foreach ($categories as $category) {
	    		$tempWeight = array();
	    		foreach ($dataDetails as $dataDetail) {
	    			if($category->id == $dataDetail->category_id){
		    			array_push($tempWeight, $dataDetail->weight_estimate);
		    			// array_push($tempWeight, $dataDetail->berat_realisasi);
		    		}
	    		}
	    		$data[$category->id] =array('id'=>$category->id, 'name'=>$category->category_name, 'weight'=>array_sum($tempWeight));
				// array_push($totalWeight, $data[$category->id]['weight']);
	    	}

	    	foreach ($data as $value) {
	    		array_push($dataRecycle, $value);
	    	}
		}



    	$result = array('result'=>1, 'error'=>'Fetching data success!', 'total_weight'=>array_sum($totalWeight), 'total_request'=>count($requests_user));

    	$result['total_point'] = \App\Models\Point::where('user_id', $user_id)->count() > 0 ? \App\Models\Point::where('user_id', $user_id)->first()->point : 0 ;

    	$gamification 	= \App\Models\Gamification::where('user_id', $user_id);

    	$result['total_watering']       = $gamification->count() > 0 ? $gamification->first()->watering : 0;
    	$result['total_watering_mod']   = $gamification->count() > 0 ? floor($gamification->first()->watering/3) : 0;
    	$result['total_fertilizer']     = $gamification->count() > 0 ? $gamification->first()->fertilizer : 0;
    	$result['total_fertilizer_mod'] = $gamification->count() > 0 ? floor($gamification->first()->fertilizer / 200000 ) : 0;
    	

    	$result['data']        = $dataRecycle;

		return $result;
	});

	Route::post('user/change_photo_profile', function() {
        $input 	= Input::all();
    	$user 	= App\Models\User::find($input['user_id']);
		if(count($input['image'])>0){
	        define('UPLOAD_DIR', 'storage/app/images/');
	        $image 			= uniqid().".jpeg";
	        $imgpicture 	= $input['image'];
	        $datapicture 	= base64_decode($imgpicture);
	        $path 			= UPLOAD_DIR . $image;
	        $success 		= file_put_contents($path, $datapicture);
	    	$user->image 	= $image;
	    	$user->update();
	    }

    	$user = App\Models\User::find($input['user_id']);
    	// $user['image'] = url('/').'/storage/app/images/'.$user->image;
		return $result = array('result' => 1, 'error' => "Profile updated successfully!", 'data'=>$user);
    });

    Route::post('scavenger/change_photo_profile', function() {
        $input 				= Input::all();
    	$user 				= App\Models\User::find($input['user_id']);

		if(count($input['image'])>0){
	        define('UPLOAD_DIR', 'storage/app/images/');
	        $image 			= uniqid().".jpeg";
	        $imgpicture 	= $input['image'];
	        $datapicture 	= base64_decode($imgpicture);

	        $path 			= UPLOAD_DIR . $image;
	        $success 		= file_put_contents($path, $datapicture);
	    	$user->image 	= $path;
	    	$user->update();
	    }

    	$user 				= App\Models\User::find($input['user_id']);
    	// $user['image'] = url('/').'/storage/app/images/'.$user->image;
		return $result = array('result' => 1, 'error' => "Profile updated successfully!", 'data'=>$user);
    });
    

    Route::post('user/totalRequest', function() {

    	$data                  = array();
    	$dataDetails           = array();
    	$user_id               = Input::get('user_id');
    	// $requests_user         = App\Models\Request::where('customer_id', $user_id)->where('status', 3)->with('detail')->get();    	
    	$requests_user         = App\Models\Request::where('customer_id', $user_id)->where('status', 3);    	
    	$total_weight_estimate = array();


    	// foreach ($requests_user as $request) {
    	// 	// $request_detail = $request->detail;
    	// 	// foreach ($request_detail as $detail) {
    	// 	// 	// array_push($total_weight_estimate, $detail->weight_estimate);
    	// 	// 	array_push($total_weight_estimate, $detail->berat_realisasi);
    	// 	// }
    	// }


    	$data['total_weight']  = $requests_user->sum('berat_realisasi');
    	$data['total_request'] = $requests_user->count();
    	$data['total_point']   = \App\Models\Point::where('user_id', $user_id)->count() > 0 ? \App\Models\Point::where('user_id', $user_id)->first()->point : 0 ;

    	$result = array('result'=>1, 'error'=>'Fetching data success!', 'data'=>$data);
		return $result;
	});

    Route::post('kebijakan_keamanan', function() {
    	$about = App\Models\AboutUs::find(3);

    	$result = array('result'=>1, 'error'=>'Fetching data success!', 'data'=>$about);
		return $result;
    });

    Route::post('/tentangKami', function() {
        $aboutus =  App\Models\AboutUs::find(2);
        $result = array('result' => 1, 'error'=> 'Fetching data success', 'data' => $aboutus);
        return $result;
    });
    Route::post('scavenger/acceptOrder' , function() {
    	$input   = Input::all();
    	$request = App\Models\Request::find($input['request_id']);
    	if($request->scavanger_id == $input['scavanger_id']){
    		return "You can't take your own order";
    	}
    	else{		
	    	$pickup_date_format = Carbon::createFromFormat('d/m/Y', $input['pickup_date']);
	    	$order_date_format  = Carbon::createFromFormat('Y-m-d', $request->order_date);
	    	$pickup_date        = Carbon::parse($pickup_date_format)->format('Y-m-d');	    	

	    	$request->update([
	    		'status'           => 2 ,
	    		'pickup_time'      => $input['pickup_time'] ,
	    		'pickup_date'      => $pickup_date,
	    		'scavanger_id'     => $input['scavanger_id'],
	    		'vehicle'          => $input['vehicle'],
	    		'catatan'          => $input['catatan'],
	    		'longitude_pickup' => $input['longitude_pickup'],
	    		'langitude_pickup' => $input['langitude_pickup'],
	    		'jarak'            => $input['jarak'],
	    	]);

	    	$sendnotif 	 	= \App\Http\Controllers\Notification\NotificationController::sendfirebase([
	    		'user_id' 	=> $request->customer_id,
	    		'type' 		=> 2,
	    		'tittle' 	=> config('app.name'),
	    		'content' 	=> 'your request has been booked',
	    	]);

	    	$result      	= array(
	    		'result' 	=> 1, 
	    		'firebase' 	=> array_flatten($sendnotif),
	    		'error'		=> 'Fetching data success', 
	    		'data' 		=> $request);
	    	$mails  		= \App\Http\Controllers\MailService\MailServiceController::acceptOrder($request);
	        return $result;
    	}
    });

    Route::post('sendMail/request-recycle' , function (\Illuminate\Mail\Mailer $mailer) {
    	$input = Input::all();
    	
    	$mailer
    		->to($input['email'])
    		->send(new \App\Mail\RequestRecycle($input['title'] , $input['content']));

    	return array('result'=> 1 , 'data' => $mailer);
    });

    Route::post('sendMail/notification-recycle-complete' , function (\Illuminate\Mail\Mailer $mailer) {
    	$input = Input::all();
    	$mailer
    		->to($input['email'])
    		->send(new \App\Mail\NotificationRecycleComplete($input['title'],$input['content']));

    	return array(
    		'result' => 1 ,
    		'data'	 => $mailer);
    });


    Route::post('sendNotif' , function () {
    	$input 					= Input::all();
    	$newsletters 			= new \App\Models\Newsletter;
    	$newsletters->title 	= $input['title'];
    	$newsletters->desc 		= $input['content'];
    	$newsletters->status 	= 1;
    	$newsletters->category 	= 1;
    	$newsletters->save();

    	$optionBuilder 			= new OptionsBuilder();
		$optionBuilder->setTimeToLive(60*20);
		$notificationBuilder 	= new PayloadNotificationBuilder($newsletters->title);
		$notificationBuilder->setBody($newsletters->desc)
						    ->setSound('default');
		$dataBuilder 			= new PayloadDataBuilder();
		$dataBuilder->addData(['a_data' => 'my_data']);

		$data 					= $dataBuilder->build();
		$option 				= $optionBuilder->build();
		$notification 			= $notificationBuilder->build();
		$token 					= $input['fcm_token'];
			
		$downstreamResponse 	= FCM::sendTo($token, $option, $notification, $data);

		$sukses 	= $downstreamResponse->numberSuccess();
		$fail 		= $downstreamResponse->numberFailure();
		$modif 		= $downstreamResponse->numberModification();


		//return Array - you must remove all this tokens in your database
		// $downstreamResponse->tokensToDelete(); 

		//return Array (key : oldToken, value : new token - you must change the token in your database )
		// 	$downstreamResponse->tokensToModify();

		//return Array - you should try to resend the message to the tokens in the array
		// $downstreamResponse->tokensToRetry();

		// print_r($downstreamResponse);
		// die();
		dd($downstreamResponse);
		return array('Sukses : ' . $sukses . ' Failure : ' . $fail . ' Modification : ' . $modif);
    });
    // Route::post('sendSMS' , function(Request $request) {

    // 	$sendSMS = \App\Http\Controllers\Notification\NotificationController::sendSMS([
    // 		'to' => $request->phone,
    // 		// 'from' => config('APP_NAME'),
    // 		'from' => 'test lagi',
    // 		'text'=> 'testing',
    // 	]);
    	
    // });
    Route::post('history/pickup', function () {
    	$input 			= Input::all();
    	$waiting 		= App\Models\Request::where('scavanger_id', $input['scavanger_id'])->where('status', 2)
    		->with(['user'=> function($query) {
			    $query->select('id', 'mobile', 'first_name', 'last_name');
			}])
    		->orderBy('id', 'desc')->get();
    	$complete 		= App\Models\Request::where('scavanger_id', $input['scavanger_id'])->where('status', 3)
    		->with(['user'=> function($query) {
			    $query->select('id', 'mobile', 'first_name', 'last_name');
			}])
    		->orderBy('id', 'desc')->get();

    	return $result 	= array(
    		'result' 	=> 1, 
    		'waiting'	=> $waiting , 
    		'complete' 	=> $complete );
    });

    

    Route::post('scavenger/ubahsemua', 'Request\RequestController@updaterequestsemua')->name('scavengerubahsemua');    
    Route::post('sendSMS' , 'Notification\NotificationController@sendSMS')->name('sendSMS');
    Route::post('SMS/recycle-pickup' , 'Notification\NotificationController@sms_recycle_pickup')->name('sms.recycle_pickup');
    Route::post('SMS/confirmation', 'Notification\NotificationController@sms_confirmation')->name('sms.confirmation');
    Route::post('SMS/complete-recycle' , 'Notification\NotificationController@sms_complete_recycle')->name('sms.recycle_pickup');
    Route::post('SMS/donation' , 'Notification\NotificationController@sms_donation')->name('sms.donation');
    Route::post('SMS/donation_verification' , 'Notification\NotificationController@sms_donation_verification')->name('sms.donation_verification');
    Route::post('confirmation_donations' , 'Donation\ConfirmationDonationController@confirmation_donations')->name('confirmation_donations.store');

    Route::post('user/getpoint', 'Users\UsersController@usergetpoint')->name('usergetpoint');    
    Route::post('user/getnotif', 'Notification\NotificationController@usergetnotif')->name('usergetnotif'); 
    Route::post('user/rating_request', 'Rating\RatingController@ratingRequest')->name('rating_request'); 
    Route::post('get_data_vehicle', 'Vehicle\VehicleController@getDataVehicle')->name('get_data_vehicle'); 
    Route::post('user/watering', 'gamification\GamificationController@watering')->name('watering'); 
    Route::post('user/fertilizer', 'gamification\GamificationController@fertilizer')->name('fertilizer'); 
    Route::post('user/ranking', 'Rank\RankUserController@rankuser')->name('user.rank');
    Route::post('scavenger/total_obp_driver', 'Scavenger\ScavengerController@total_obp_driver')->name('scavenger.total_obp_driver');

//});
// Route::post('user/reset-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// Route::get('/verificationuser/{id}', 'Customer\CustomerHelper@verificationuser')->name('verificationuser');



